
//Scroll Animation
jQuery(document).ready(function() {

    jQuery('body').css('visibility','visible');

    // TimelineLite for title animation, then start up superscrollorama when complete
    (new TimelineLite({onComplete:initScrollAnimations}))
        .to( jQuery('#text-milestone-land'), .4, {delay: .5, css:{opacity:1, top:'50%'}, ease:Back.easeOut});
    
    
    function initScrollAnimations() {
        var controller = jQuery.superscrollorama();

        // set duration, in pixels scrolled, for pinned element
        var pinDur = 12000;

        // create animation timeline for pinned element
        controller.pin(jQuery('#about-us-scroll-anim'), pinDur, {
//            var pinAnimations = new TimelineLite();
//            pinAnimations
            anim: (new TimelineLite())
        /* Landing Out, 2016 Slide 1 In */
//            .append(TweenMax.from(jQuery('#pin-frame-pin h2'), .5, {css:{marginTop:0}, ease: Quad.easeInOut}))
            .append([
                TweenMax.to(jQuery('#text-milestone-land'), 1, {css:{opacity:0, top:'50%'}}),
                TweenMax.to(jQuery('#milestone-slide-2016-1-left'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#milestone-slide-2016-1-right'), 10, {css:{bottom:0}}),
                TweenMax.to(jQuery('#ms-indicator-home'), 1, {css:{color:'#d7d7d7'}}),
                TweenMax.to(jQuery('#ms-indicator-home .indicator-circle'), 1, {css:{backgroundColor:'#d7d7d7'}}),
                TweenMax.to(jQuery('#ms-indicator-2016'), 1, {css:{color:'#ee2e24'}}),
                TweenMax.to(jQuery('#ms-indicator-2016 .indicator-circle'), 1, {css:{backgroundColor:'#ee2e24'}})                
            ], 10)
            .append([
                TweenMax.to(jQuery('#milestone-slide-2016-1-left'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#milestone-slide-2016-1-right'), 10, {css:{bottom:0}}),
                TweenMax.to(jQuery('#year-circle-2016-1'), 4, {css:{opacity:1, left:'20%'}}),
                TweenMax.to(jQuery('#text-2016-1'), 4, {css:{opacity:1, left:'30%'}}),
                TweenMax.to(jQuery('#line-2016-1'), 4, {css:{opacity:1, top:'52%'}})
            ], 10)

        /* 2016 Slide 1 Out, 2016 Slide 2 In */
            .append([
                TweenMax.to(jQuery('#milestone-slide-2016-1-left'), 15, {css:{top:'-100%'}}),
                TweenMax.to(jQuery('#milestone-slide-2016-1-right'), 15, {css:{bottom:'-100%'}}),
                TweenMax.to(jQuery('#year-circle-2016-1'), 4, {css:{opacity:0, left:'17%'}}),
                TweenMax.to(jQuery('#text-2016-1'), 4, {css:{opacity:0, left:'27%'}}),
                TweenMax.to(jQuery('#line-2016-1'), 4, {css:{opacity:0, left:'35%'}}),
                TweenMax.to(jQuery('#milestone-slide-2016-2-left'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#milestone-slide-2016-2-right'), 10, {css:{bottom:0}})
            ], 10)
            .append([
                TweenMax.to(jQuery('#milestone-slide-2016-2-left'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#milestone-slide-2016-2-right'), 10, {css:{bottom:0}}),
                TweenMax.to(jQuery('#year-circle-2016-2'), 4, {css:{opacity:1, left:'20%'}}),
                TweenMax.to(jQuery('#text-2016-2'), 4, {css:{opacity:1, top:'13%'}}),
                TweenMax.to(jQuery('#line-2016-2'), 4, {css:{opacity:1, top:'33%'}})
            ], 10)

        /* 2016 Slide 2 Out, 2016 Slide 3 In */
            .append([
                TweenMax.to(jQuery('#milestone-slide-2016-2-left'), 15, {css:{top:'-100%'}}),
                TweenMax.to(jQuery('#milestone-slide-2016-2-right'), 15, {css:{bottom:'-100%'}}),
                TweenMax.to(jQuery('#year-circle-2016-2'), 4, {css:{opacity:0, left:'17%'}}),
                TweenMax.to(jQuery('#text-2016-2'), 4, {css:{opacity:0, right:'23%'}}),
                TweenMax.to(jQuery('#line-2016-2'), 4, {css:{opacity:0, right:'35.5%'}}),
                TweenMax.to(jQuery('#milestone-slide-2016-3-left'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#milestone-slide-2016-3-right'), 10, {css:{bottom:0}})
            ], 10)
            .append([
                TweenMax.to(jQuery('#milestone-slide-2016-3-left'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#milestone-slide-2016-3-right'), 10, {css:{bottom:0}}),
                TweenMax.to(jQuery('#year-circle-2016-3'), 4, {css:{opacity:1, left:'20%'}}),
                TweenMax.to(jQuery('#text-2016-3'), 4, {css:{opacity:1, left:'20%'}}),
                TweenMax.to(jQuery('#line-2016-3'), 4, {css:{opacity:1, left:'30%'}})
            ], 10)

        /* 2016 Slide 3 Out, 2016 Slide 4 In */
            .append([
                TweenMax.to(jQuery('#milestone-slide-2016-3-left'), 15, {css:{top:'-100%'}}),
                TweenMax.to(jQuery('#milestone-slide-2016-3-right'), 15, {css:{bottom:'-100%'}}),
                TweenMax.to(jQuery('#year-circle-2016-3'), 4, {css:{opacity:0, left:'17%'}}),
                TweenMax.to(jQuery('#text-2016-3'), 4, {css:{opacity:0, left:'27%'}}),
                TweenMax.to(jQuery('#line-2016-3'), 4, {css:{opacity:0, left:'35%'}}),
                TweenMax.to(jQuery('#milestone-slide-2016-4-left'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#milestone-slide-2016-4-right'), 10, {css:{bottom:0}})
            ], 10)
            .append([
                TweenMax.to(jQuery('#milestone-slide-2016-4-left'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#milestone-slide-2016-4-right'), 10, {css:{bottom:0}}),
                TweenMax.to(jQuery('#year-circle-2016-4'), 4, {css:{opacity:1, left:'20%'}}),
                TweenMax.to(jQuery('#text-2016-4'), 4, {css:{opacity:1, top:'33%'}}),
                TweenMax.to(jQuery('#line-2016-4'), 4, {css:{opacity:1, left:'30%'}})
            ], 10)

        /* 2016 Slide 4 Out, 2016 Slide 5 In */
            .append([
                TweenMax.to(jQuery('#milestone-slide-2016-4-left'), 15, {css:{top:'-100%'}}),
                TweenMax.to(jQuery('#milestone-slide-2016-4-right'), 15, {css:{bottom:'-100%'}}),
                TweenMax.to(jQuery('#year-circle-2016-4'), 4, {css:{opacity:0, top:'12%'}}),
                TweenMax.to(jQuery('#text-2016-4'), 4, {css:{opacity:0, top:'30%'}}),
                TweenMax.to(jQuery('#line-2016-4'), 4, {css:{opacity:0, top:'57%'}}),
                TweenMax.to(jQuery('#milestone-slide-2016-5-left'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#milestone-slide-2016-5-right'), 10, {css:{bottom:0}})
            ], 10)
            .append([
                TweenMax.to(jQuery('#milestone-slide-2016-5-left'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#milestone-slide-2016-5-right'), 10, {css:{bottom:0}}),
                TweenMax.to(jQuery('#year-circle-2016-5'), 4, {css:{opacity:1, left:'20%'}}),
                TweenMax.to(jQuery('#text-2016-5'), 4, {css:{opacity:1, left:'30%'}}),
                TweenMax.to(jQuery('#line-2016-5'), 4, {css:{opacity:1, top:'39%'}})
            ], 10)

        /* 2016 Slide 5 Out, 2016 Slide 6 In */
            .append([
                TweenMax.to(jQuery('#milestone-slide-2016-5-left'), 15, {css:{top:'-100%'}}),
                TweenMax.to(jQuery('#milestone-slide-2016-5-right'), 15, {css:{bottom:'-100%'}}),
                TweenMax.to(jQuery('#year-circle-2016-5'), 4, {css:{opacity:0, left:'17%'}}),
                TweenMax.to(jQuery('#text-2016-5'), 4, {css:{opacity:0, left:'27%'}}),
                TweenMax.to(jQuery('#line-2016-5'), 4, {css:{opacity:0, left:'36%'}}),
                TweenMax.to(jQuery('#milestone-slide-2016-6-left'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#milestone-slide-2016-6-right'), 10, {css:{bottom:0}})
            ], 10)
            .append([
                TweenMax.to(jQuery('#milestone-slide-2016-6-left'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#milestone-slide-2016-6-right'), 10, {css:{bottom:0}}),
                TweenMax.to(jQuery('#year-circle-2016-6'), 4, {css:{opacity:1, left:'20%'}}),
                TweenMax.to(jQuery('#text-2016-6'), 4, {css:{opacity:1, left: '53%'}}),
                TweenMax.to(jQuery('#line-2016-6'), 4, {css:{opacity:1, top:'33%'}})
            ], 10)

        /* 2016 Slide 6 Out, 2016 Slide 7 In */
            .append([
                TweenMax.to(jQuery('#milestone-slide-2016-6-left'), 15, {css:{top:'-100%'}}),
                TweenMax.to(jQuery('#milestone-slide-2016-6-right'), 15, {css:{bottom:'-100%'}}),
                TweenMax.to(jQuery('#year-circle-2016-6'), 4, {css:{opacity:0, left:'17%'}}),
                TweenMax.to(jQuery('#text-2016-6'), 4, {css:{opacity:0, left:'56%'}}),
                TweenMax.to(jQuery('#line-2016-6'), 4, {css:{opacity:0, left:'35%'}}),
                TweenMax.to(jQuery('#milestone-slide-2016-7-left'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#milestone-slide-2016-7-right'), 10, {css:{bottom:0}})
            ], 10)            
            .append([
                TweenMax.to(jQuery('#milestone-slide-2016-7-left'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#milestone-slide-2016-7-right'), 10, {css:{bottom:0}}),
                TweenMax.to(jQuery('#year-circle-2016-7'), 4, {css:{opacity:1, left:'20%'}}),
                TweenMax.to(jQuery('#text-2016-7'), 4, {css:{opacity:1, top:'36%'}}),
                TweenMax.to(jQuery('#line-2016-7'), 4, {css:{opacity:1, top:'47%'}})
            ], 10)  

        /* 2016 Slide 7 Out, 2015 Slide 1 In */
            .append([
                TweenMax.to(jQuery('#milestone-slide-2016-7-left'), 15, {css:{top:'-100%'}}),
                TweenMax.to(jQuery('#milestone-slide-2016-7-right'), 15, {css:{bottom:'-100%'}}),
                TweenMax.to(jQuery('#year-circle-2016-7'), 4, {css:{opacity:0, left:'17%'}}),
                TweenMax.to(jQuery('#text-2016-7'), 4, {css:{opacity:0, left:'27%'}}),
                TweenMax.to(jQuery('#line-2016-7'), 4, {css:{opacity:0, left:'35%'}}),
                TweenMax.to(jQuery('#milestone-slide-2015-1-left'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#milestone-slide-2015-1-right'), 10, {css:{bottom:0}}),
                TweenMax.to(jQuery('#ms-indicator-2016'), 1, {css:{color:'#d7d7d7'}}),
                TweenMax.to(jQuery('#ms-indicator-2016 .indicator-circle'), 1, {css:{backgroundColor:'#d7d7d7'}}),
                TweenMax.to(jQuery('#ms-indicator-2015'), 1, {css:{color:'#ee2e24'}}),
                TweenMax.to(jQuery('#ms-indicator-2015 .indicator-circle'), 1, {css:{backgroundColor:'#ee2e24'}}) 
            ], 10)            
            .append([
                TweenMax.to(jQuery('#milestone-slide-2015-1-left'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#milestone-slide-2015-1-right'), 10, {css:{bottom:0}}),
                TweenMax.to(jQuery('#year-circle-2015-1'), 4, {css:{opacity:1, left:'20%'}}),
                TweenMax.to(jQuery('#text-2015-1'), 4, {css:{opacity:1, bottom:'29%'}}),
                TweenMax.to(jQuery('#line-2015-1'), 4, {css:{opacity:1, left:'27%'}})
            ], 10)  

        /* 2015 Slide 1 Out, 2015 Slide 2 In */
            .append([
                TweenMax.to(jQuery('#milestone-slide-2015-1-left'), 15, {css:{top:'-100%'}}),
                TweenMax.to(jQuery('#milestone-slide-2015-1-right'), 15, {css:{bottom:'-100%'}}),
                TweenMax.to(jQuery('#year-circle-2015-1'), 4, {css:{opacity:0, left:'17%'}}),
                TweenMax.to(jQuery('#text-2015-1'), 4, {css:{opacity:0, left:'27%'}}),
                TweenMax.to(jQuery('#line-2015-1'), 4, {css:{opacity:0, left:'35%'}}),
                TweenMax.to(jQuery('#milestone-slide-2015-2-left'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#milestone-slide-2015-2-right'), 10, {css:{bottom:0}})
            ], 10)            
            .append([
                TweenMax.to(jQuery('#milestone-slide-2015-2-left'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#milestone-slide-2015-2-right'), 10, {css:{bottom:0}}),
                TweenMax.to(jQuery('#year-circle-2015-2'), 4, {css:{opacity:1, left:'20%'}}),
                TweenMax.to(jQuery('#text-2015-2'), 4, {css:{opacity:1, top:'20%'}}),
                TweenMax.to(jQuery('#line-2015-2'), 4, {css:{opacity:1, top:'36%'}})
            ], 10)    

        /* 2015 Slide 2 Out, 2015 Slide 3 In */
            .append([
                TweenMax.to(jQuery('#milestone-slide-2015-2-left'), 15, {css:{top:'-100%'}}),
                TweenMax.to(jQuery('#milestone-slide-2015-2-right'), 15, {css:{bottom:'-100%'}}),
                TweenMax.to(jQuery('#year-circle-2015-2'), 4, {css:{opacity:0, left:'17%'}}),
                TweenMax.to(jQuery('#text-2015-2'), 4, {css:{opacity:0, left:'27%'}}),
                TweenMax.to(jQuery('#line-2015-2'), 4, {css:{opacity:0, left:'36%'}}),
                TweenMax.to(jQuery('#milestone-slide-2015-3-left'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#milestone-slide-2015-3-right'), 10, {css:{bottom:0}})
            ], 10)            
            .append([
                TweenMax.to(jQuery('#milestone-slide-2015-3-left'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#milestone-slide-2015-3-right'), 10, {css:{bottom:0}}),
                TweenMax.to(jQuery('#year-circle-2015-3'), 4, {css:{opacity:1, left:'20%'}}),
                TweenMax.to(jQuery('#text-2015-3'), 4, {css:{opacity:1, left:'20%'}})
            ],10)    

        /* 2015 Slide 3 Out, 2013 Slide 1 In */
            .append([
                TweenMax.to(jQuery('#milestone-slide-2015-3-left'), 15, {css:{top:'-100%'}}),
                TweenMax.to(jQuery('#milestone-slide-2015-3-right'), 15, {css:{bottom:'-100%'}}),
                TweenMax.to(jQuery('#year-circle-2015-3'), 4, {css:{opacity:0, left:'17%'}}),
                TweenMax.to(jQuery('#text-2015-3'), 4, {css:{opacity:0, left:'17%'}}),
                TweenMax.to(jQuery('#line-2015-3'), 4, {css:{opacity:0, left:'17%'}}),
                TweenMax.to(jQuery('#milestone-slide-2013-1-left'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#milestone-slide-2013-1-right'), 10, {css:{bottom:0}}),
                TweenMax.to(jQuery('#ms-indicator-2015'), 1, {css:{color:'#d7d7d7'}}),
                TweenMax.to(jQuery('#ms-indicator-2015 .indicator-circle'), 1, {css:{backgroundColor:'#d7d7d7'}}),
                TweenMax.to(jQuery('#ms-indicator-2013'), 1, {css:{color:'#ee2e24'}}),
                TweenMax.to(jQuery('#ms-indicator-2013 .indicator-circle'), 1, {css:{backgroundColor:'#ee2e24'}}) 
            ], 10)            
            .append([
                TweenMax.to(jQuery('#milestone-slide-2013-1-left'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#milestone-slide-2013-1-right'), 10, {css:{bottom:0}}),
                TweenMax.to(jQuery('#year-circle-2013-1'), 4, {css:{opacity:1, left:'20%'}}),
                TweenMax.to(jQuery('#text-2013-1'), 4, {css:{opacity:1, left:'43%'}})
            ], 10)   

        /* 2013 Slide 1 Out, 2013 Slide 2 In */
            .append([
                TweenMax.to(jQuery('#milestone-slide-2013-1-left'), 15, {css:{top:'-100%'}}),
                TweenMax.to(jQuery('#milestone-slide-2013-1-right'), 15, {css:{bottom:'-100%'}}),
                TweenMax.to(jQuery('#year-circle-2013-1'), 4, {css:{opacity:0, left:'17%'}}),
                TweenMax.to(jQuery('#text-2013-1'), 4, {css:{opacity:0, left:'46%'}}),
                TweenMax.to(jQuery('#milestone-slide-2013-2-left'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#milestone-slide-2013-2-right'), 10, {css:{bottom:0}})
            ], 10)            
            .append([
                TweenMax.to(jQuery('#milestone-slide-2013-2-left'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#milestone-slide-2013-2-right'), 10, {css:{bottom:0}}),
                TweenMax.to(jQuery('#year-circle-2013-2'), 4, {css:{opacity:1, left:'20%'}}),
                TweenMax.to(jQuery('#text-2013-2'), 4, {css:{opacity:1, bottom:'4%'}}),
                TweenMax.to(jQuery('#line-2013-2'), 4, {css:{opacity:1, bottom:'29%'}})
            ], 10)  

        /* 2013 Slide 2 Out, 2013 Slide 3 In */
            .append([
                TweenMax.to(jQuery('#milestone-slide-2013-2-left'), 15, {css:{top:'-100%'}}),
                TweenMax.to(jQuery('#milestone-slide-2013-2-right'), 15, {css:{bottom:'-100%'}}),
                TweenMax.to(jQuery('#year-circle-2013-2'), 4, {css:{opacity:0, left:'17%'}}),
                TweenMax.to(jQuery('#text-2013-2'), 4, {css:{opacity:0, bottom:'7%'}}),
                TweenMax.to(jQuery('#line-2013-2'), 4, {css:{opacity:0, bottom:'26%'}}),
                TweenMax.to(jQuery('#milestone-slide-2013-3-left'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#milestone-slide-2013-3-right'), 10, {css:{bottom:0}})
            ], 10)            
            .append([
                TweenMax.to(jQuery('#milestone-slide-2013-3-left'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#milestone-slide-2013-3-right'), 10, {css:{bottom:0}}),
                TweenMax.to(jQuery('#year-circle-2013-3'), 4, {css:{opacity:1, left:'20%'}}),
                TweenMax.to(jQuery('#text-2013-3'), 4, {css:{opacity:1, bottom:'12%'}}),
                TweenMax.to(jQuery('#line-2013-3'), 4, {css:{opacity:1, bottom:'3%'}})
            ], 10)   

        /* 2013 Slide 3 Out, 2012 Slide 1 In */
            .append([
                TweenMax.to(jQuery('#milestone-slide-2013-3-left'), 15, {css:{top:'-100%'}}),
                TweenMax.to(jQuery('#milestone-slide-2013-3-right'), 15, {css:{bottom:'-100%'}}),
                TweenMax.to(jQuery('#year-circle-2013-3'), 4, {css:{opacity:0, left:'17%'}}),
                TweenMax.to(jQuery('#text-2013-3'), 4, {css:{opacity:0, bottom:'7%'}}),
                TweenMax.to(jQuery('#line-2013-3'), 4, {css:{opacity:0, bottom:'26%'}}),
                TweenMax.to(jQuery('#milestone-slide-2012-1-left'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#milestone-slide-2012-1-right'), 10, {css:{bottom:0}}),
                TweenMax.to(jQuery('#ms-indicator-2013'), 1, {css:{color:'#d7d7d7'}}),
                TweenMax.to(jQuery('#ms-indicator-2013 .indicator-circle'), 1, {css:{backgroundColor:'#d7d7d7'}}),
                TweenMax.to(jQuery('#ms-indicator-2012'), 1, {css:{color:'#ee2e24'}}),
                TweenMax.to(jQuery('#ms-indicator-2012 .indicator-circle'), 1, {css:{backgroundColor:'#ee2e24'}}) 
            ], 10)            
            .append([
                TweenMax.to(jQuery('#milestone-slide-2012-1-left'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#milestone-slide-2012-1-right'), 10, {css:{bottom:0}}),
                TweenMax.to(jQuery('#year-circle-2012-1'), 4, {css:{opacity:1, left:'20%'}}),
                TweenMax.to(jQuery('#text-2012-1'), 4, {css:{opacity:1, bottom:'22%'}}),
                TweenMax.to(jQuery('#line-2012-1'), 4, {css:{opacity:1, bottom:'4%'}})
            ], 10) 

        /* 2012 Slide 1 Out, 2010 Slide 1 In */
            .append([
                TweenMax.to(jQuery('#milestone-slide-2012-1-left'), 15, {css:{top:'-100%'}}),
                TweenMax.to(jQuery('#milestone-slide-2012-1-right'), 15, {css:{bottom:'-100%'}}),
                TweenMax.to(jQuery('#year-circle-2012-1'), 4, {css:{opacity:0, left:'17%'}}),
                TweenMax.to(jQuery('#text-2012-1'), 4, {css:{opacity:0, bottom:'19%'}}),
                TweenMax.to(jQuery('#line-2012-1'), 4, {css:{opacity:0, bottom:'7%'}}),
                TweenMax.to(jQuery('#milestone-slide-2010-1-left'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#milestone-slide-2010-1-right'), 10, {css:{bottom:0}}),
                TweenMax.to(jQuery('#ms-indicator-2012'), 1, {css:{color:'#d7d7d7'}}),
                TweenMax.to(jQuery('#ms-indicator-2012 .indicator-circle'), 1, {css:{backgroundColor:'#d7d7d7'}}),
                TweenMax.to(jQuery('#ms-indicator-2010'), 1, {css:{color:'#ee2e24'}}),
                TweenMax.to(jQuery('#ms-indicator-2010 .indicator-circle'), 1, {css:{backgroundColor:'#ee2e24'}}) 
            ], 10)            
            .append([
                TweenMax.to(jQuery('#milestone-slide-2010-1-left'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#milestone-slide-2010-1-right'), 10, {css:{bottom:0}}),
                TweenMax.to(jQuery('#year-circle-2010-1'), 4, {css:{opacity:1, left:'20%'}}),
                TweenMax.to(jQuery('#text-2010-1'), 4, {css:{opacity:1, top:'20%'}}),
                TweenMax.to(jQuery('#line-2010-1'), 4, {css:{opacity:1, top:'39%'}})
            ], 10)    

        /* 2010 Slide 1 Out, 2007 Slide 1 In */
            .append([
                TweenMax.to(jQuery('#milestone-slide-2010-1-left'), 15, {css:{top:'-100%'}}),
                TweenMax.to(jQuery('#milestone-slide-2010-1-right'), 15, {css:{bottom:'-100%'}}),
                TweenMax.to(jQuery('#year-circle-2010-1'), 4, {css:{opacity:0, left:'17%'}}),
                TweenMax.to(jQuery('#text-2010-1'), 4, {css:{opacity:0, top:'17%'}}),
                TweenMax.to(jQuery('#line-2010-1'), 4, {css:{opacity:0, bottom:'42%'}}),
                TweenMax.to(jQuery('#milestone-slide-2007-1-left'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#milestone-slide-2007-1-right'), 10, {css:{bottom:0}}),
                TweenMax.to(jQuery('#ms-indicator-2010'), 1, {css:{color:'#d7d7d7'}}),
                TweenMax.to(jQuery('#ms-indicator-2010 .indicator-circle'), 1, {css:{backgroundColor:'#d7d7d7'}}),
                TweenMax.to(jQuery('#ms-indicator-2007'), 1, {css:{color:'#ee2e24'}}),
                TweenMax.to(jQuery('#ms-indicator-2007 .indicator-circle'), 1, {css:{backgroundColor:'#ee2e24'}}) 
            ], 10)            
            .append([
                TweenMax.to(jQuery('#milestone-slide-2007-1-left'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#milestone-slide-2007-1-right'), 10, {css:{bottom:0}}),
                TweenMax.to(jQuery('#year-circle-2007-1'), 4, {css:{opacity:1, left:'20%'}}),
                TweenMax.to(jQuery('#text-2007-1'), 4, {css:{opacity:1, bottom:'22%'}}),
                TweenMax.to(jQuery('#line-2007-1'), 4, {css:{opacity:1, bottom:'12%'}})
            ], 10) 

        /* 2007 Slide 1 Out, 2006 Slide 1 In */
            .append([
                TweenMax.to(jQuery('#milestone-slide-2007-1-left'), 15, {css:{top:'-100%'}}),
                TweenMax.to(jQuery('#milestone-slide-2007-1-right'), 15, {css:{bottom:'-100%'}}),
                TweenMax.to(jQuery('#year-circle-2007-1'), 4, {css:{opacity:0, left:'17%'}}),
                TweenMax.to(jQuery('#text-2007-1'), 4, {css:{opacity:0, bottom:'19%'}}),
                TweenMax.to(jQuery('#line-2007-1'), 4, {css:{opacity:0, bottom:'7%'}}),
                TweenMax.to(jQuery('#milestone-slide-2006-1-left'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#milestone-slide-2006-1-right'), 10, {css:{bottom:0}}),
                TweenMax.to(jQuery('#ms-indicator-2007'), 1, {css:{color:'#d7d7d7'}}),
                TweenMax.to(jQuery('#ms-indicator-2007 .indicator-circle'), 1, {css:{backgroundColor:'#d7d7d7'}}),
                TweenMax.to(jQuery('#ms-indicator-2006'), 1, {css:{color:'#ee2e24'}}),
                TweenMax.to(jQuery('#ms-indicator-2006 .indicator-circle'), 1, {css:{backgroundColor:'#ee2e24'}}) 
            ], 10)            
            .append([
                TweenMax.to(jQuery('#milestone-slide-2006-1-left'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#milestone-slide-2006-1-right'), 10, {css:{bottom:0}}),
                TweenMax.to(jQuery('#year-circle-2006-1'), 4, {css:{opacity:1, left:'20%'}}),
                TweenMax.to(jQuery('#text-2006-1'), 4, {css:{opacity:1, top:'20%'}}),
                TweenMax.to(jQuery('#line-2006-1'), 4, {css:{opacity:1, top:'47%'}})
            ], 10)  

        /* 2006 Slide 1 Out, 2005 Slide 1 In */
            .append([
                TweenMax.to(jQuery('#milestone-slide-2006-1-left'), 15, {css:{top:'-100%'}}),
                TweenMax.to(jQuery('#milestone-slide-2006-1-right'), 15, {css:{bottom:'-100%'}}),
                TweenMax.to(jQuery('#year-circle-2006-1'), 4, {css:{opacity:0, left:'17%'}}),
                TweenMax.to(jQuery('#text-2006-1'), 4, {css:{opacity:0, left:'27%'}}),
                TweenMax.to(jQuery('#line-2006-1'), 4, {css:{opacity:0, left:'36%'}}),
                TweenMax.to(jQuery('#milestone-slide-2005-1-left'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#milestone-slide-2005-1-right'), 10, {css:{bottom:0}}),
                TweenMax.to(jQuery('#ms-indicator-2006'), 1, {css:{color:'#d7d7d7'}}),
                TweenMax.to(jQuery('#ms-indicator-2006 .indicator-circle'), 1, {css:{backgroundColor:'#d7d7d7'}}),
                TweenMax.to(jQuery('#ms-indicator-2005'), 1, {css:{color:'#ee2e24'}}),
                TweenMax.to(jQuery('#ms-indicator-2005 .indicator-circle'), 1, {css:{backgroundColor:'#ee2e24'}}) 
            ], 10)
            .append([
                TweenMax.to(jQuery('#milestone-slide-2005-1-left'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#milestone-slide-2005-1-right'), 10, {css:{bottom:0}}),
                TweenMax.to(jQuery('#year-circle-2005-1'), 4, {css:{opacity:1, left:'20%'}}),
                TweenMax.to(jQuery('#text-2005-1'), 4, {css:{opacity:1, left: '53%'}}),
                TweenMax.to(jQuery('#line-2005-1'), 4, {css:{opacity:1, top:'33%'}})
            ], 10)

        /* 2005 Slide 1 Out, 1999 Slide 1 In */
            .append([
                TweenMax.to(jQuery('#milestone-slide-2005-1-left'), 15, {css:{top:'-100%'}}),
                TweenMax.to(jQuery('#milestone-slide-2005-1-right'), 15, {css:{bottom:'-100%'}}),
                TweenMax.to(jQuery('#year-circle-2005-1'), 4, {css:{opacity:0, left:'17%'}}),
                TweenMax.to(jQuery('#text-2005-1'), 4, {css:{opacity:0, left:'56%'}}),
                TweenMax.to(jQuery('#line-2005-1'), 4, {css:{opacity:0, left:'35%'}}),
                TweenMax.to(jQuery('#milestone-slide-1999-1-left'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#milestone-slide-1999-1-right'), 10, {css:{bottom:0}}),
                TweenMax.to(jQuery('#ms-indicator-2005'), 1, {css:{color:'#d7d7d7'}}),
                TweenMax.to(jQuery('#ms-indicator-2005 .indicator-circle'), 1, {css:{backgroundColor:'#d7d7d7'}}),
                TweenMax.to(jQuery('#ms-indicator-1999'), 1, {css:{color:'#ee2e24'}}),
                TweenMax.to(jQuery('#ms-indicator-1999 .indicator-circle'), 1, {css:{backgroundColor:'#ee2e24'}}) 
            ], 10)            
            .append([
                TweenMax.to(jQuery('#milestone-slide-1999-1-left'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#milestone-slide-1999-1-right'), 10, {css:{bottom:0}}),
                TweenMax.to(jQuery('#year-circle-1999-1'), 4, {css:{opacity:1, left:'20%'}}),
                TweenMax.to(jQuery('#text-1999-1'), 4, {css:{opacity:1, top:'20%'}}),
                TweenMax.to(jQuery('#line-1999-1'), 4, {css:{opacity:1, top:'44%'}})
            ], 10)  

        /* 1999 Slide 1 Out, 1998 Slide 1 In */
            .append([
                TweenMax.to(jQuery('#milestone-slide-1999-1-left'), 15, {css:{top:'-100%'}}),
                TweenMax.to(jQuery('#milestone-slide-1999-1-right'), 15, {css:{bottom:'-100%'}}),
                TweenMax.to(jQuery('#year-circle-1999-1'), 4, {css:{opacity:0, left:'17%'}}),
                TweenMax.to(jQuery('#text-1999-1'), 4, {css:{opacity:0, top:'17%'}}),
                TweenMax.to(jQuery('#line-1999-1'), 4, {css:{opacity:0, top:'47%'}}),
                TweenMax.to(jQuery('#milestone-slide-1998-1-left'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#milestone-slide-1998-1-right'), 10, {css:{bottom:0}}),
                TweenMax.to(jQuery('#ms-indicator-1999'), 1, {css:{color:'#d7d7d7'}}),
                TweenMax.to(jQuery('#ms-indicator-1999 .indicator-circle'), 1, {css:{backgroundColor:'#d7d7d7'}}),
                TweenMax.to(jQuery('#ms-indicator-1998'), 1, {css:{color:'#ee2e24'}}),
                TweenMax.to(jQuery('#ms-indicator-1998 .indicator-circle'), 1, {css:{backgroundColor:'#ee2e24'}}) 
            ], 10)            
            .append([
                TweenMax.to(jQuery('#milestone-slide-1998-1-left'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#milestone-slide-1998-1-right'), 10, {css:{bottom:0}}),
                TweenMax.to(jQuery('#year-circle-1998-1'), 4, {css:{opacity:1, left:'20%'}}),
                TweenMax.to(jQuery('#text-1998-1'), 4, {css:{opacity:1, top:'20%'}})
            ], 10)   

        /* 1998 Slide 1 Out, 1995 Slide 1 In */
            .append([
                TweenMax.to(jQuery('#milestone-slide-1998-1-left'), 15, {css:{top:'-100%'}}),
                TweenMax.to(jQuery('#milestone-slide-1998-1-right'), 15, {css:{bottom:'-100%'}}),
                TweenMax.to(jQuery('#year-circle-1998-1'), 4, {css:{opacity:0, left:'23%'}}),
                TweenMax.to(jQuery('#text-1998-1'), 4, {css:{opacity:0, top:'23%'}}),
                TweenMax.to(jQuery('#milestone-slide-1995-1-left'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#milestone-slide-1995-1-right'), 10, {css:{bottom:0}}),
                TweenMax.to(jQuery('#ms-indicator-1998'), 1, {css:{color:'#d7d7d7'}}),
                TweenMax.to(jQuery('#ms-indicator-1998 .indicator-circle'), 1, {css:{backgroundColor:'#d7d7d7'}}),
                TweenMax.to(jQuery('#ms-indicator-1995'), 1, {css:{color:'#ee2e24'}}),
                TweenMax.to(jQuery('#ms-indicator-1995 .indicator-circle'), 1, {css:{backgroundColor:'#ee2e24'}}) 
            ], 10)            
            .append([
                TweenMax.to(jQuery('#milestone-slide-1995-1-left'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#milestone-slide-1995-1-right'), 10, {css:{bottom:0}}),
                TweenMax.to(jQuery('#year-circle-1995-1'), 4, {css:{opacity:1, left:'20%'}}),
                TweenMax.to(jQuery('#text-1995-1'), 4, {css:{opacity:1, top:'34%'}}),
                TweenMax.to(jQuery('#line-1995-1'), 4, {css:{opacity:1, top:'52%'}})
            ], 10)  

        /* 1995 Slide 1 Out, 1995 Slide 2 In */
            .append([
                TweenMax.to(jQuery('#milestone-slide-1995-1-left'), 15, {css:{top:'-100%'}}),
                TweenMax.to(jQuery('#milestone-slide-1995-1-right'), 15, {css:{bottom:'-100%'}}),
                TweenMax.to(jQuery('#year-circle-1995-1'), 4, {css:{opacity:0, left:'17%'}}),
                TweenMax.to(jQuery('#text-1995-1'), 4, {css:{opacity:0, top:'37%'}}),
                TweenMax.to(jQuery('#line-1995-1'), 4, {css:{opacity:0, top:'49%'}}),
                TweenMax.to(jQuery('#milestone-slide-1995-2-left'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#milestone-slide-1995-2-right'), 10, {css:{bottom:0}})
            ], 10)            
            .append([
                TweenMax.to(jQuery('#milestone-slide-1995-2-left'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#milestone-slide-1995-2-right'), 10, {css:{bottom:0}}),
                TweenMax.to(jQuery('#year-circle-1995-2'), 4, {css:{opacity:1, left:'20%'}}),
                TweenMax.to(jQuery('#text-1995-2'), 4, {css:{opacity:1, top:'34%'}})
            ], 10)  
            
            .append([
                TweenMax.to(jQuery('#milestone-slide-1995-2-left'), 15, {css:{top:'-100%'}}),
                TweenMax.to(jQuery('#milestone-slide-1995-2-right'), 15, {css:{bottom:'100%'}}),
                TweenMax.to(jQuery('#year-circle-1995-2'), 4, {css:{opacity:0, top:'5%'}}),
                TweenMax.to(jQuery('#text-1995-2'), 4, {css:{opacity:0, top:'20%'}}),
                TweenMax.to(jQuery('#slide-learn-more'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#ms-indicator-1995'), 1, {css:{color:'#d7d7d7'}}),
                TweenMax.to(jQuery('#ms-indicator-1995 .indicator-circle'), 1, {css:{backgroundColor:'#d7d7d7'}}),
                TweenMax.to(jQuery('#ms-indicator-learn'), 1, {css:{color:'#ee2e24'}}),
                TweenMax.to(jQuery('#ms-indicator-learn .indicator-circle'), 1, {css:{backgroundColor:'#ee2e24'}}) 
            ], 10)           
            .append([
                TweenMax.to(jQuery('.about-cards-milestones'), 10, {css:{opacity:1, marginTop:0}})
            ], 10)         
            .append([
                TweenMax.to(jQuery('.about-cards-philosophy'), 10, {css:{opacity:1, marginTop:0}})
            ], 10)         
            .append([
                TweenMax.to(jQuery('.about-cards-innovation'), 10, {css:{opacity:1, marginTop:0}})
            ], 10)          
            .append([
                TweenMax.to(jQuery('.slide-learn-more'), 20, {css:{top:0}})
            ], 10)
            
            .append([
                TweenMax.to(jQuery('#about-us-scroll-anim'), .5, {css:{position:'relative'}})
            ], 10) 
            .append([
                TweenMax.to(jQuery('#ms-indicator'), .5, {css:{display:'none'}})
            ]) 
        })


    }
});