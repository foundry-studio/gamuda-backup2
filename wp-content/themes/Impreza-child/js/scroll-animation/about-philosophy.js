jQuery(document).ready(function(){
    // init controller
	var controller = new ScrollMagic.Controller();

	// START background switch 
	var firstBackgroundExitAnimation = TweenMax.fromTo('.philosophy-background-one', 1, {
		css: {opacity: 1},
		ease: Cubic.easingIn
	}, {
		css: {opacity: 0},
		ease: Cubic.easingIn
	});

	var firstBackgroundExitScene = new ScrollMagic.Scene({
		triggerElement: '#philosophy-trigger-one',
		triggerHook: 1,
		duration: "50%"
	})
	.setTween(firstBackgroundExitAnimation);
	// .addIndicators({name: "(test)"}); // add indicators (requires plugin)

	var secondBackgroundEnterAnimation = TweenMax.fromTo('.philosophy-background-two', 1, {
		css: {visibility: "hidden", opacity: 0},
		ease: Cubic.easingIn
	}, {
		css: {visibility: "visible", opacity: 1},
		ease: Cubic.easingIn
	});

	var secondBackgroundEnterScene = new ScrollMagic.Scene({
		triggerElement: '#philosophy-trigger-one',
		triggerHook: 1,
		duration: "50%"
	})
	.setTween(secondBackgroundEnterAnimation);
	// .addIndicators({name: "(test)"}); // add indicators (requires plugin)

	var secondBackgroundExitAnimation = TweenMax.fromTo('.philosophy-background-two', 1, {
		css: {opacity: 1},
		ease: Cubic.easingIn
	}, {
		css: {opacity: 0},
		ease: Cubic.easingIn
	});

	var secondBackgroundExitScene = new ScrollMagic.Scene({
		triggerElement: '#philosophy-trigger-two',
		triggerHook: 1,
		duration: "50%"
	})
	.setTween(secondBackgroundExitAnimation);
	// .addIndicators({name: "(test)"}); // add indicators (requires plugin)

	var thirdBackgroundEnterAnimation = TweenMax.fromTo('.philosophy-background-three', 1, {
		css: {visibility: "hidden", opacity: 0},
		ease: Cubic.easingIn
	}, {
		css: {visibility: "visible", opacity: 1},
		ease: Cubic.easingIn
	});

	var thirdBackgroundEnterScene = new ScrollMagic.Scene({
		triggerElement: '#philosophy-trigger-two',
		triggerHook: 1,
		duration: "50%"
	})
	.setTween(thirdBackgroundEnterAnimation);
	// .addIndicators({name: "(test)"}); // add indicators (requires plugin)

	var thirdBackgroundExitAnimation = TweenMax.fromTo('.philosophy-background-three', 1, {
		css: {opacity: 1},
		ease: Cubic.easingIn
	}, {
		css: {opacity: 0},
		ease: Cubic.easingIn
	});

	var thirdBackgroundExitScene = new ScrollMagic.Scene({
		triggerElement: '#philosophy-trigger-three',
		triggerHook: 1,
		duration: "50%"
	})
	.setTween(thirdBackgroundExitAnimation);
	// .addIndicators({name: "(test)"}); // add indicators (requires plugin)

	var fourthBackgroundEnterAnimation = TweenMax.fromTo('.philosophy-background-four', 1, {
		css: {visibility: "hidden", opacity: 0},
		ease: Cubic.easingIn
	}, {
		css: {visibility: "visible", opacity: 1},
		ease: Cubic.easingIn
	});

	var fourthBackgroundEnterScene = new ScrollMagic.Scene({
		triggerElement: '#philosophy-trigger-three',
		triggerHook: 1,
		duration: "50%"
	})
	.setTween(fourthBackgroundEnterAnimation);
	// .addIndicators({name: "(test)"}); // add indicators (requires plugin)

	var fourthBackgroundExitAnimation = TweenMax.fromTo('.philosophy-background-four', 1, {
		css: {opacity: 1},
		ease: Cubic.easingIn
	}, {
		css: {opacity: 0},
		ease: Cubic.easingIn
	});

	var fourthBackgroundExitScene = new ScrollMagic.Scene({
		triggerElement: '#philosophy-trigger-four',
		triggerHook: 1,
		duration: "50%"
	})
	.setTween(fourthBackgroundExitAnimation);
	// .addIndicators({name: "(test)"}); // add indicators (requires plugin)

	var fifthBackgroundEnterAnimation = TweenMax.fromTo('.philosophy-background-five', 1, {
		css: {visibility: "hidden", opacity: 0},
		ease: Cubic.easingIn
	}, {
		css: {visibility: "visible", opacity: 1},
		ease: Cubic.easingIn
	});

	var fifthBackgroundEnterScene = new ScrollMagic.Scene({
		triggerElement: '#philosophy-trigger-four',
		triggerHook: 1,
		duration: "50%"
	})
	.setTween(fifthBackgroundEnterAnimation);
	// .addIndicators({name: "(test)"}); // add indicators (requires plugin)

	var fifthBackgroundExitAnimation = TweenMax.fromTo('.philosophy-background-five', 1, {
		css: {opacity: 1},
		ease: Cubic.easingIn
	}, {
		css: {opacity: 0},
		ease: Cubic.easingIn
	});

	var fifthBackgroundExitScene = new ScrollMagic.Scene({
		triggerElement: '#philosophy-trigger-five',
		triggerHook: 1,
		duration: "50%"
	})
	.setTween(fifthBackgroundExitAnimation);
	// .addIndicators({name: "(test)"}); // add indicators (requires plugin)

	var sixthBackgroundEnterAnimation = TweenMax.fromTo('.philosophy-background-six', 1, {
		css: {visibility: "hidden", opacity: 0},
		ease: Cubic.easingIn
	}, {
		css: {visibility: "visible", opacity: 1},
		ease: Cubic.easingIn
	});

	var sixthBackgroundEnterScene = new ScrollMagic.Scene({
		triggerElement: '#philosophy-trigger-five',
		triggerHook: 1,
		duration: "50%"
	})
	.setTween(sixthBackgroundEnterAnimation);
	// .addIndicators({name: "(test)"}); // add indicators (requires plugin)

	//END background switch

	var moveFrameAnimation = TweenMax.fromTo('.philosophy-background-six', 0, {
		css: {position: "fixed"},
		ease: Cubic.easingIn
	}, {
		css: {position: "absolute", top: "550%"},
		ease: Cubic.easingIn
	});

	var moveFrameScene = new ScrollMagic.Scene({
		triggerElement: '#philosophy-trigger-six',
		triggerHook: 1
		// duration: "1"
	})
	.setTween(moveFrameAnimation);
	// .addIndicators({name: "LOL"});

	controller.addScene([
		firstBackgroundExitScene,
		secondBackgroundEnterScene,
		secondBackgroundExitScene,
		thirdBackgroundEnterScene,
		thirdBackgroundExitScene,
		fourthBackgroundEnterScene,
		fourthBackgroundExitScene,
		fifthBackgroundEnterScene,
		fifthBackgroundExitScene,
		sixthBackgroundEnterScene,
		moveFrameScene
	]);
});

jQuery(window).load(function() {
	// executes when complete page is fully loaded, including all frames, objects and images
	// $(".philosophy-background-two").css({visibility: "hidden"});
	// $(".philosophy-background-three").css({visibility: "hidden"});
	// $(".philosophy-background-four").css({visibility: "hidden"});
	// $(".philosophy-background-five").css({visibility: "hidden"});
	// $(".philosophy-background-six").css({visibility: "hidden"});
});