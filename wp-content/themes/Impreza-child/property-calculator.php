<div class="side-banner desktop">
    <div class="buyers-guide">
        <a href="http://192.168.99.100:32773/buyers-guide/">
        <img src="/wp-content/uploads/2016/07/icon-buyers-guide.png" alt="Buyer's Guide">
        </a>
        <p>Buyer's<br>Guide</p>
    </div>
    <hr class="side-banner-separator">
    <div class="calculator">
        <img src="/wp-content/uploads/2016/07/icon-calculator.png" alt="Calculator">
        <p>Calculator</p>
    </div>
</div>

<div class="side-banner mobile col-xs-12">
    <div class="buyers-guide col-xs-6">
        <a href="http://192.168.99.100:32773/buyers-guide/">
        <img src="/wp-content/uploads/2016/07/icon-buyers-guide.png" alt="Buyer's Guide">
        </a>
        <p>Buyer's<br>Guide</p>
    </div>
    <hr class="side-banner-separator col-xs-6">
    <div class="calculator">
        <img src="/wp-content/uploads/2016/07/icon-calculator.png" alt="Calculator">
        <p>Calculator</p>
    </div>
</div>

<div class="calculator-overlay"></div>
<div class="row calculator-popup">
    <div class="row calculator-box">
        <div class="col-md-12 calculator-title">
            <h2>MORTGAGE CALCULATOR</h2>
            <span class="glyphicon glyphicon-remove-circle close-calculator" aria-hidden="true"></span>
        </div>
        <div class="row calculator-row">
            <div class="col-md-6 calculator-item">
                <h3>Property Price <span>(MYR)</span></h3>
            </div>
            <div class="col-md-6 calculator-input">
                <input class="form-control input-lg property-price" type="number" min="0">
            </div>
        </div>
        <div class="row calculator-row">
            <div class="col-md-6 calculator-item">
                <h3>Loan Period <span>(Years)</span></h3>
            </div>
            <div class="col-md-6 calculator-input">
                <select class="form-control input-lg loan-period">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                    <option value="13">13</option>
                    <option value="14">14</option>
                    <option value="15">15</option>
                    <option value="16">16</option>
                    <option value="17">17</option>
                    <option value="18">18</option>
                    <option value="19">19</option>
                    <option value="20">20</option>
                    <option value="21">21</option>
                    <option value="22">22</option>
                    <option value="23">23</option>
                    <option value="24">24</option>
                    <option value="25">25</option>
                    <option value="26">26</option>
                    <option value="27">27</option>
                    <option value="28">28</option>
                    <option value="29">29</option>
                    <option value="30">30</option>
                    <option value="31">31</option>
                    <option value="32">32</option>
                    <option value="33">33</option>
                    <option value="34">34</option>
                    <option value="35">35</option>
                    <option value="36">36</option>
                    <option value="37">37</option>
                    <option value="38">38</option>
                    <option value="39">39</option>
                    <option value="40">40</option>
                </select>
            </div>
        </div>
        <div class="row calculator-row">
            <div class="col-md-6 calculator-item">
                <h3>Expected Interest Rate <span>(%)</span></h3>
            </div>
            <div class="col-md-6 calculator-input">
                <input class="form-control input-lg interest-rate" type="number" min="0">
            </div>
        </div>
        <div class="row calculator-row">
            <div class="col-md-6 calculator-item">
                <h3>Down Payment <span>(MYR)</span></h3>
            </div>
            <div class="col-md-6 calculator-input">
                <input class="form-control input-lg down-payment" type="number" min="0">
            </div>
        </div>
        <div class="row calculator-btn">
            <div class="col-md-6 reset-btn">
                <button class="calculator-reset" type="reset" onclick="propertyReset()">Reset</button>
            </div>
            <div class="col-md-6 submit-btn">
                <button class="calculator-submit" type="Submit" onclick="propertyCalculator()">Calculate</button>
            </div>
        </div>
        <div class="row disclaimer-text">
            <p><span>Disclaimer: </span>The above is an estimation of the repayment amount.</p>
        </div>
    </div>
    <div class="row result-box">
        <div class="row result-box-seperator">
            <hr class="col-md-12">
        </div>
        <div class="row calculator-row">
            <div class="col-md-6 calculator-item">
                <h3>Monthly Installment <span>(MYR)</span></h3>
            </div>
            <div class="col-md-6 calculator-input">
                <input class="form-control input-lg monthly-installment" type="text" readonly>
            </div>
        </div>
        <div class="row calculator-row">
            <div class="col-md-6 calculator-item">
                <h3>Total Interest <span>(MYR)</span></h3>
            </div>
            <div class="col-md-6 calculator-input">
                <input class="form-control input-lg total-interest" type="text" readonly>
            </div>
        </div>
        <div class="row calculator-row">
            <div class="col-md-6 calculator-item">
                <h3>Monthly Average Interest <span>(MYR)</span></h3>
            </div>
            <div class="col-md-6 calculator-input">
                <input class="form-control input-lg monthly-average-interest" type="text" readonly>
            </div>
        </div>
    </div>
</div>