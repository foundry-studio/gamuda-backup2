<?php
// Template Name:  Innovation Template
get_header(); ?>
<div class="page-breadcrumbs slider-breadcrumbs">
    <p><a href="#">Home</a> <span>/</span> <a href="#">About Us</a> <span>/</span> Innovation</p>
</div>

<div id="inno-indicator" class="scroll-indicator">
    <ul>
        <li id="inno-indicator-home"><a href="#">Home<div class="indicator-circle"></div></a></li>
        <li id="inno-indicator-inno"><a href="#">Innovation &amp; Creativity<div class="indicator-circle"></div></a></li>
        <li id="inno-indicator-master"><a href="#">Masterplanning<div class="indicator-circle"></div></a></li>
        <li id="inno-indicator-hydro"><a href="#">Hydrology<div class="indicator-circle"></div></a></li>
        <li id="inno-indicator-terrain"><a href="#">Terrain<div class="indicator-circle"></div></a></li>
        <li id="inno-indicator-green"><a href="#">Greenscapes<div class="indicator-circle"></div></a></li>
        <li id="inno-indicator-learn"><a href="#">Learn More<div class="indicator-circle"></div></a></li>
    </ul>
</div>

<div class="section-view">
    <div id="section-controller" class="section-wrap section">
        <div class="section section1"></div>
        <div class="section section2"></div>
        <div class="section section3"></div>
        <div class="section section4"></div>
    </div>
    <div id="section-anim">
        
        <!-- Innovation & Creativity -->
        <div id="inno-home" class="section">
            <div id="text-inno-home" class="text-box">
                <h3>INNOVATION</h3>
                <p>To steer technology towards the greater good of the community.</p>
            </div>
            <div class="section bg-behind"></div>
            <div class="section bg-front"></div>
            <img id="inno-inno-chart" src="/wp-content/uploads/2016/08/innovation-creativity-chart.png">
        </div>
        
        <!-- Masterplanning -->
        <div id="inno-master" class="section">
            <div id="inno-master-left" class="pin-frame bg-wrap-left"></div>
            <div id="inno-master-right" class="pin-frame bg-wrap-right"></div>
            <div id="inno-master-front" class="pin-frame"></div>
            <div id="text-inno-master" class="desc-box container">
                <div class="desc-title">
                    <h5 style="color:#ee2e24">MASTERPLANNING</h5>
                </div>
                <div class="desc-color col-md-9">
                    <div class="col-md-8">
                        <p>Benchmark in natural topography layout</p>
                        <p style="margin-top:25px;padding-left:30px;padding-right:30px;border-left:1px solid #fff">We believe that sustainable masterplanning is key to creating communities which are able to grow and expand organically in terms of population and activities. It promotes the sharing of social infrastructure and amenities that are planned for future growth, enhances economic investment, as well as creates a setting where nature can thrive. That is why we make livability and sustainability the cornerstones of each of our developments.</p>
                    </div>
                    <div class="col-md-4">
                        <h6>CONVENTIONAL METHOD</h6>
                        <p>- Flatten the ground</p>
                        <p>- Fills the low lands</p>
                    </div>
                </div>
                <div class="desc-white col-md-3">
                    <h6>GAMUDA METHOD</h6>
                    <img src="/wp-content/uploads/2016/08/innovation-master-desc-img.jpg">
                    <p>- Utilise the low lands as fairways</p>
                    <p>- Minimum cutting of the hill/shopes</p>
                </div>
            </div>
            <div id="text-inno-master-sm" class="desc-box">
                <h6>HIGHLIGHTS</h6>
                <ul>
                    <li>Density of 6 unites per acre</li>
                    <li>Approximately 70% of homes are built with north-south orientation</li>
                    <li>A sustainable mix of commercial (10%), residential (55%), industrial (20% and amenities (15%)</li>
                    <li>Distance of neighbourhoods to amenities	(approximately 5-10 minutes’ walk)</li>
                </ul>
            </div>
        </div>
        
        <!-- Hydrology -->
        <div id="inno-hydro" class="section">
            <div id="inno-hydro-left" class="pin-frame bg-wrap-left"></div>
            <div id="inno-hydro-right" class="pin-frame bg-wrap-right"></div>
            <div id="text-inno-hydro" class="desc-box container">
                <div class="desc-title">
                    <h5 style="color:#00a4c7">HYDROLOGY</h5>
                </div>
                <div class="desc-color col-md-6">
                    <p style="color:#1e1e1e;padding-left:30px;padding-right:30px;border-left:1px solid #1e1e1e">Good water quality has always been a vital component of 
a healthy and sustainable community. We recognise the importance of a clean water body, supported by a network of surrounding green fingers. We are continuously exploring and innovating our approach to urban  hydrology, making it an integral part of the way we plan our developments.</p>
                </div>
                <div class="desc-white col-md-6">
                    <div class="col-md-6">
                        <img src="/wp-content/uploads/2016/08/innovation-hydro-desc-img.jpg">
                    </div>
                    <div class="col-md-6">
                        <h6>GAMUDA METHOD</h6>
                        <p>‘Floating Pile Raft’ building platform foundation system that combined pile and raft platfoorm</p>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>
<?php get_footer();
// Omit closing PHP tag to avoid "Headers already sent" issues.