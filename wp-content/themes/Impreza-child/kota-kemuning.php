<?php
// Template Name:  Kota Kemuning Template
get_header(); ?>

<div id="kk-indicator" class="scroll-indicator">
    <ul>
        <li style="margin-bottom:55px;"><img src="/wp-content/uploads/2016/08/slider-logo-kota-kemuning.png" alt="Kota Kemuning Logo" class="img-responsive"></li>
        <li id="kk-indicator-home" data-target="#kotakemuningHome"><div id="iHome" class="indicator-circle indicator-gradient" style="border:#0c5b06 solid 1px"></div><p>HOME</p></li>
        <li id="kk-indicator-overview" data-target="#firstTrigger"><div id="iOverview" class="indicator-circle indicator-gradient" style="border:#4a6b09 solid 1px"></div><p>OVERVIEW</p></li>
        <li id="kk-indicator-concept" data-target="#thirdTrigger"><div id="iConcept" class="indicator-circle indicator-gradient" style="border:#5b840b solid 1px"></div><p>CONCEPT &amp; DESIGN</p></li>
        <li id="kk-indicator-vision" data-target="#fifthTrigger"><div id="iVision" class="indicator-circle indicator-gradient" style="border:#6a9a0d solid 1px"></div><p>VISION</p></li>
        <li id="kk-indicator-map" data-target="#sixthTrigger"><div id="iMap" class="indicator-circle indicator-gradient" style="border:#98b10d solid 1px"></div><p>MAP</p></li>
        <li id="kk-indicator-happenings" data-target="#kk-happenings"><div id="iHappenings" class="indicator-circle indicator-gradient" style="border:#a7c20e solid 1px"></div><p>HAPPENINGS</p></li>
        <li id="kk-indicator-gallery" data-target="#kk-gallery"><div id="iGallery" class="indicator-circle indicator-gradient" style="border:#b6d509 solid 1px"></div><p>GALLERY</p></li>
        <li id="kk-indicator-more" data-target="#section-next-prev-proj"><div id="iMore" class="indicator-circle indicator-gradient" style="border:#d5c909 solid 1px"></div><p>OTHER DEVELOPMENTS</p></li>
    </ul>
</div>


<div class="custom-container kota-kemuning-desktop" style="width:100%;">
    <div class="container-fluid full first-container" id="kotakemuningHome">
        <div class="mouse">
            <div class="mouse-icon">
                <span class="mouse-wheel"></span>
            </div>
        </div>
        <div class="page-breadcrumbs slider-breadcrumbs" style="position:absolute">
            <p><a href="http://128.199.89.40/">Home</a> <span>/</span> <a href="./our-developments/">Our Developments</a> <span>/</span> <a href="./our-developments/#ourdev-land-fullysold-section">Fully Sold Developments</a> <span>/</span> Kota Kemuning</p>
        </div>
        
    </div>

    <div class="container-fluid full second-container" id="firstTrigger">
        <div class="text-container header-one">
            <p>OVERVIEW</p>
        </div>

        <div class="text-container text-one">
            <p>AN AWARD-WINNING<br>1,854-ACRE INTERGRATED<br>TOWNSHIP IN SHAH ALAM</p>
        </div>
    </div>

    <div class="container-fluid full third-container" id="secondTrigger">
        <div class="half first-half">
            <div class="circle firstTop"></div>
            <div class="topMessageOne container">
                <p>Kota Kemuning was first envisioned by DRB-HICOM Berhad and<br>Gamuda Berhad some two decades ago. These affiliates foresaw<br>the potential for something that had never been done before - the<br>creation of an innovative masterplan that focused on the<br>long-term, sustainable growth of the community.<br><br>
				
                In 1995, these affiliates formed Hicom-Gamuda Development<br>Sdn Bhd, a strategic partnership that delivered the best<br>development solution in every aspect, creating a landmark of<br>distinction for the community.</p>
            </div>
        </div>
        <div class="half second-half">
            <div class="circle firstBottom"></div>
            <div class="bottomMessageOne container">
                <p>Kota Kemuning is award-winning 1,854-acre integrated township in<br>Shah Alam that epitomises quality living. This self-sustaining<br>township contains a well-balanced mix of residential and commercial<br>developments supported by essential amenities, as well as a network<br>of expressways. All these elements together with others within its<br>innovative masterplan, ensure residents get to enjoy a holistic lifestyle.<br><br>
				
                While each residential precinct in this extraordinary township is<br>crafted with its own distinct character, the township is still able to<br>maintain a 'harmonious living environment'. With its emphasis on<br>quality master planning and innovative solutions that deliver a quality<br>lifestyle, Kota Kemuning is today one of the most sought after<br>townships in the Klang Valley.</p>
            </div>
        </div>
    </div>

    <div class="container-fluid full fourth-container" id="thirdTrigger">
        <div class="half first-half2">
            <div class="secondCircleTitle">
                <div class="header-three">CONCEPT &amp; DESIGN</div>
                <img src="/wp-content/uploads/2016/08/half-circle.png">
            </div>
            <div class="secondCircle secondTop"></div>
            <div class="topMessageTwo container">
                <p class="messageTwoTopHeader header-two">A DEFINING<br>NEW BENCHMARK FOR<br>SUSTAINABLE COMMUNITIES</p>
                <p class="messageTwoTopText text-two">Kota Kemuning is an award-winning intergrated<br>
                township featuring unbridled green living accentuated<br>
                by a harmonious blend of modern homes and<br>
                exceptional public amenities.</p>
            </div>
        </div>
        <div class="half second-half2">
            <div class="secondCircle secondBottom"></div>
            <div class="bottomMessageTwo container">
                <p class="messageTwoTopHeader header-two">CREATIVITY IN<br>ENVIRONMENTAL<br>ARCHITECTURE</p>
                <p class="messageTwoTopText"><br>Our effort to enhance nature's beauty is driven by our<br>
                belief of creating eco-friendly environments where<br>
                communities can learn to live with the delicate beauty<br>
                of nature. We stand firm to the rule of enhancing the<br>
                environment and this is a critical factor during the<br>
                concept planning and design stages.</p>
            </div>
        </div>
    </div>

    <div class="container-fluid full third-container" id="fourthTrigger">
        <div class="row half first-half3">
            <!--<div class="topMessageThree container">
                <p class="messageThreeTopHeader header-three">CREATIVITY IN ENVIRONMENTAL<br>ARCHITECTURE</p>
                <p class="messageThreeTopText">Our effort to enhance nature's beauty is driven by our belief of creating<br>
                eco-friendly environments where communities can learn to live with<br>
                the delicate beauty of nature.</p>
            </div> -->
        </div>
        <div class="row half second-half3">
            <div class="container containerNew">		
				<div class="text-container header-one">
					<p style="color:#35804a">AWARDS &amp; CERTIFICATION</p>
				</div>			
                <div class="col-md-12 kk-awards-section">
					<ul>
						<li><img src="/wp-content/uploads/2016/08/kota.png" alt="The EDGE Property Development Excellence Award 2015 Gamuda Land"></li>
						<li><img src="/wp-content/uploads/2016/08/honour.png" alt="MLAA Honour Award Gamuda Land"></li>
						<li><img src="/wp-content/uploads/2016/08/conquas.png" alt="Conquas Rared Gamuda Homes Gamuda Land"></li>
					</ul>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid full fourth-container" id="fifthTrigger">
        <div class="text-container header-one" id="secondMainHeader">
            <p>VISION</p>
        </div>

        <div class="text-container text-one" id="secondMainText">
            <p>AN AWARD-WINNING<br>1,854-ACRE INTERGRATED<br>TOWNSHIP IN SHAH ALAM</p>
        </div>
    </div>
	
	<div class="container-fluid full fifth-container" id="sixthTrigger">
        <div class="text-container header-one" id="thirdMainHeader">
            <p>MAP</p>
        </div>
        <img id="Image-Maps-Com-image-maps-2016-09-23-005131" src="/wp-content/themes/Impreza-child/img/kota-kemuning-map.png" border="0" width="1920" height="1016" orgWidth="1920" orgHeight="1016" usemap="#image-maps-2016-09-23-005131" alt="" />
		<map name="image-maps-2016-09-23-005131" id="ImageMapsCom-image-maps-2016-09-23-005131">
			<area id="golf-course-play-btn" alt="Golf Course Video" shape="rect" coords="377,492,451,567" style="outline:none;" target="_self" data-toggle="modal" data-target="#kkModalVideo1"     />
			<area id="golf-course-info-btn" alt="Golf Course Information" shape="rect" coords="451,494,525,569" style="outline:none;" target="_self" data-toggle="modal" data-target="#kk-golf-more-modal"     />
			<area id="recreational-parks-play-btn" alt="Recreational Parks Video" shape="rect" coords="1331,364,1405,434" style="outline:none;" target="_self" data-toggle="modal" data-target="#kkModalVideo2"     />
			<area id="recrational-parks-info-btn" alt="Recreational Parks Information" shape="rect" coords="1408,361,1477,431" style="outline:none;" target="_self" data-toggle="modal" data-target="#kk-recreational-more-modal"     />
			<area id="residential-cluster-play-btn" alt="Residential Cluster Video" shape="rect" coords="1354,604,1423,674" style="outline:none;" target="_self" data-toggle="modal" data-target="#kkModalVideo3"     />
			<area id="residential-cluster-info-btn" alt="Residential Cluster Information" shape="rect" coords="1428,603,1497,673" style="outline:none;" target="_self" data-toggle="modal" data-target="#kk-residential-more-modal"     />
			<area shape="rect" coords="1918,1014,1920,1016" alt="Image Map" style="outline:none;" title="Image Map" href="http://www.image-maps.com/index.php?aff=mapped_users_0" />
		</map>
<!--
		<div class="kk-three-circles">
			<img src="/wp-content/uploads/2016/08/kota-kemuning-map-circle.png" alt="Kota-Kemuning">
			<div class="arrow-bg arrow-golf-course">
				<ul>
					<li><div class="trigger-modal-btn video_thumbnail" data-toggle="modal" data-target="#kkModalVideo1"><img src="/wp-content/uploads/2016/08/btn-video-play.png" alt="Kota Kemuning Golf Course Video Button"></div></li>
					<li><div class="trigger-modal-btn" data-toggle="modal" data-target="#kk-golf-more-modal"><img src="/wp-content/uploads/2016/08/btn-know-more.png" alt="Kota Kemuning Golf Course Know More Button"></div></li>
					<li style="margin-left:20px;">GOLF<br>COURSE</li>
				</ul>
			</div>
			<div class="arrow-bg arrow-recreational-parks">
				<ul>
					<li><div class="trigger-modal-btn video_thumbnail" data-toggle="modal" data-target="#kkModalVideo2"><img src="/wp-content/uploads/2016/08/btn-video-play.png" alt="Kota Kemuning Recreational Park Video Button"></div></li>
					<li><div class="trigger-modal-btn " data-toggle="modal" data-target="#kk-recreational-more-modal"><img src="/wp-content/uploads/2016/08/btn-know-more.png" alt="Kota Kemuning Recreational Park More Button"></div></li>
					<li>RECREATIONAL<br>PARKS</li>
				</ul>
			</div>
			<div class="arrow-bg arrow-residential-cluster">
				<ul>
					<li><div class="trigger-modal-btn video_thumbnail" data-toggle="modal" data-target="#kkModalVideo3"><img src="/wp-content/uploads/2016/08/btn-video-play.png" alt="Kota Kemuning Residential Cluster Video Button"></div></li>
					<li><div class="trigger-modal-btn " data-toggle="modal" data-target="#kk-residential-more-modal"><img src="/wp-content/uploads/2016/08/btn-know-more.png" alt="Kota Kemuning Residential Cluster More Button"></div></li>
					<li>RESIDENTIAL<br>CLUSTER</li>
				</ul>
			</div>
		</div>
-->
	</div>
	
    <div class="modal fade modalVideo" id="kkModalVideo1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body text-center"> 
					<ul class="video-modal-buttons">
						<li><div class="trigger-modal-btn" data-toggle="modal" data-target="#kk-golf-more-modal" data-dismiss="modal" aria-label="Close"><img src="/wp-content/uploads/2016/08/btn-know-more.png" alt="Know More Button"></div></li>
						<li><button type="button" class="close" data-dismiss="modal" aria-label="Close" style="text-transform: inherit;color:#fff;opacity:1"><img src="/wp-content/uploads/2016/08/btn-modal-close.png" alt="Close Button"></button>  </li>
					</ul>
					    
                    <video id="video1" controls = "controls" class="modal_video_element" width="100%" allowfullscreen>
                    	<source src="http://103.8.25.232/uploads/golf.mp4" type="video/mp4" allowfullscreen />
                    </video>
                </div>
            </div>
        </div>
    </div>
	
    <div class="modal fade modalVideo" id="kkModalVideo2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body text-center"> 
					<ul class="video-modal-buttons">
						<li><div class="trigger-modal-btn" data-target="#kk-recreational-more-modal" data-toggle="modal" data-dismiss="modal" aria-label="Close"><img src="/wp-content/uploads/2016/08/btn-know-more.png" alt="Know More Button"></div></li>
						<li><button type="button" class="close" data-dismiss="modal" aria-label="Close" style="text-transform: inherit;color:#fff;opacity:1"><img src="/wp-content/uploads/2016/08/btn-modal-close.png" alt="Close Button"></button>  </li>
					</ul>
					    
                    <video id="video1" controls = "controls" class="modal_video_element" width="100%" allowfullscreen>
                    	<source src="http://103.8.25.232/uploads/commercial.mp4" type="video/mp4" allowfullscreen />
                    </video>
                </div>
            </div>
        </div>
    </div>
	
    <div class="modal fade modalVideo" id="kkModalVideo3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body text-center"> 
					<ul class="video-modal-buttons">
						<li><div class="trigger-modal-btn" data-toggle="modal" data-target="#kk-residential-more-modal" data-dismiss="modal" aria-label="Close"><img src="/wp-content/uploads/2016/08/btn-know-more.png" alt="Know More Button"></div></li>
						<li><button type="button" class="close" data-dismiss="modal" aria-label="Close" style="text-transform: inherit;color:#fff;opacity:1"><img src="/wp-content/uploads/2016/08/btn-modal-close.png" alt="Close Button"></button>  </li>
					</ul>
					    
                    <video id="video1" controls = "controls" class="modal_video_element" width="100%" allowfullscreen>
                    	<source src="http://103.8.25.232/uploads/residential.mp4" type="video/mp4" allowfullscreen />
                    </video>
                </div>
            </div>
        </div>
    </div>
	
	<div class="modal fade modal-info" id="kk-golf-more-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-body">
					<div class="container">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="text-transform: inherit;color:#333;opacity:1">Close  <i aria-hidden="true" class="fa fa-times-circle-o"></i></button>
						<div class="header-one">
							<p style="color:#9bba08;margin-bottom:50px;">GOLF<br>COURSE</p>
						</div>
						<div class="row">
							<p class="col-lg-6 col-md-6 col-sm-7">The hallmark of the Kota Kemuning township, the Kota Permai Golf and Country Club is the perfect getaway for golf and recreational pleasures. Boasting an international standard 18-hole golf course designed by the renowned Ross Watson, this award-winning course offers arguably, the best golfing experience in Malaysia and ranks among Asia’s premier golfing destinations. Aside from its immaculately maintained greens, its premium clubhouse provides the community a full range of facilities and amenities.</p>
						</div>
						<div class="w-btn btn-popup-watchvideo col-lg-3 col-md-3"  style="font-size: 15px ;margin-top:50px;" data-toggle="modal" data-target="#kkModalVideo1" data-dismiss="modal" aria-label="Close"><span class="w-btn-label">Watch Video</span></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="modal fade modal-info" id="kk-recreational-more-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-body">
					<div class="container">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="text-transform: inherit;color:#333;opacity:1">Close  <i aria-hidden="true" class="fa fa-times-circle-o"></i></button>
						<div class="header-one">
							<p style="color:#066c68;margin-bottom:50px;">RECREATIONAL<br>PARKS</p>
						</div>
						<div class="row">
							<p class="col-lg-10 col-md-10 col-sm-12">One of Kota Kemuning’s finest attributes is its vast tracks of greens, parks, lakes and landscaped gardens. More than 45% of its land is dedicated to greens, parks and lakes providing the community with a healthy and harmonious environment. The vast track of greens includes an eight-kilometre continuous walkway that meanders throughout the neighbourhood linking parks and precincts.<br><br>

							<strong>CENTRAL LAKE</strong><br><br>
							The signature 22-acre Central Lake and surround 25-acre park provide the perfect environment for the community to relax and unwind. For residents, there is nothing more soothing than the panoramic views of the lake and the surrounding greens. Residents can also take a stroll along Lakeside Drive that offers great vistas of the serene lake.<br><br>

							<strong>WETLAND PARK</strong><br><br>
							The 22-acre Wetland Park integrates a parkland environment with a touch of 'tropical wilderness' into this resort-styled township. Originally, swampland, Gamuda Land transformed this once unproductive area into a wetland park, making it the only wetlands to be built by a housing developer as a recreational park for the community. This award-winning park is today a habitat for a variety of birds species and local flora and fauna.<br><br>

							<strong>HILL PARK</strong><br><br>
							Set atop a hill, the lush three-acre Hill Park serves as a park for community recreation and relaxation activities. Built around existing greens and trees, the award-winning Hill Park encompasses attractions such as a reflexology path, timber lookout deck, gazebo for relaxation and walking trail that challenges fitness levels.</p>
						</div>
						<div class="w-btn btn-popup-watchvideo col-lg-3 col-md-3"  style="font-size: 15px ;margin-top:50px;" data-toggle="modal" data-target="#kkModalVideo2" data-dismiss="modal" aria-label="Close"><span class="w-btn-label">Watch Video</span></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="modal fade modal-info" id="kk-residential-more-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-body">
					<div class="container">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="text-transform: inherit;color:#333;opacity:1">Close  <i aria-hidden="true" class="fa fa-times-circle-o"></i></button>
						<div class="header-one">
							<p style="color:#409fb7;margin-bottom:50px;">RESIDENTIAL<br>CLUSTER</p>
						</div>
						<div class="row">
							<p class="col-lg-6 col-md-6 col-sm-7">Within Kota Kemuning’s residential cluster is Kota Kemuning Hills, an exclusive gated residential precinct set atop the highest point of the township and offering magnificent views of the golf and country club. The homes and facilities here nestle amidst the undulating terrain and rolling greens, much of which has been preserved to ensure the integrity of the development’s hilltop charm.</p>
						</div>
						<div class="w-btn btn-popup-watchvideo col-lg-3 col-md-3"  style="font-size: 15px ;margin-top:50px;" data-toggle="modal" data-target="#kkModalVideo3" data-dismiss="modal" aria-label="Close"><span class="w-btn-label">Watch Video</span></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	
    <div>
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<?php the_content(); ?>
<?php endwhile; endif; ?>
    </div>
</div>
<?php get_footer();
// Omit closing PHP tag to avoid "Headers already sent" issues.