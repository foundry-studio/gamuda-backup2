<?php
// Template Name:  Milestones Mobile Template 
get_header(); ?>

<div class="mobile-main-nav mobile-main-nav-all">
    <div class="mobile-main-nav-header">
        <a class="mobile-main-nav-logo" href="http://128.199.89.40/"><img src="http://128.199.89.40/wp-content/uploads/2016/07/logo-1.png" alt="Gamuda Land Logo"></a>
		<div class="mobile-main-nav-close"><img src="/wp-content/uploads/2016/09/nav-close-btn.png"></div>
    </div>
    <ul class="list-unstyled">
        <li><a href="http://128.199.89.40/about-us/">ABOUT US</a></li>
        <li class="mobile-sub-nav-open">
            <a href="http://128.199.89.40/our-developments/">OUR DEVELOPMENTS</a>
            <i class="fa fa-angle-right" aria-hidden="true"></i>
        </li>
        <li><a href="http://128.199.89.40/happenings/">HAPPENINGS</a></li>
        <li><a href="http://128.199.89.40/contact-us/">CONTACT US</a></li>
    </ul>
    <a class="mobile-main-nav-register" href="https://webreg.gamudaland.com.my/contact/_publicwebregistration/" target="_blank">REGISTER</a>
    <div class="mobile-main-nav-sns">
        <a href="https://www.facebook.com/GamudaLand/?fref=ts&ref=br_tf"><i class="fa fa-facebook" aria-hidden="true"></i></a>
        <a href="https://www.instagram.com/gamudaland/"><i class="fa fa-instagram" aria-hidden="true"></i></a>
    </div>
</div>

<div class="mobile-main-nav mobile-main-nav-projects">

    <div class="mobile-main-nav-header">
        <a class="mobile-main-nav-logo" href="http://128.199.89.40/"><img src="http://128.199.89.40/wp-content/uploads/2016/07/logo-1.png" alt="Gamuda Land Logo"></a>
		<div class="mobile-main-nav-close"><img src="/wp-content/uploads/2016/09/nav-close-btn.png"></div>
    </div>
    <div class="mobile-main-nav-projects-list">
        <ul class="list-unstyled">
            <li class="mobile-sub-nav-close">
                <b>FEATURED</b> DEVELOPMENTS
                <i class="fa fa-angle-left" aria-hidden="true"></i>
            </li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/highpark-suites/">HIGHPARK SUITES<span>Kelana Jaya</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/the-robertson/">THE ROBERTSON<span>Kuala Lumpur</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/jade-hills/">JADE HILLS<span>Kajang</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/horizon-hills/">HORIZON HILLS<span>Iskandar Puteri</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/bukit-bantayan/">BUKIT BANTAYAN<span>Kota Kinabalu</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/gamuda-walk/">GAMUDA WALK<span>Shah Alam</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/gem-residences/">GEM RESIDENCES<span>Toa Payoh</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/661-chapel-st/">661 CHAPEL ST.<span>Melbourne</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/gamuda-city/">GAMUDA CITY<span>Hanoi</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/celadon-city/">CELADON CITY<span>Ho Chi Minh</span></a></li>
        </ul>
        <ul class="list-unstyled">
            <li><b>UPCOMING</b> DEVELOPMENTS</li>
            <li><a href="http://gamudaland.com.my/kundangestates/">KUNDANG ESTATES<span>Kuang</span></a></li>
            <li><a href="#" class="navi-disable">GAMUDA GARDENS<span>Kuang</span></a></li>
            <li><a href="http://gamudaland.com.my/twentyfive7/">TWENTYFIVE7<span>Kota Kemuning</span></a></li>
        </ul>
        <ul class="list-unstyled">
            <li><b>FULLY SOLD</b> DEVELOPMENTS</li>
            <li><a href="http://128.199.89.40/our-developments/fully-sold-developments/kota-kemuning/">KOTA KEMUNING<span>Shah Alam</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/fully-sold-developments/bandar-botanic/">BANDAR BOTANIC<span>Klang</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/fully-sold-developments/valencia/">VALENCIA<span>Sungai Buloh</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/fully-sold-developments/madge-mansions/">MADGE MANSIONS<span>Kuala Lumpur</span></a></li>
        </ul>
        <ul class="list-unstyled">
            <li><b>GOLF &amp; COUNTRY</b> CLUBS</li>
            <li><a href="http://www.kotapermai.com.my/" target="_blank">KOTA PERMAI<span>Golf &amp; Country Club</span></a></li>
            <li><a href="http://hhgcc.com.my/" target="_blank">HORIZON HILLS<span>Golf &amp; Country Club</span></a></li>
            <li><a href="http://www.botanicresortclub.com.my/" target="_blank">BOTANIC<span>Resort Club</span></a></li>
        </ul>
    </div>
</div>

<div id="mobile-pages" class="mobile-swipe-page mobile-milestones mobile-page">
<div class="container-fluid document">
    <div class="row mobile-header-test"></div>

    <div class="row mobile-swipe-nav milestones-swipe-nav">
        <ul class="list-unstyled list-inline">
            <li>
                <a href="#mobile-milestones-1995">1995</a>
                <div class="mobile-swipe-nav-indicator"></div>
            </li>
            <li>
                <a href="#mobile-milestones-learn-more">LEARN<br>MORE</a>
                <div class="mobile-swipe-nav-indicator"></div>
            </li>
            <li class="active">
                <a href="#mobile-milestones-home">HOME</a>
                <div class="mobile-swipe-nav-indicator"></div>
            </li>
            <li>
                <a href="#mobile-milestones-2016">2016</a>
                <div class="mobile-swipe-nav-indicator"></div>
            </li>
            <li>
                <a href="#mobile-milestones-2015">2015</a>
                <div class="mobile-swipe-nav-indicator"></div>
            </li>
            <li>
                <a href="#mobile-milestones-2013">2013</a>
                <div class="mobile-swipe-nav-indicator"></div>
            </li>
            <li>
                <a href="#mobile-milestones-2012">2012</a>
                <div class="mobile-swipe-nav-indicator"></div>
            </li>
            <li>
                <a href="#mobile-milestones-2011">2011</a>
                <div class="mobile-swipe-nav-indicator"></div>
            </li>
            <li>
                <a href="#mobile-milestones-2018">2008</a>
                <div class="mobile-swipe-nav-indicator"></div>
            </li>
            <li>
                <a href="#mobile-milestones-2007">2007</a>
                <div class="mobile-swipe-nav-indicator"></div>
            </li>
            <li>
                <a href="#mobile-milestones-2005">2005</a>
                <div class="mobile-swipe-nav-indicator"></div>
            </li>
            <li>
                <a href="#mobile-milestones-2001">2001</a>
                <div class="mobile-swipe-nav-indicator"></div>
            </li>
            <li>
                <a href="#mobile-milestones-2000">2000</a>
                <div class="mobile-swipe-nav-indicator"></div>
            </li>
        </ul>
        <hr>
    </div>

    <div class="row mobile-swipe-wrap">
        <!-------------------- Main Cover -------------------->
        <div class="mobile-swipe-cover mobile-milestones-main" id="mobile-milestones-home">
            <div class="mobile-swipe-cover-title">
                <h1 class="lg-font" style="color:#fff">OUR MILESTONES</h1>
            </div>
            <!--<img class="scroll-icon" src="/wp-content/themes/Impreza-child/img/scroll-icon.png" alt="Please Scroll To Read More">-->
			<div class="mouse">
				<div class="mouse-icon">
					<span class="mouse-wheel"></span>
				</div>
			</div>
        </div>

        <!-------------------- 2016 Content -------------------->
        <div class="mobile-swipe-cover mobile-milestones-gem" id="mobile-milestones-2016">
            <div class="left-top-content width60">
                <h2 class="md-font">GEM RESIDENCES MARKS GAMUDA LAND'S MAIDEN FORAY INTO SINGAPORE</h2>
            </div>
        </div>

        <!-------------------- 2016 Content -------------------->
        <div class="mobile-swipe-cover mobile-milestones-kundang">
            <div class="left-top-content width60">
                <h2 class="md-font">KUNDANG ESTATES, THE EMBODIMENT OF MODERN COUNTRYSIDE LIVING, IS LAUNCHED</h2>
            </div>
        </div>

        <!-------------------- 2016 Content -------------------->
        <div class="mobile-swipe-cover mobile-milestones-30000-houses">
            <div class="center-top-content">
                <h2 class="xlg-font">30,000 HOUSES<br>HANDED OVER</h2>
            </div>
        </div>

        <!-------------------- 2016 Content -------------------->
        <div class="mobile-swipe-cover mobile-milestones-bukit-bantayan">
            <div class="left-top-content width80">
                <h2 class="md-font">THE BUKIT BANTAYAN RESIDENCES DEVELOPMENT MARKS OUR MAIDEN ENTRY INTO EAST MALAYSIA</h2>
            </div>
        </div>

        <!-------------------- 2015 Content -------------------->
        <div class="mobile-swipe-cover mobile-milestones-tender-land-singapore" id="mobile-milestones-2015">
            <div class="left-bottom-content width60">
                <h2 class="md-font">WE WIN THE TENDER FOR A 3-ACRE PLOT OF LAND IN SINGAPORE</h2>
            </div>
        </div>

        <!-------------------- 2015 Content -------------------->
        <div class="mobile-swipe-cover mobile-milestones-australian-market">
            <div class="left-top-content width60">
                <h2 class="md-font">GAMUDA LAND ENTERS THE AUSTRALIAN MARKET WITH 661 CHAPEL STREET</h2>
            </div>
        </div>

        <!-------------------- 2015 Content -------------------->
        <div class="mobile-swipe-cover mobile-milestones-highpark">
            <div class="left-bottom-content width80">
                <h2 class="md-font">OUR FIRST HIGH-RISE WELLNESS DEVELOPMENT, HIGHPARK SUITES IN KELANA JAYA, IS LAUNCHED</h2>
            </div>
        </div>

        <!-------------------- 2013 Content -------------------->
        <div class="mobile-swipe-cover mobile-milestones-the-robertson" id="mobile-milestones-2013">
            <div class="left-top-content width80">
                <h2 class="md-font">THE ROBERTSON BECOMES OUR PIONEEER HIGH RISE INTEGRATED DEVELOPMENT</h2>
            </div>
        </div>

        <!-------------------- 2013 Content -------------------->
        <div class="mobile-swipe-cover mobile-milestones-5000-mark">
            <div class="center-top-content">
                <h2 class="xlg-font">OUR LAND BANK<br>REACHES THE 5,000-ACRE MARK</h2>
            </div>
        </div>

        <!-------------------- 2012 Content -------------------->
        <div class="mobile-swipe-cover mobile-milestones-madge-mansions" id="mobile-milestones-2012">
            <div class="left-top-content width80">
                <h2 class="md-font">MADGE MANSIONS HAS THE DISTINCTION OF BEING OUR MAIDEN LUXURY CONDOMINIUM</h2>
            </div>
        </div>

        <!-------------------- 2011 Content -------------------->
        <div class="mobile-swipe-cover mobile-milestones-celadon-city" id="mobile-milestones-2011">
            <div class="left-top-content width70 top20">
                <h2 class="md-font">GAMUDA LAND STRENGTHENS FOOTPRINT IN VIETNAM WITH CELADON CITY DEVELOPMENT</h2>
            </div>
        </div>

        <!-------------------- 2008 Content -------------------->
        <div class="mobile-swipe-cover mobile-milestones-jadehills" id="mobile-milestones-2008">
            <div class="left-top-content width80">
                <h2 class="md-font">JADE HILLS IN KAJANG, GAMUDA LAND'S FIRST THEMATIC DEVELOPMENT</h2>
            </div>
        </div>

        <!-------------------- 2007 Content -------------------->
        <div class="mobile-swipe-cover mobile-milestones-horizon-hills" id="mobile-milestones-2007">
            <div class="left-top-content top10">
                <h2 class="md-font">GAMUDA LAND ENTERS ISKANDAR MALAYSIA WITH ITS FIRST PROPERTY DEVELOPMENT - HORIZON HILLS</h2>
            </div>
        </div>

        <!-------------------- 2007 Content -------------------->
        <div class="mobile-swipe-cover mobile-milestones-vietnam-international">
            <div class="left-bottom-content">
                <h2 class="md-font">GAMUDA CITY IN VIETNAM BECOMES GAMUDA LAND'S FIRST INTERNATIONAL DEVELOPMENT</h2>
            </div>
        </div>

        <!-------------------- 2005 Content -------------------->
        <div class="mobile-swipe-cover mobile-milestones-10000-houses" id="mobile-milestones-2005">
            <div class="center-top-content">
                <h2 class="xlg-font">GAMUDA LAND HANDS OVER<br>10,000 HOUSES</h2>
            </div>
        </div>

        <!-------------------- 2001 Content -------------------->
        <div class="mobile-swipe-cover mobile-milestones-bandar-botanic" id="mobile-milestones-1999">
            <div class="left-top-content top10">
                <h2 class="md-font">BANDAR BOTANIC BECOMES THE FIRST FIABCI AWARD-WINNING TOWNSHIP IN KLANG</h2>
            </div>
        </div>

        <!-------------------- 2000 Content -------------------->
        <div class="mobile-swipe-cover mobile-milestones-private-golf" id="mobile-milestones-1998">
            <div class="right-top-content text-right width60 top15">
                <h2 class="md-font">WE INTRODUCE THE FIRST RESORT-INSPIRED DEVELOPMENT WITH A PRIVATE GOLF COURSE</h2>
            </div>
        </div>

        <!-------------------- 1995 Content -------------------->
        <div class="mobile-swipe-cover mobile-milestones-kota-kemuning" id="mobile-milestones-1995">
            <div class="left-top-content width70 top25">
                <h2 class="md-font">KOTA KEMUNING, THE FIRST INTEGRATED TOWNSHIP DEVELOPMENT IS LAUNCHED</h2>
            </div>
        </div>

        <!-------------------- 1995 Content -------------------->
        <div class="mobile-swipe-cover mobile-milestones-founded">
            <div class="left-middle-content width80">
                <h2 class="md-font">GAMUDA LAND SDN BHD IS ESTABLISHED</h2>
            </div>
        </div>
		<div class="mobile-other-projects" id="mobile-milestones-learn-more">
			<h2 class="md-font" style="color:#333;">OTHER ABOUT US</h2>
			<div class="col-xs-6 mobile-prev-project">
				<a href="http://128.199.89.40/about-us/awards/"><img src="/wp-content/uploads/2016/09/m-about-us-banner-awards.jpg" alt="Awards"></a>
			</div>
			<div class="col-xs-6 mobile-next-project">
				<a href="http://128.199.89.40/about-us/philosophy/"><img src="/wp-content/uploads/2016/09/m-about-us-banner-philosophy.jpg" alt="Philosophy"></a>
			</div>
		</div

    </div>
</div>


</div>


<?php get_footer();