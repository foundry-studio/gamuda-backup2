jQuery(document).ready(function(){	
   var tabList = jQuery('.desktop-tab-group ul li');
   jQuery(tabList).click(function() {
	  for(var i = 1; i <= 3; i++) {
		  if(jQuery(this).data("num") == i) {
			  tabList.removeClass('active');
			  jQuery(this).addClass('active');
			  jQuery('.desktop-tab').velocity({opacity: 0}, {display: 'none', delay: 0, duration: 0});
			jQuery('.desktop-tab' + i).velocity({opacity: 1}, {display: 'block', delay: 0, duration: 0});
		  }
		  if(jQuery(this).data("num") == 1) {
			  jQuery('.desktop-tab-group').css('backgroundImage', 'url(/wp-content/uploads/2016/09/bb-features-text-bg.png)');
			  tabList.css('color', 'white');
			  jQuery(this).css('color', '#158179');
			  jQuery('.features1.bg-halfwrap-left').css('backgroundImage', 'url(/wp-content/uploads/2016/09/botanic-slider-features-1-left.jpg)');
			  jQuery('.features1.bg-halfwrap-right').css('backgroundImage', 'url(/wp-content/uploads/2016/09/botanic-slider-features-1-right.jpg)');
		  }
		  if(jQuery(this).data("num") == 2) {
			  jQuery('.desktop-tab-group').css('backgroundImage', 'url(/wp-content/uploads/2016/09/bb-west-text-bg.png)');
			  tabList.css('color', 'white');
			  jQuery(this).css('color', '#29bbc4');
			  jQuery('.features1.bg-halfwrap-left').css('backgroundImage', 'url(/wp-content/uploads/2016/08/botanic-slider-features-2-left.jpg)');
			  jQuery('.features1.bg-halfwrap-right').css('backgroundImage', 'url(/wp-content/uploads/2016/08/botanic-slider-features-2-right.jpg)');
		  }
		  if(jQuery(this).data("num") == 3) {
			  jQuery('.desktop-tab-group').css('backgroundImage', 'url(/wp-content/uploads/2016/09/bb-east-text-bg.png)');
			  tabList.css('color', 'white');
			  jQuery(this).css('color', '#12ad95');
			  jQuery('.features1.bg-halfwrap-left').css('backgroundImage', 'url(/wp-content/uploads/2016/08/botanic-slider-features-3-left.jpg)');
			  jQuery('.features1.bg-halfwrap-right').css('backgroundImage', 'url(/wp-content/uploads/2016/08/botanic-slider-features-3-right.jpg)');
		  }
	  }
   })
})

jQuery(document).ready(function() {	
    jQuery('#botanic-box-main .main-text-box1').velocity({opacity:1});
    jQuery('#botanic-box-main .main-text-box1 .main-text-box-left img').velocity({marginRight: "0", opacity:1});
    jQuery('#botanic-box-main .main-text-box1 .main-text-box-right h5').velocity({marginLeft: "0", opacity:1});
	
var scrolling = false;
var oldTime = 0;
var newTime = 0;
var isTouchPad;
var eventCount = 0;
var eventCountStart;
var macScrollCount = 0;
var scrollCount = 0;


var mouseHandle = function (evt) {
    var isTouchPadDefined = isTouchPad || typeof isTouchPad !== "undefined";
    if (!isTouchPadDefined) {
        if (eventCount === 0) {
            eventCountStart = new Date().getTime();
        }
        eventCount++;
        if (new Date().getTime() - eventCountStart > 50) {
                if (eventCount > 5) {
                    isTouchPad = true;
                } else {
                    isTouchPad = false;
                }
            isTouchPadDefined = true;
        }
    }

    if (isTouchPadDefined) { 
        if (!evt) evt = event;
        var direction = (evt.detail<0 || evt.wheelDelta>0) ? 1 : -1; 

        if (isTouchPad) {
            newTime = new Date().getTime();

            if (!scrolling && newTime-oldTime > 1000 ) {
                scrolling = true;
                if (direction < 0) {
                    macScrollCount ++;
				
					var vhToPx = jQuery(window).height() * 5.6;
					var lastScroll = -(vhToPx + jQuery('#botanic-box-footer footer').height());
					var moveUplastScroll = -(vhToPx);
					
					if(macScrollCount >= 1){
						jQuery('.page-id-6933 header').addClass('sticky');
					}
					
					if (macScrollCount == 1) {
						/* Main 1 Out - Main 2 In*/			
						aniOnePlus();		
					} else if (macScrollCount == 2) {
						/* Main 2 out - Overview in */
						aniTwoPlus();
					} else if (macScrollCount == 3) {
						/* Overview out - Features in */
						aniThreePlus();
					} else if (macScrollCount == 4) {
						/* Features out - Facilities in */
						aniFourPlus();
					} else if (macScrollCount == 5) {
						/* Facilities out - Master 1 in */
						aniFivePlus();
					} else if (macScrollCount == 6) {
						/* Master 1 out - Master 2 in */
						aniSixPlus();
					} else if (macScrollCount == 7) {
						/* Master 2 out - Master 3 in */
						aniSevenPlus();
					} else if (macScrollCount == 8) {
						/* Master 3 out - Master 4 in */
						aniEightPlus();
					} else if (macScrollCount == 9) {
						aniNinePlus();
					} else if (macScrollCount == 10) {					
						aniTenPlus();
					} else if (macScrollCount == 11) {
						jQuery('.page-id-6933 #botanic-indicator').velocity({right: '-175px'});
						jQuery('#botanic-box-wrap').velocity({top:lastScroll});
					} else {
						macScrollCount = 11; 
					}
                    console.log(macScrollCount);
                    console.log("macdown");
                } else {
                    macScrollCount --;

					if (macScrollCount == 0) {
						/* Main 2 Out - Main 1 In*/
						aniZeroMinus();
					} else if (macScrollCount == 1) {
						/* Overview out - Main 2 in */
						aniOneMinus();
					} else if (macScrollCount == 2) {
						/* Features out - Overview in */
						aniTwoMinus();			
					} else if (macScrollCount == 3) {
						/* Facilities out - Features in */
						aniThreeMinus();
					} else if (macScrollCount == 4) {	
						/* Master 1 out - Facilities in */		
						aniFourMinus();
					} else if (macScrollCount == 5) {	
						/* Master 2 out - Master 1 in */	
						aniFiveMinus();
					} else if (macScrollCount == 6) {	
						/* Master 3 out - Master 2 in */
						aniSixMinus();
					} else if (macScrollCount == 7) {
						/* Master 4 out - Master 3 in */
						aniSevenMinus();
					} else if (macScrollCount == 8) {
						aniEightMinus();
					} else if (macScrollCount == 9) {
						aniNineMinus();
					} else if (macScrollCount == 10) {
						jQuery('.page-id-6933 #botanic-indicator').velocity({right: 0});
						jQuery('#botanic-box-wrap').velocity({top:'-560vh'});
					}else{
						macScrollCount = 0;
					}
                    console.log(macScrollCount);
                    console.log("macup");
                }
                setTimeout(function() {oldTime = new Date().getTime();scrolling = false}, 500);
            }
        } else {
            if (direction < 0 && scrollCount < 12) {
				scrollCount ++;
				
				var vhToPx = jQuery(window).height() * 5.6;
				var lastScroll = -(vhToPx + jQuery('#botanic-box-footer footer').height());
				var moveUplastScroll = -(vhToPx);
				
				if(scrollCount >= 1){
					jQuery('.page-id-6933 header').addClass('sticky');
				}
				
				if (scrollCount == 1) {
					/* Main 1 Out - Main 2 In*/			
					aniOnePlus();		
                } else if (scrollCount == 2) {
					/* Main 2 out - Overview in */
					aniTwoPlus();
                } else if (scrollCount == 3) {
					/* Overview out - Features in */
                    aniThreePlus();
                } else if (scrollCount == 4) {
					/* Features out - Facilities in */
                    aniFourPlus();
                } else if (scrollCount == 5) {
					/* Facilities out - Master 1 in */
					aniFivePlus();
                } else if (scrollCount == 6) {
					/* Master 1 out - Master 2 in */
					aniSixPlus();
                } else if (scrollCount == 7) {
					/* Master 2 out - Master 3 in */
                    aniSevenPlus();
                } else if (scrollCount == 8) {
					/* Master 3 out - Master 4 in */
					aniEightPlus();
                } else if (scrollCount == 9) {
                    aniNinePlus();
                } else if (scrollCount == 10) {					
                    aniTenPlus();
                } else if (scrollCount == 11) {
                    jQuery('.page-id-6933 #botanic-indicator').velocity({right: '-175px'});
					jQuery('#botanic-box-wrap').velocity({top:lastScroll});
				} else {
					scrollCount = 11; 
				}
                console.log(scrollCount);
                console.log("down");
            } else {
                scrollCount --;

                if (scrollCount == 0) {
					/* Main 2 Out - Main 1 In*/
					aniZeroMinus();
                } else if (scrollCount == 1) {
					/* Overview out - Main 2 in */
					aniOneMinus();
				} else if (scrollCount == 2) {
					/* Features out - Overview in */
					aniTwoMinus();			
                } else if (scrollCount == 3) {
					/* Facilities out - Features in */
                    aniThreeMinus();
                } else if (scrollCount == 4) {	
					/* Master 1 out - Facilities in */		
                    aniFourMinus();
                } else if (scrollCount == 5) {	
					/* Master 2 out - Master 1 in */	
					aniFiveMinus();
                } else if (scrollCount == 6) {	
					/* Master 3 out - Master 2 in */
					aniSixMinus();
				} else if (scrollCount == 7) {
					/* Master 4 out - Master 3 in */
					aniSevenMinus();
				} else if (scrollCount == 8) {
                    aniEightMinus();
				} else if (scrollCount == 9) {
                    aniNineMinus();
				} else if (scrollCount == 10) {
                    jQuery('.page-id-6933 #botanic-indicator').velocity({right: 0});
					jQuery('#botanic-box-wrap').velocity({top:'-560vh'});
				}else{
					scrollCount = 0;
				}
                console.log(scrollCount);
                console.log("up");
            }
        }
    }
}

window.addEventListener("mousewheel", mouseHandle, false);
window.addEventListener("DOMMouseScroll", mouseHandle, false);

	//Side Nav
	function aniOnePlus() {
		jQuery('#botanic-box-main .main-text-box1').velocity({opacity:0});
		jQuery('#botanic-box-main .main-text-box2').velocity({top:'50%', opacity:1});
        jQuery('#botanic-box-main .bg-halfwrap-left').velocity({opacity: "1"});
        jQuery('#botanic-box-main .bg-halfwrap-right').velocity({opacity: "1"});
	}
	function aniTwoPlus() {
		jQuery('#botanic-box-main .main-text-box2').velocity({top:'45%', opacity:0});
        jQuery('#botanic-box-main .bg-halfwrap-right').velocity({bottom: "-200%"});
		jQuery('#botanic-box-wrap').velocity({top: "-100vh"});
        jQuery('#botanic-box-overview .bg-halfwrap-right').velocity({bottom: "0"});
        jQuery('#botanic-box-overview .bg-halfwrap-right').css('display', 'block');
        jQuery('#botanic-box-overview .botanic-text-box').velocity({opacity: 1, left:0}, {delay:200});
		jQuery('#bb-indicator-home').velocity({color:'#d7d7d7'});
		jQuery('#bb-indicator-home .indicator-circle').velocity({backgroundColor:'#d7d7d7'});
		jQuery('#bb-indicator-overview').velocity({color:'#ee2e24'});
		jQuery('#bb-indicator-overview .indicator-circle').velocity({backgroundColor:'#ee2e24'});
	}
	function aniThreePlus() {
		jQuery('#botanic-box-overview .bg-halfwrap-right').velocity({bottom: "-200%"});
        jQuery('#botanic-box-overview .botanic-text-box').velocity({opacity: 0, left:'-30px'}, {delay:200});	
		jQuery('#botanic-box-features .features1.bg-halfwrap-right').css('display', 'block');
		jQuery('#botanic-box-wrap').velocity({top: "-200vh"});
        jQuery('#botanic-box-features .features1.bg-halfwrap-left').velocity({top: "0"});
        jQuery('#botanic-box-features .features1.bg-halfwrap-right').velocity({bottom: "0"});
		jQuery('#botanic-box-features .desktop-tab-group').velocity({opacity:1, top:'47%'});
		jQuery('#bb-indicator-overview').velocity({color:'#d7d7d7'});
		jQuery('#bb-indicator-overview .indicator-circle').velocity({backgroundColor:'#d7d7d7'});
		jQuery('#bb-indicator-features').velocity({color:'#ee2e24'});
		jQuery('#bb-indicator-features .indicator-circle').velocity({backgroundColor:'#ee2e24'});
	}
	function aniFourPlus() {
		jQuery('#botanic-box-wrap').velocity({top: "-300vh"});
        jQuery('#botanic-box-features .botanic-text-box').velocity({opacity: "0"});	
        jQuery('#botanic-box-facilities .botanic-text-box').velocity({opacity: "1"}, {delay:200});
		jQuery('#bb-indicator-features').velocity({color:'#d7d7d7'});
		jQuery('#bb-indicator-features .indicator-circle').velocity({backgroundColor:'#d7d7d7'});
		jQuery('#bb-indicator-facilities').velocity({color:'#ee2e24'});
		jQuery('#bb-indicator-facilities .indicator-circle').velocity({backgroundColor:'#ee2e24'});
	}
	function aniFivePlus() {
		jQuery('#botanic-box-master .grey-box-bg').css('display', 'block');
        jQuery('#botanic-box-master .grey-box-bg').velocity({opacity:1}, {duration:200});
        jQuery('#botanic-box-wrap').velocity({top: "-400vh"}, {delay:50});
		jQuery('#botanic-box-master .master-title-box1').css('display', 'block');
		jQuery('#botanic-box-master .master-map-box1').css('display', 'block');
		jQuery('#botanic-box-master .master-text-box1').css('display', 'block');
        jQuery('#botanic-box-master .master1').velocity({opacity:1});
        jQuery('#botanic-box-master .master-title-box1').velocity({opacity:1}, {delay:200});
        jQuery('#botanic-box-master .master-map-box1').velocity({opacity:1}, {delay:200});
        jQuery('#botanic-box-master .master-map-box1 img').velocity({width:'50%', marginLeft:'25%'}, {delay:200});
        jQuery('#botanic-box-master .master-text-box1').velocity({marginLeft:0, opacity:1}, {delay:250});
        jQuery('#botanic-box-master .text-box-border').velocity({opacity:1}, {delay:500});	
		jQuery('#bb-indicator-facilities').velocity({color:'#d7d7d7'});
		jQuery('#bb-indicator-facilities .indicator-circle').velocity({backgroundColor:'#d7d7d7'});
		jQuery('#bb-indicator-master').velocity({color:'#ee2e24'});
		jQuery('#bb-indicator-master .indicator-circle').velocity({backgroundColor:'#ee2e24'});
	}
	function aniSixPlus() {
		jQuery('#botanic-box-master .grey-box-bg').css('display', 'none');
		jQuery('#botanic-box-master .master1').addClass('moveBg');
        jQuery('#botanic-box-master .master-map-box1').velocity({marginTop:'20%',opacity:0});
        jQuery('#botanic-box-master .master-text-box1').velocity({opacity:0});					
		setTimeout(function() {
			jQuery('#botanic-box-master .master-map-box1').css('display', 'none');
			jQuery('#botanic-box-master .master-text-box1').css('display', 'none');
		}, 400);
        jQuery('#botanic-box-master .master-text-box2-left').css('display', 'block');
        jQuery('#botanic-box-master .master-text-box2-right').css('display', 'block');
		jQuery('#botanic-box-master .master2').velocity({height:'100vh'},{delay:400});
		jQuery('#botanic-box-master .master-text-box2-left').velocity({left:0, opacity:1},{delay:500});
		jQuery('#botanic-box-master .master-text-box2-right').velocity({right:0, opacity:1},{delay:550});
	}
	function aniSevenPlus() {
		jQuery('#botanic-box-master .master-title-box1').velocity({opacity:0});				
		setTimeout(function() {
			jQuery('#botanic-box-master .master-title-box1').css('display', 'none');
			jQuery('#botanic-box-master .master-text-box2-left').css('display', 'none');
			jQuery('#botanic-box-master .master-text-box2-right').css('display', 'none');
		}, 400);
		jQuery('#botanic-box-master .master-text-box2-left').velocity({opacity:0});
		jQuery('#botanic-box-master .master-text-box2-right').velocity({opacity:0});
        jQuery('#botanic-box-master .master3.bg-halfwrap-left').css('display', 'block');
        jQuery('#botanic-box-master .master3.bg-halfwrap-right').css('display', 'block');
		jQuery('#botanic-box-master .master3.bg-halfwrap-left').velocity({opacity:1});
		jQuery('#botanic-box-master .master3.bg-halfwrap-right').velocity({opacity:1});	
        jQuery('#botanic-box-master .master-text-box3').css('display', 'block');
		jQuery('#botanic-box-master .master-text-box3').velocity({top:'30px', opacity:1}, {delay:200});
		jQuery('#botanic-box-master .master-text-box3-bottom').velocity({opacity:1}, {delay:200});
	}
	function aniEightPlus() {
		jQuery('#botanic-box-master .master3.bg-halfwrap-left').velocity({top:'-100%'});
		jQuery('#botanic-box-master .master3.bg-halfwrap-right').velocity({bottom:'-100%'});	
		jQuery('#botanic-box-master .master-text-box3').velocity({top:0, opacity:0});		
		setTimeout(function() {
			jQuery('#botanic-box-master .master-text-box3').css('display', 'none');
			jQuery('#botanic-box-master .master3.bg-halfwrap-left').css('display', 'none');
			jQuery('#botanic-box-master .master3.bg-halfwrap-right').css('display', 'none');
		}, 300);
        jQuery('#botanic-box-master .master4.bg-halfwrap-left').css('display', 'block');
        jQuery('#botanic-box-master .master4.bg-halfwrap-right').css('display', 'block');
        jQuery('#botanic-box-master .master-text-box4').css('display', 'block');
		jQuery('#botanic-box-master .master4.bg-halfwrap-left').velocity({top:0});
		jQuery('#botanic-box-master .master4.bg-halfwrap-right').velocity({bottom:0});
		jQuery('#botanic-box-master .master-text-box4').velocity({top:'50px', opacity:1});
	}
	function aniNinePlus() {
		jQuery('#botanic-box-wrap').velocity({top: "-500vh"});					
		setTimeout(function() {
			jQuery('#botanic-box-master .master-text-box4').css('display', 'none');
		}, 300);
		jQuery('#bb-indicator-master').velocity({color:'#d7d7d7'});
		jQuery('#bb-indicator-master .indicator-circle').velocity({backgroundColor:'#d7d7d7'});
		jQuery('#bb-indicator-happenings').velocity({color:'#ee2e24'});
		jQuery('#bb-indicator-happenings .indicator-circle').velocity({backgroundColor:'#ee2e24'});
	}
	function aniTenPlus() {
		jQuery('.page-id-6933 .page-breadcrumbs').velocity({opacity: 0});
        jQuery('#botanic-box-wrap').velocity({top: '-560vh'});
		jQuery('#botanic-box-other-dev').velocity({bottom:0});
		jQuery('#bb-indicator-happenings').velocity({color:'#d7d7d7'});
		jQuery('#bb-indicator-happenings .indicator-circle').velocity({backgroundColor:'#d7d7d7'});
		jQuery('#bb-indicator-more').velocity({color:'#ee2e24'});
		jQuery('#bb-indicator-more .indicator-circle').velocity({backgroundColor:'#ee2e24'});
	}
	function aniZeroMinus() {
		jQuery('.page-id-6933 header').removeClass('sticky');		
		jQuery('#botanic-box-main .main-text-box1').velocity({opacity:1});
		jQuery('#botanic-box-main .main-text-box2').velocity({top:'45%', opacity:0});
        jQuery('#botanic-box-main .bg-halfwrap-left').velocity({opacity: "0"});
        jQuery('#botanic-box-main .bg-halfwrap-right').velocity({opacity: "0"});
	}
	function aniOneMinus() {
		jQuery('#botanic-box-wrap').velocity({top: "0"});
		jQuery('#botanic-box-main .main-text-box2').velocity({top:'50%', opacity:1});
        jQuery('#botanic-box-main .bg-halfwrap-left').velocity({top: "0"});
        jQuery('#botanic-box-main .bg-halfwrap-right').velocity({bottom: "0"});
        jQuery('#botanic-box-overview .bg-halfwrap-right').velocity({bottom: "200%"});
        jQuery('#botanic-box-overview .botanic-text-box').velocity({opacity: 0, left:'-30px'}, {delay:200});	
		setTimeout(function() {
			jQuery('#botanic-box-overview .bg-halfwrap-right').css('display', 'none');
		}, 400);
		jQuery('#bb-indicator-home').velocity({color:'#ee2e24'});
		jQuery('#bb-indicator-home .indicator-circle').velocity({backgroundColor:'#ee2e24'});
		jQuery('#bb-indicator-overview').velocity({color:'#d7d7d7'});
		jQuery('#bb-indicator-overview .indicator-circle').velocity({backgroundColor:'#d7d7d7'});
	}
	function aniTwoMinus() {
		jQuery('#botanic-box-wrap').velocity({top: "-100vh"});
		jQuery('#botanic-box-overview .bg-halfwrap-right').css('display', 'block');
        jQuery('#botanic-box-overview').velocity({top: "0"});
        jQuery('#botanic-box-overview .bg-halfwrap-right').velocity({bottom: "0"});
        jQuery('#botanic-box-overview .botanic-text-box').velocity({opacity: 1, left:0}, {delay:200});
        jQuery('#botanic-box-features .features1.bg-halfwrap-right').velocity({bottom: "200%"});
		jQuery('#botanic-box-features .desktop-tab-group').velocity({opacity:0, top:"70%"},{delay:100});		
		setTimeout(function() {
			jQuery('#botanic-box-features .bg-halfwrap-right').css('display', 'none');
		}, 400);
		jQuery('#bb-indicator-overview').velocity({color:'#ee2e24'});
		jQuery('#bb-indicator-overview .indicator-circle').velocity({backgroundColor:'#ee2e24'});
		jQuery('#bb-indicator-features').velocity({color:'#d7d7d7'});
		jQuery('#bb-indicator-features .indicator-circle').velocity({backgroundColor:'#d7d7d7'});	
	}
	function aniThreeMinus() {
		jQuery('#botanic-box-features').velocity({top: "0"});
        jQuery('#botanic-box-features .botanic-text-box').velocity({opacity: "1"}, {delay:200});
        jQuery('#botanic-box-wrap').velocity({top: "-200vh"});
        jQuery('#botanic-box-facilities .botanic-text-box').velocity({opacity: "0"});	
		jQuery('#bb-indicator-features').velocity({color:'#ee2e24'});
		jQuery('#bb-indicator-features .indicator-circle').velocity({backgroundColor:'#ee2e24'});
		jQuery('#bb-indicator-facilities').velocity({color:'#d7d7d7'});
		jQuery('#bb-indicator-facilities .indicator-circle').velocity({backgroundColor:'#d7d7d7'});
        jQuery('#botanic-box-facilities .botanic-text-box').velocity({opacity: "0"});
	}
	function aniFourMinus() {
		jQuery('#botanic-box-master .grey-box-bg').velocity({opacity:0});
        jQuery('#botanic-box-wrap').velocity({top: "-300vh"});
		jQuery('#botanic-box-facilities').css('display', 'block');
		setTimeout(function() {
			jQuery('#botanic-box-master .grey-box-bg').css('display', 'none');
			jQuery('#botanic-box-master').css('marginTop', '0');
			jQuery('#botanic-box-master .master-title-box1').css('display', 'none');
			jQuery('#botanic-box-master .master-map-box1').css('display', 'none');
			jQuery('#botanic-box-master .master-text-box1').css('display', 'none');
		}, 400);
        jQuery('#botanic-box-master .master1').velocity({opacity:0});
        jQuery('#botanic-box-master .master-title-box1').velocity({opacity:0});
        jQuery('#botanic-box-master .master-map-box1').velocity({opacity:0});
        jQuery('#botanic-box-master .master-map-box1 img').velocity({width:'58%', marginLeft:'21%'});
        jQuery('#botanic-box-master .master-text-box1').velocity({marginLeft:'-100%', opacity:0});
        jQuery('#botanic-box-master .text-box-border').velocity({opacity:0});
		jQuery('#bb-indicator-facilities').velocity({color:'#ee2e24'});
		jQuery('#bb-indicator-facilities .indicator-circle').velocity({backgroundColor:'#ee2e24'});
		jQuery('#bb-indicator-master').velocity({color:'#d7d7d7'});
		jQuery('#bb-indicator-master .indicator-circle').velocity({backgroundColor:'#d7d7d7'});
	}
	function aniFiveMinus() {
		jQuery('#botanic-box-master .grey-box-bg').css('display', 'block');					
		setTimeout(function() {
			jQuery('#botanic-box-master .master1').removeClass('moveBg');	
		}, 400);
		jQuery('#botanic-box-master .master-map-box1').css('display', 'block');
		jQuery('#botanic-box-master .master-text-box1').css('display', 'block');
        jQuery('#botanic-box-master .master-map-box1').velocity({marginTop:'-40px', opacity:1}, {delay:300});
        jQuery('#botanic-box-master .master-text-box1').velocity({opacity:1}, {delay:300});
		jQuery('#botanic-box-master .master2').velocity({height:'0'},{duration:400});
		jQuery('#botanic-box-master .master-text-box2-left').velocity({left:'-100%', opacity:0});
		jQuery('#botanic-box-master .master-text-box2-right').velocity({right:'-100%', opacity:0});		
		setTimeout(function() {
			jQuery('#botanic-box-master .master-text-box2-left').css('display', 'none');
			jQuery('#botanic-box-master .master-text-box2-right').css('display', 'none');
		}, 400);
	}
	function aniSixMinus() {
		jQuery('#botanic-box-master .master-title-box1').css('display', 'block');
		jQuery('#botanic-box-master .master-text-box2-left').css('display', 'block');
		jQuery('#botanic-box-master .master-text-box2-right').css('display', 'block');
        jQuery('#botanic-box-master .master-title-box1').velocity({opacity:1});
		jQuery('#botanic-box-master .master-text-box2-left').velocity({opacity:1}, {delay:200});
		jQuery('#botanic-box-master .master-text-box2-right').velocity({opacity:1}, {delay:200});
		jQuery('#botanic-box-master .master3.bg-halfwrap-left').velocity({opacity:0});
		jQuery('#botanic-box-master .master3.bg-halfwrap-right').velocity({opacity:0});	
		setTimeout(function() {
			jQuery('#botanic-box-master .master-text-box3').css('display', 'none');
        jQuery('#botanic-box-master .master3.bg-halfwrap-left').css('display', 'none');
        jQuery('#botanic-box-master .master3.bg-halfwrap-right').css('display', 'none');
		}, 300);
		jQuery('#botanic-box-master .master-text-box3').velocity({top:'0', opacity:0});
		jQuery('#botanic-box-master .master-text-box3-bottom').velocity({opacity:0});
	}
	function aniSevenMinus() {
		jQuery('#botanic-box-master .master-text-box3').css('display', 'block');
		jQuery('#botanic-box-master .master3.bg-halfwrap-left').css('display', 'block');
		jQuery('#botanic-box-master .master3.bg-halfwrap-right').css('display', 'block');
		jQuery('#botanic-box-master .master3.bg-halfwrap-left').velocity({top:0});
		jQuery('#botanic-box-master .master3.bg-halfwrap-right').velocity({bottom:0});
		jQuery('#botanic-box-master .master-text-box3').velocity({top:'30px', opacity:1}, {delay:200});
		jQuery('#botanic-box-master .master4.bg-halfwrap-left').velocity({top:'100%'});
		jQuery('#botanic-box-master .master4.bg-halfwrap-right').velocity({bottom:'100%'});
		jQuery('#botanic-box-master .master-text-box4').velocity({top:0, opacity:0});
		setTimeout(function() {
			jQuery('#botanic-box-master .master-text-box4').css('display', 'none');
			jQuery('#botanic-box-master .master4.bg-halfwrap-left').css('display', 'none');
			jQuery('#botanic-box-master .master4.bg-halfwrap-right').css('display', 'none');
		}, 300);
	}
	function aniEightMinus() {
		jQuery('#botanic-box-wrap').velocity({top: "-400vh"});
		jQuery('#botanic-box-master .master-text-box4').css('display', 'block');
		jQuery('#bb-indicator-master').velocity({color:'#ee2e24'});
		jQuery('#bb-indicator-master .indicator-circle').velocity({backgroundColor:'#ee2e24'});
		jQuery('#bb-indicator-happenings').velocity({color:'#d7d7d7'});
		jQuery('#bb-indicator-happenings .indicator-circle').velocity({backgroundColor:'#d7d7d7'});
	}
	function aniNineMinus() {
		jQuery('.page-id-6933 .page-breadcrumbs').velocity({opacity: 1});
        jQuery('#botanic-box-wrap').velocity({top: '-500vh'});
		jQuery('#bb-indicator-happenings').velocity({color:'#ee2e24'});
		jQuery('#bb-indicator-happenings .indicator-circle').velocity({backgroundColor:'#ee2e24'});
		jQuery('#bb-indicator-more').velocity({color:'#d7d7d7'});
		jQuery('#bb-indicator-more .indicator-circle').velocity({backgroundColor:'#d7d7d7'});
	}
	var currentScreen = 1;
	
	jQuery('#bb-indicator-home').click(function() {
		if (currentScreen == 2) {
			aniOneMinus(); aniZeroMinus();
		} else if (currentScreen == 3) {
			aniTwoMinus();
			aniOneMinus();
			aniZeroMinus();
		} else if (currentScreen == 4) {
			aniThreeMinus();
			aniTwoMinus();
			aniOneMinus();
			aniZeroMinus();
		} else if (currentScreen == 5) {
			aniFourMinus();
			aniThreeMinus();
			aniTwoMinus();
			aniOneMinus();
			aniZeroMinus();
		} else if (currentScreen == 6) {
			aniEightMinus();
			aniSevenMinus();
			aniSixMinus();
			aniFiveMinus();
			aniFourMinus();
			aniThreeMinus();
			aniTwoMinus();
			aniOneMinus();
			aniZeroMinus();
		} else {
			aniNineMinus();
			aniEightMinus();
			aniSevenMinus();
			aniSixMinus();
			aniFiveMinus();
			aniFourMinus();
			aniThreeMinus();
			aniTwoMinus();
			aniOneMinus();
			aniZeroMinus();
		}
		currentScreen = 1;
		scrollCount = 0;
	});
	
	jQuery('#bb-indicator-overview').click(function() {
		if (currentScreen == 1) {
			aniOnePlus();
			aniTwoPlus();
		} else if (currentScreen == 3) {
			aniTwoMinus();
		} else if (currentScreen == 4) {
			aniThreeMinus();
			aniTwoMinus();
		} else if (currentScreen == 5) {
			aniFourMinus();
			aniThreeMinus();
			aniTwoMinus();
		} else if (currentScreen == 6) {
			aniEightMinus();
			aniSevenMinus();
			aniSixMinus();
			aniFiveMinus();
			aniFourMinus();
			aniThreeMinus();
			aniTwoMinus();
		} else {
			aniNineMinus();
			aniEightMinus();
			aniSevenMinus();
			aniSixMinus();
			aniFiveMinus();
			aniFourMinus();
			aniThreeMinus();
			aniTwoMinus();
		}
		currentScreen = 2;
		scrollCount = 2;
	});
	
	jQuery('#bb-indicator-features').click(function() {
		if (currentScreen == 1) {
			aniOnePlus();
			aniTwoPlus();
			aniThreePlus();
		} else if (currentScreen == 2) {
			aniThreePlus();	
		} else if (currentScreen == 4) {
			aniThreeMinus();
		} else if (currentScreen == 5) {
			aniFourMinus();
			aniThreeMinus();
		} else if (currentScreen == 6) {
			aniEightMinus();
			aniSevenMinus();
			aniSixMinus();
			aniFiveMinus();
			aniFourMinus();
			aniThreeMinus();
		} else {
			aniNineMinus();
			aniEightMinus();
			aniSevenMinus();
			aniSixMinus();
			aniFiveMinus();
			aniFourMinus();
			aniThreeMinus();
		}
		currentScreen = 3;
		scrollCount = 3;
	});
	
	jQuery('#bb-indicator-facilities').click(function() {
		if (currentScreen == 1) {
			aniOnePlus();
			aniTwoPlus();
			aniThreePlus();
			aniFourPlus();
		} else if (currentScreen == 2) {
			aniThreePlus();
			aniFourPlus();
		} else if (currentScreen == 3) {
			aniFourPlus();
		} else if (currentScreen == 5) {
			aniFourMinus();
		} else if (currentScreen == 6) {
			aniEightMinus();
			aniSevenMinus();
			aniSixMinus();
			aniFiveMinus();
			aniFourMinus();
		} else {
			aniNineMinus();
			aniEightMinus();
			aniSevenMinus();
			aniSixMinus();
			aniFiveMinus();
			aniFourMinus();
		}
		currentScreen = 4;
		scrollCount = 4;
	});
	
	jQuery('#bb-indicator-master').click(function() {
		if (currentScreen == 1) {
			aniOnePlus();
			aniTwoPlus();
			aniThreePlus();
			aniFourPlus();
			aniFivePlus();
		} else if (currentScreen == 2) {
			aniThreePlus();
			aniFourPlus();
			aniFivePlus();
		} else if (currentScreen == 3) {
			aniFourPlus();
			aniFivePlus();
		} else if (currentScreen == 4) {
			aniFivePlus();
		} else if (currentScreen == 6) {
			aniEightMinus();
			aniSevenMinus();
			aniSixMinus();
			aniFiveMinus();
		} else {
			aniNineMinus();
			aniEightMinus();
			aniSevenMinus();
			aniSixMinus();
			aniFiveMinus();
		}
		currentScreen = 5;
		scrollCount = 5;
	});
	
	jQuery('#bb-indicator-happenings').click(function() {
		if (currentScreen == 1) {
			aniOnePlus();
			aniTwoPlus();
			aniThreePlus();
			aniFourPlus();
			aniFivePlus();
			aniSixPlus();
			aniSevenPlus();
			aniEightPlus();
			aniNinePlus();
		} else if (currentScreen == 2) {
			aniThreePlus();
			aniFourPlus();
			aniFivePlus();
			aniSixPlus();
			aniSevenPlus();
			aniEightPlus();
			aniNinePlus();
		} else if (currentScreen == 3) {
			aniFourPlus();
			aniFivePlus();
			aniSixPlus();
			aniSevenPlus();
			aniEightPlus();
			aniNinePlus();
		} else if (currentScreen == 4) {
			aniFivePlus();
			aniSixPlus();
			aniSevenPlus();
			aniEightPlus();
			aniNinePlus();
		} else if (currentScreen == 5) {
			aniSixPlus();
			aniSevenPlus();
			aniEightPlus();
			aniNinePlus();
		} else {
			aniNineMinus();
		}
		currentScreen = 6;
		scrollCount = 9;
	});
	
	jQuery('#bb-indicator-more').click(function() {
		if (currentScreen == 1) {
			aniOnePlus();
			aniTwoPlus();
			aniThreePlus();
			aniFourPlus();
			aniFivePlus();
			aniSixPlus();
			aniSevenPlus();
			aniEightPlus();
			aniNinePlus();
			aniTenPlus();
		} else if (currentScreen == 2) {
			aniThreePlus();
			aniFourPlus();
			aniFivePlus();
			aniSixPlus();
			aniSevenPlus();
			aniEightPlus();
			aniNinePlus();
			aniTenPlus();
		} else if (currentScreen == 3) {
			aniFourPlus();
			aniFivePlus();
			aniSixPlus();
			aniSevenPlus();
			aniEightPlus();
			aniNinePlus();
			aniTenPlus();
		} else if (currentScreen == 4) {
			aniFivePlus();
			aniSixPlus();
			aniSevenPlus();
			aniEightPlus();
			aniNinePlus();
			aniTenPlus();
		} else if (currentScreen == 5) {
			aniSixPlus();
			aniSevenPlus();
			aniEightPlus();
			aniNinePlus();
			aniTenPlus();
		} else if (currentScreen == 6) {
			aniTenPlus();
		}
		currentScreen = 7;
		scrollCount = 10;
	});

});