<?php
// Template Name:  Valencia Mobile Template 
get_header(); ?>

<div class="mobile-main-nav mobile-main-nav-all">
    <div class="mobile-main-nav-header">
        <a class="mobile-main-nav-logo" href="http://128.199.89.40/"><img src="http://128.199.89.40/wp-content/uploads/2016/07/logo-1.png" alt="Gamuda Land Logo"></a>
		<div class="mobile-main-nav-close"><img src="/wp-content/uploads/2016/09/nav-close-btn.png"></div>
    </div>
    <ul class="list-unstyled">
        <li><a href="http://128.199.89.40/about-us/">ABOUT US</a></li>
        <li class="mobile-sub-nav-open">
            <a href="http://128.199.89.40/our-developments/">OUR DEVELOPMENTS</a>
            <i class="fa fa-angle-right" aria-hidden="true"></i>
        </li>
        <li><a href="http://128.199.89.40/happenings/">HAPPENINGS</a></li>
        <li><a href="http://128.199.89.40/contact-us/">CONTACT US</a></li>
    </ul>
    <a class="mobile-main-nav-register" href="https://webreg.gamudaland.com.my/contact/_publicwebregistration/" target="_blank">REGISTER</a>
    <div class="mobile-main-nav-sns">
        <a href="https://www.facebook.com/GamudaLand/?fref=ts&ref=br_tf"><i class="fa fa-facebook" aria-hidden="true"></i></a>
        <a href="https://www.instagram.com/gamudaland/"><i class="fa fa-instagram" aria-hidden="true"></i></a>
    </div>
</div>

<div class="mobile-main-nav mobile-main-nav-projects">

    <div class="mobile-main-nav-header">
        <a class="mobile-main-nav-logo" href="http://128.199.89.40/"><img src="http://128.199.89.40/wp-content/uploads/2016/07/logo-1.png" alt="Gamuda Land Logo"></a>
		<div class="mobile-main-nav-close"><img src="/wp-content/uploads/2016/09/nav-close-btn.png"></div>
    </div>
    <div class="mobile-main-nav-projects-list">
        <ul class="list-unstyled">
            <li class="mobile-sub-nav-close">
                <b>FEATURED</b> DEVELOPMENTS
                <i class="fa fa-angle-left" aria-hidden="true"></i>
            </li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/highpark-suites/">HIGHPARK SUITES<span>Kelana Jaya</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/the-robertson/">THE ROBERTSON<span>Kuala Lumpur</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/jade-hills/">JADE HILLS<span>Kajang</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/horizon-hills/">HORIZON HILLS<span>Iskandar Puteri</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/bukit-bantayan/">BUKIT BANTAYAN<span>Kota Kinabalu</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/gamuda-walk/">GAMUDA WALK<span>Shah Alam</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/gem-residences/">GEM RESIDENCES<span>Toa Payoh</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/661-chapel-st/">661 CHAPEL ST.<span>Melbourne</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/gamuda-city/">GAMUDA CITY<span>Hanoi</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/celadon-city/">CELADON CITY<span>Ho Chi Minh</span></a></li>
        </ul>
        <ul class="list-unstyled">
            <li><b>UPCOMING</b> DEVELOPMENTS</li>
            <li><a href="http://gamudaland.com.my/kundangestates/">KUNDANG ESTATES<span>Kuang</span></a></li>
            <li><a href="#" class="navi-disable">GAMUDA GARDENS<span>Kuang</span></a></li>
            <li><a href="http://gamudaland.com.my/twentyfive7/">TWENTYFIVE7<span>Kota Kemuning</span></a></li>
        </ul>
        <ul class="list-unstyled">
            <li><b>FULLY SOLD</b> DEVELOPMENTS</li>
            <li><a href="http://128.199.89.40/our-developments/fully-sold-developments/kota-kemuning/">KOTA KEMUNING<span>Shah Alam</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/fully-sold-developments/bandar-botanic/">BANDAR BOTANIC<span>Klang</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/fully-sold-developments/valencia/">VALENCIA<span>Sungai Buloh</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/fully-sold-developments/madge-mansions/">MADGE MANSIONS<span>Kuala Lumpur</span></a></li>
        </ul>
        <ul class="list-unstyled">
            <li><b>GOLF &amp; COUNTRY</b> CLUBS</li>
            <li><a href="http://www.kotapermai.com.my/" target="_blank">KOTA PERMAI<span>Golf &amp; Country Club</span></a></li>
            <li><a href="http://hhgcc.com.my/" target="_blank">HORIZON HILLS<span>Golf &amp; Country Club</span></a></li>
            <li><a href="http://www.botanicresortclub.com.my/" target="_blank">BOTANIC<span>Resort Club</span></a></li>
        </ul>
    </div>
</div>

<div id="mobile-pages" class="mobile-slide-page valencia mobile-page">
    <div class="container-fluid document">
        <div class="row mobile-cover mobile-valencia-main-cover">
            <div class="mobile-cover-content">
                <table>
                    <tr>
                        <td>
                            <img src="/wp-content/themes/Impreza-child/img/valencia-logo-white.png" alt="Valencia Logo">
                        </td>
                        <td>
                            <p class="lg-font" style="margin-bottom:10px;">in 360°</p>
                            <p class="sm-font">Explore the sounds, streets and soul of the Townsthip in 360° </p>
                        </td>
                    </tr>
                </table>
                <!--<a class="sm-font start-journey-btn" href="#">Start your Journey</a>-->
            </div>
			<div class="mouse">
				<div class="mouse-icon">
					<span class="mouse-wheel"></span>
				</div>
			</div>
            <div class="mobile-cover-bg">
                <!--<img src="/wp-content/themes/Impreza-child/img/valencia-main-cover.jpg" alt="Mobile Cover Image">-->
            </div>
        </div>

        <div class="row mobile-valencia-overview-content">
            <img src="/wp-content/themes/Impreza-child/img/valencia-logo-color.png" alt="Valencia Logo">
            <h2 class="md-font">OVERVIEW</h2>
            <p class="sm-font">Valencia is Gamuda Land’s flagship boutique development. Located at Sungai Buloh and neighbouring the Forest Reserve Institute of Malaysia, Valencia is Malaysia’s sole residential development that offers a private residents-only golf course and clubhouse in a luxurious resort-style setting.</p>
            <p class="sm-font">Built on a sprawling 280-acres of gentle rolling terrain, this gated lifestyle resort houses a variety of bungalows, semi-Ds, terraces, town villas and SOHO residences. Capitalising on the 9-hole golf course terrain, the homes at Valencia nestle amidst lush foliage, picturesque lakes and green fairways.</p>
            <p class="sm-font">Over the years, Valencia has become one of the Klang Valley’s most sought after properties, growing in value as an exclusive gated community with a high quality lifestyle.</p>
        </div>

        <div class="row mobile-valencia-concept">
            <h2 class="md-font">CONCEPT & DESIGN</h2>
            <p>Valencia was conceived as an exclusive and luxurious residential enclave that would be home to a small and close-knit community of residents offering them privacy, comfort and peace of mind. Valencia’s focus on “Lifestyle and Community” is exemplified by way of its offer of a bevy of premier services and facilities, as well as in the way the homes are creatively built to integrate with the green surroundings.</p>
            <p>One of the privileges of living in Valencia is the luxury of golfing in your very own private golf course. Overlooking the undulating golf terrain is the Golfers Terrace that provides an excellent avenue to unwind after a round of golf.</p>
            <p>Designed by internationally renowned golf course designer, Ross Watson, Valencia’s 9-hole course is gracefully intertwined between the homes and the undulating terrain while its lush landscaping blends in seamlessly with the rich tropical backdrop of the forest reserve. In creating the ideal resort ambience for residents, Valencia holds true to its “Lifestyle and Community” identity.</p>
        </div>

        <div class="row mobile-valencia-master-plan">
            <h2 class="md-font">MASTER PLAN</h2>
            <table>
                <tr style="background-color: #371618;">
                    <td><img src="/wp-content/themes/Impreza-child/img/valencia-master-plan-icon1.png" alt="Icon"></td>
                    <td>
                        <h3 class="md-font">SOUTH GOLF PRECINCT</h3>
                        <p class="sm-font">The South Golf Precinct, situated at the southern end of Valencia's 9-hole golf course, overlooks the fairways, picturesque lakes and lush landscaped gardens. Each neighbourhood in this precinct enjoys spectacular views of the golf course and its surrounding area. </p>
                        <p class="sm-font">Divided into six neighbourhoods, each neighbourhood is inspired by unique landscape features and the natural surroundings of the forest reserve.</p>
                    </td>
                </tr>
                <tr style="background-color: #661317;">
                    <td><img src="/wp-content/themes/Impreza-child/img/valencia-master-plan-icon2.png" alt="Icon"></td>
                    <td>
                        <h3 class="md-font">NORTH GOLF PRECINT</h3>
                        <p class="sm-font">The North Golf Precinct, just above the South Golf Precinct, enjoys scenic views of the golf course and the stream running along Fairway Number 1.</p>
                        <p class="sm-font">This precinct also enjoys the distant views of the Forest Reserve Institute of Malaysia and the abundance of landscaped parks and greens.</p>
                    </td>
                </tr>
                <tr style="background-color: #450f12;">
                    <td><img src="/wp-content/themes/Impreza-child/img/valencia-master-plan-icon3.png" alt="Icon"></td>
                    <td>
                        <h3 class="md-font">HILL COURT PRECINCT</h3>
                        <p class="sm-font">Strategically located on the highest point of Valencia, the Hill Court Precinct incorporates intimate cluster homes with well-planned neighbourhood parks and green lungs. It is also the precinct closest to the clubhouse providing its residents easy access at all times.</p>
                    </td>
                </tr>
                <tr style="background-color: #371618;">
                    <td><img src="/wp-content/themes/Impreza-child/img/valencia-master-plan-icon4.png" alt="Icon"></td>
                    <td>
                        <h3 class="md-font">GARDEN PRECINCT</h3>
                        <p class="sm-font">The Garden Precinct, located closest to the forest reserve at the top left of Valencia is an evergreen precinct as it is surrounded by beautiful palm trees, pine trees, pocket gardens and neighbourhood gardens.</p>
                    </td>
                </tr>
            </table>
        </div>

        <div class="row mobile-valencia-banner">
            <div class="mobile-valencia-banner-content">
                <h2 class="lg-font" style="color:#fff">KLANG VALLEYS'S FINEST<br>IN ESTEEMED LIVING</h2>
            </div>
            <div class="mobile-valencia-banner-bg">
                <img src="/wp-content/themes/Impreza-child/img/valencia-banner-bg.jpg" alt="Valencia Banner Background Image">
            </div>
        </div>
	<div class="row">
		<div class="mobile-other-projects">
			<h2 class="md-font">OTHER DEVELOPMENTS</h2>
			<div class="col-xs-6 mobile-prev-project">
				<a href="http://128.199.89.40/our-developments/fully-sold-developments/bandar-botanic/"><img src="/wp-content/uploads/2016/09/m-other-dev-banner-bandarbotanic.jpg" alt="Bandar Botanic"></a>
			</div>
			<div class="col-xs-6 mobile-next-project">
				<a href="http://128.199.89.40/our-developments/fully-sold-developments/madge-mansions/"><img src="/wp-content/uploads/2016/09/m-other-dev-banner-madgemansions.jpg" alt="Madge Mansions"></a>
			</div>
		</div>
	</div>
    </div>
</div>
<?php get_footer();