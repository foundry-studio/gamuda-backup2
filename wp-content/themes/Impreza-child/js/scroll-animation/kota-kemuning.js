/* jQuery(document).ready(function(){	
	jQuery('.video_thumbnail').click(function(){
		
        var url=jQuery(this).data('video');
		
        jQuery('.modal_video_element').attr('src',url);
	});

    jQuery(".close").click(function(){
        jQuery('.modal_video_element').attr('src','');
    });
    jQuery(".modal-backdrop").click(function(){
        jQuery('.modal_video_element').attr('src','');
    });
    jQuery("#myModal").on('hidden.bs.modal', function (e) { 
       jQuery('.modal_video_element').attr('src','');
	});
}); */
jQuery(document).ready(function(){
	
	jQuery('#kk-indicator ul li').click(function(){
		var kk_indicator_id = jQuery(this).data('target');
		jQuery('html, body').animate({ 
		 	//scrollTop: kk_indicator_id.offset().top
			scrollTop: jQuery(jQuery(this).data('target')).offset().top 
		}, 600);
        return false;
	})
})
jQuery(document).ready(function(){
	// Responsive Image Map
	jQuery('img[usemap]').rwdImageMaps();
	
    // init controller
	var controller = new ScrollMagic.Controller();

	/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*\
	|                             PAGE INDICATIONS                             |
	\*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
	var firstIndicator = new ScrollMagic.Scene({
		triggerElement: "#firstTrigger",
		triggerHook: 0,
		duration: "900%"
	})
	.setClassToggle("#iHome", "inactive-kk-home") // add class toggle
	//.addIndicators();

	var secondIndicator = new ScrollMagic.Scene({
		triggerElement: "#firstTrigger",
		triggerHook: 0,
		duration: "200%"
	})
	.setClassToggle("#iOverview", "active-kk-overview") // add class toggle
	//.addIndicators();

	var thirdIndicator = new ScrollMagic.Scene({
		triggerElement: "#thirdTrigger",
		triggerHook: 0,
		duration: "200%"
	})
	.setClassToggle("#iConcept", "active-kk-concept") // add class toggle
	//.addIndicators();

	var fourthIndicator = new ScrollMagic.Scene({
		triggerElement: "#fifthTrigger",
		triggerHook: 0,
		duration: "100%"
	})
	.setClassToggle("#iVision", "active-kk-vision") // add class toggle
	//.addIndicators();

	var fifthIndicator = new ScrollMagic.Scene({
	 	triggerElement: "#sixthTrigger",
	 	triggerHook: 0,
	 	duration: "100%"
	})
	.setClassToggle("#iMap", "active-kk-map") // add class toggle
	// .addIndicators();

	var sixthIndicator = new ScrollMagic.Scene({
		triggerElement: "#kk-happenings",
		triggerHook: 0,
		duration: 602
	})
	.setClassToggle("#iHappenings", "active-kk-happenings") // add class toggle
	//.addIndicators();

	var seventhIndicator = new ScrollMagic.Scene({
		triggerElement: "#kk-gallery",
		triggerHook: 0,
		duration: 860
	})
	.setClassToggle("#iGallery", "active-kk-gallery") // add class toggle
	//.addIndicators();

	var lastIndicator = new ScrollMagic.Scene({
		triggerElement: ".kk-other-developments",
		triggerHook: 0.05,
		duration: 506
	})
	.setClassToggle("#iMore", "active-kk-more") // add class toggle
	//.addIndicators();

	/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*\
	|                           END PAGE INDICATIONS                           |
	\*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

	var sideBarAnimation = TweenMax.fromTo('#kk-indicator', 1, {
		right: "-128px",
		ease: Cubic.easingIn
	}, {
		right: "0px",
		ease: Cubic.easingIn
	});

	var sideBarScene = new ScrollMagic.Scene({
		triggerElement: '#firstTrigger',
		triggerHook: 1,
		duration: "50%"
	})
	.setTween(sideBarAnimation);
	// .addIndicators({name : "LOL"});

	var firstTopCircleAnimation = TweenMax.fromTo('.firstTop', 1, {
		css: {marginLeft: "35vw"},
		ease: Cubic.easingIn
	}, {
		css: {marginLeft: "0"},
		ease: Cubic.easingIn
	});

	var firstTopCircleScene = new ScrollMagic.Scene({
		triggerElement: '#secondTrigger',
		triggerHook: 1,
		duration: "100%"
	})
	.setTween(firstTopCircleAnimation);
	//.addIndicators({name: "(test)"}); // add indicators (requires plugin)

	var firstTopMessageAnimation = TweenMax.fromTo('.topMessageOne', 1, {
		css: {visibility: "hidden", opacity: 0},
		ease: Cubic.easingIn
	}, {
		css: {visibility: "visible", opacity: 1},
		ease: Cubic.easingIn
	})

	var firstTopMessageScene = new ScrollMagic.Scene({
		triggerElement: '#secondTrigger',
		triggerHook: 1,
		duration: "100%"
	})
	.setTween(firstTopMessageAnimation);

	var firstBottomCircleAnimation = TweenMax.fromTo('.firstBottom', 1, {
		css: {marginRight: "35vw"},
		ease: Cubic.easingIn
	}, {
		css: {marginRight: "0"},
		ease: Cubic.easingIn
	});

	var firstBottomCircleScene = new ScrollMagic.Scene({
		triggerElement: '#secondTrigger',
		duration: "50%"
	})
	.setTween(firstBottomCircleAnimation);
	//.addIndicators({name: "(test)"}); // add indicators (requires plugin)

	var firstBottomMessageAnimation = TweenMax.fromTo('.bottomMessageOne', 1, {
		css: {visibility: "hidden", opacity: 0},
		ease: Cubic.easingIn
	}, {
		css: {visibility: "visible", opacity: 1},
		ease: Cubic.easingIn
	})

	var firstBottomMessageScene = new ScrollMagic.Scene({
		triggerElement: '#secondTrigger',
		duration: "50%"
	})
	.setTween(firstBottomMessageAnimation);

	// second Half Scene

	var secondTopCircleAnimation = TweenMax.fromTo('.secondTop', 1, {
		css: {marginRight: "35vw"},
		ease: Cubic.easingIn
	}, {
		css: {marginRight: "0"},
		ease: Cubic.easingIn
	});

	var secondTopCircleScene = new ScrollMagic.Scene({
		triggerElement: '#thirdTrigger',
		triggerHook: 1,
		duration: "100%"
	})
	.setTween(secondTopCircleAnimation);
	//.addIndicators({name: "(test)"}); // add indicators (requires plugin)

	var secondTopMessageAnimation = TweenMax.fromTo('.topMessageTwo', 1, {
		css: {visibility: "hidden", opacity: 0},
		ease: Cubic.easingIn
	}, {
		css: {visibility: "visible", opacity: 1},
		ease: Cubic.easingIn
	})

	var secondTopMessageScene = new ScrollMagic.Scene({
		triggerElement: '#thirdTrigger',
		triggerHook: 1,
		duration: "100%"
	})
	.setTween(secondTopMessageAnimation);

	var secondBottomCircleAnimation = TweenMax.fromTo('.secondBottom', 1, {
		css: {marginLeft: "35vw"},
		ease: Cubic.easingIn
	}, {
		css: {marginLeft: "0"},
		ease: Cubic.easingIn
	});

	var secondBottomCircleScene = new ScrollMagic.Scene({
		triggerElement: '#thirdTrigger',
		duration: "50%"
	})
	.setTween(secondBottomCircleAnimation);
	//.addIndicators({name: "(test)"}); // add indicators (requires plugin)

	var secondBottomMessageAnimation = TweenMax.fromTo('.bottomMessageTwo', 1, {
		css: {visibility: "hidden", opacity: 0},
		ease: Cubic.easingIn
	}, {
		css: {visibility: "visible", opacity: 1},
		ease: Cubic.easingIn
	})

	var secondBottomMessageScene = new ScrollMagic.Scene({
		triggerElement: '#thirdTrigger',
		duration: "50%"
	})
	.setTween(secondBottomMessageAnimation);

	controller.addScene([
		firstIndicator,
		secondIndicator,
		thirdIndicator,
		fourthIndicator,
		fifthIndicator,
		sixthIndicator,
		seventhIndicator,
		lastIndicator,
		sideBarScene,
		firstTopCircleScene,
		firstTopMessageScene,
		firstBottomCircleScene,
		firstBottomMessageScene,
		secondTopCircleScene,
		secondTopMessageScene,
		secondBottomCircleScene,
		secondBottomMessageScene
	]);
});