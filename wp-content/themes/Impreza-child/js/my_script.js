//jQuery(document).ready(function() {
//    jQuery('.navi-kundang-estates a').click(function() {
//       jQuery.scrollTo($('#http://128.199.89.40/our-developments#section-kundang-estates/'), 1000);
//    });
//    jQuery('.navi-twentyfive a').click(function() {
//       jQuery.scrollTo($('#http://128.199.89.40/our-developments#section-twentyfive/'), 1000);
//    });
//});
jQuery(document).ready(function() {
	jQuery('header .l-subheader.at_middle .l-subheader-cell.at_right .ush_btn_1 .w-btn').attr("target", "_blank");
	jQuery('.ourdev-main-slider-modal').prependTo(jQuery('body'));
	jQuery(".chapel-watchvideo-btn").attr("data-toggle", "modal");
	jQuery(".chapel-watchvideo-btn").attr("data-target", ".ourdev-main-slider-modal");
	jQuery(".robert-watchvideo-btn").attr("data-toggle", "modal");
	jQuery(".robert-watchvideo-btn").attr("data-target", ".ourdev-main-slider-modal");
	jQuery(".jadehills-watchvideo-btn").attr("data-toggle", "modal");
	jQuery(".jadehills-watchvideo-btn").attr("data-target", ".ourdev-main-slider-modal");
	jQuery(".horizon-watchvideo-btn").attr("data-toggle", "modal");
	jQuery(".horizon-watchvideo-btn").attr("data-target", ".ourdev-main-slider-modal");
	jQuery(".gem-watchvideo-btn").attr("data-toggle", "modal");
	jQuery(".gem-watchvideo-btn").attr("data-target", ".ourdev-main-slider-modal");
	
	if(jQuery(window).width() <= 768) {
		jQuery('.contact-row .contact-us').prependTo(jQuery('.contact-row'));
	};
	
	if(jQuery(window).width() <= 600) {
		jQuery(".main-page-kundang").attr("height", "250");
		
		
	};
});


jQuery(document).ready(function() {
    if(jQuery(window).width() <= 768) {
        jQuery('.w-nav').html('<i class="mobile-main-nav-open fa fa-bars" aria-hidden="true"></i>');
        jQuery('.mobile-main-nav-open').click(function() {
           jQuery('.mobile-main-nav-all').velocity({left: '0vw'}, {delay: 0, duration: 200});
        });
        jQuery('.mobile-main-nav-close').click(function() {
           jQuery('.mobile-main-nav').velocity({left: '100vh'}, {delay: 0, duration: 200});
        });
        jQuery('.mobile-sub-nav-open').click(function() {
           jQuery('.mobile-main-nav-projects').velocity({left: '0'}, {delay: 0, duration: 200});
        });
        jQuery('.mobile-sub-nav-close').click(function() {
            jQuery('.mobile-main-nav-projects').velocity({left: '100vh'}, {delay: 0, duration: 200});
        });
    }
});

jQuery(document).on('click', '.dropdown-menu-large', function(e) {
  e.stopPropagation();
});

//Sort By
jQuery(document).ready(function() {
    jQuery('.page-id-2810 .dd-selected-text').text('SORT BY');
});


//Calculator
jQuery('.loan-period').val('');

function propertyCalculator() {
    var propertyPrice = jQuery('.property-price').val();
    var loanPeriod = jQuery('.loan-period').val() * 12;
    var interestRate = jQuery('.interest-rate').val() * 0.01;
    var downPayment = jQuery('.down-payment').val();

    var monthlyInstallment = (((propertyPrice - downPayment) + ((propertyPrice - downPayment) * interestRate)) / loanPeriod).toFixed(2);
    var totalInterest = ((propertyPrice - downPayment) * interestRate).toFixed(2);
    var monthlyAverageInterest = (((propertyPrice - downPayment) * interestRate) / loanPeriod).toFixed(2);

    if(propertyPrice, loanPeriod, interestRate, downPayment != '') {
        jQuery('.calculator-popup').css('height', '700');
        jQuery('.result-box').show();
        jQuery('.disclaimer-text').appendTo('.result-box');

        jQuery('.monthly-installment').val(monthlyInstallment);
        jQuery('.total-interest').val((totalInterest));
        jQuery('.monthly-average-interest').val((monthlyAverageInterest));
    } else {
        alert ('Fill out the form again!');
    }

};

function clearInput() {
    jQuery('.property-price, .loan-period, .interest-rate, .down-payment, .monthly-installment, .total-interest, .monthly-average-interest').val('');
};

function propertyReset() {
    jQuery('.calculator-popup').css('height', '470');
    jQuery('.result-box').hide();
    jQuery('.disclaimer-text').appendTo('.calculator-box');
    clearInput();
};

jQuery(document).ready(function() {
    jQuery('.calculator').click(function() {
        jQuery('.calculator-overlay').show();
        jQuery('.calculator-popup').show();
    });
   jQuery('.close-calculator').click(function() {
       jQuery('.calculator-overlay').hide();
       jQuery('.calculator-popup').hide();
       clearInput();
   });
   jQuery('.calculator-overlay').click(function() {
       jQuery('.calculator-overlay').hide();
       jQuery('.calculator-popup').hide();
       clearInput();
   });

   jQuery('#ex1').slider({
		formatter: function(value) {
			return 'Current value: ' + value;
		}
	});
	jQuery("#ex4").slider({
		reversed : true
	});
	jQuery("#ex2").slider({
		reversed : true
	});
	jQuery("#range1").slider({
		reversed : false
	});
	


    jQuery('.page-id-2810 .dd-selected-text').text('SORT BY');
});
function check_all_location(isChecked) {

	if(isChecked) {
		jQuery('.locationcheckbox').each(function() { 
			this.checked = true; 
		});
	} else {
		jQuery('.locationcheckbox').each(function() {
			this.checked = false;
		});
	}
	

}
function check_all_propertytype(isChecked) {

	if(isChecked) {
		jQuery('.propertytypecheckbox').each(function() { 
			this.checked = true; 
		});
	} else {
		jQuery('.propertytypecheckbox').each(function() {
			this.checked = false;
		});
	}
}
function check_all_phase(isChecked) {

	if(isChecked) {
		jQuery('.phasecheckbox').each(function() { 
			this.checked = true; 
		});
	} else {
		jQuery('.phasecheckbox').each(function() {
			this.checked = false;
		});
	}
}

function commaSeparateNumber(index, val) {
    val = val.replace(/\D/g, '');
    var array = val.split('');
    var index = -3;
    while (array.length + index > 0) {
        array.splice(index, 0, ',');
        // Decrement by 4 since we just added another unit to the array.
        index -= 4;
    }
    return array.join('');
};
jQuery(document).on('keyup', '.priceinput', function () {
   //jQuery(this).val(commaSeparateNumber);
});
