<?php
// Template Name:  Bandar Botanic Backup Template
get_header(); ?>

<div id="botanic-indicator" class="scroll-indicator">
    <ul>
        <li id="bb-indicator-home">Home<div class="indicator-circle"></div></li>
        <li id="bb-indicator-overview">Overview<div class="indicator-circle"></div></li>
        <li id="bb-indicator-features">Features<div class="indicator-circle"></div></li>
        <li id="bb-indicator-facilities">Facilities<div class="indicator-circle"></div></li>
        <li id="bb-indicator-master">Master Site Plan<div class="indicator-circle"></div></li>
        <li id="bb-indicator-happenings">Happenings<div class="indicator-circle"></div></li>
        <li id="bb-indicator-more">Other Developments<div class="indicator-circle"></div></li>
    </ul>
</div>


<div id="content-wrapper">
    <div id="botanic-scroll-anim">
        <div class="page-breadcrumbs slider-breadcrumbs" style="position:absolute">
            <p><a href=".">Home</a> <span>/</span> <a href="./developments/">Developments</a> <span>/</span> <a href="#" class="navi-disable">Fully Sold Developments</a> <span>/</span> Bandar Botanic</p>
        </div>
        
        <!-- Bandar Botanic - Landing slide 1 -->
        <div id="text-botanic-land-1" class="text-botanic-land text-box">
            <div class="text-box-left col-md-6"><img src="/wp-content/uploads/2016/07/logo-bandar-botanic.png"></div>
            <div class="text-box-right col-md-6"><h5>WHERE TRANQUILITY<br>SURROUNDS YOU</h5></div>
        </div>
        <div id="botanic-land-slide-1" class="pin-frame"></div>
        
        <!-- Bandar Botanic - Landing slide 2 -->
        <div id="text-botanic-land-2" class="text-botanic-land text-box">
            <div class="text-box-left col-sm-6">
                <h5>ELEGANCE WITHIN A<br>SCENIC TOWNSHIP</h5>
                <svg height="100" width="350">
                    <polygon points="0,0 20,100 350,90 350,15" style="fill:#099f93"></polygon>
                </svg>
            </div>
            <div class="text-box-right col-sm-6"><p>One of the most sought-after developments in the Klang Valley, Bandar Botanic offers you a holistic lifestyle with its meticulously planned "Home in A Garden' concept and host of amenities. Having set the benchmark for developments of its kind, this premier integrated self-sustaining township continues to receive accolades for sustainable value appreciation year after year.</p></div>
        </div>
        <div id="botanic-land-slide-2-left" class="pin-frame bg-wrap-left"></div>
        <div id="botanic-land-slide-2-right" class="pin-frame bg-wrap-right"></div>
        
        <!-- Bandar Botanic - Overview Slide -->
        <div id="botanic-overview-slide-left" class="pin-frame bg-wrap-left"></div>
        <div id="botanic-overview-slide-right" class="pin-frame bg-wrap-right"></div>
		<div id="text-botanic-overview" class="text-box">
			<h5>OVERVIEW</h5>
			<div class="text-box-border">
				<p>A multi-year winner of the Edge-PEPS Value Creation Excellence Award, Bandar Botanic continues to thrive as a premier integrated self-sustaining township. Conceived in the year 2000, this development pioneered the concept of bringing the outdoors, indoors by weaving nature seamlessly into the everyday lives of its community.</p><br>

				<p>Encompassing comprehensive retail and commercial services as well as community amenities, this township has become has become the new benchmark for property developments in the Klang Valley. Built with nature in mind and seeking to make a difference in the way people live, Bandar Botanic offers a harmonious, sustainable environment that will be enjoyed for generations to come.</p>
			</div>
-->
		</div>
        
        <!-- Bandar Botanic - Features Slide -->

        
            <div id="text-botanic-features" class="text-box">
                <ul>
                    <li id="tab1" class="slide-tab">FEATURES</li>
                    <li id="tab2" class="slide-tab">BOTANIC WEST</li>
                    <li id="tab3" class="slide-tab">BOTANIC EAST</li>
                </ul>

                <div class="text-box-border" id="tab1-text">
                    <p>The appeal of Bandar Botanic lies in its rich, tropical landscape. Conceived as "A Home in A Garden", an emphasis has been placed on creating a lush green environment for the community to enjoy the serene beauty of nature from the comfort of their homes. </p><br>

                    <p>To this end, a significant portion of the development has been dedicated to green lungs, parks, lakes and open spaces, all with the aim of encouraging healthy and active lifestyles as well as harmonious community living. </p><br>

                    <p>The Botanic West and Botanic East precincts within Bandar Botanic have been meticulously planned. Each embody their own distinct characteristics, parkland environment, amenities and facilities to provide residents a holistic lifestyle. </p><br>

                    <p>In 2004, Bandar Botanic’s 2-km signature Central Lake and Park, the community’s main recreational hub, was awarded the prestigious Malaysia National Landscape Award 2004 under the City Garden category.</p>
                </div>
                <div class="text-box-border" id="tab2-text">
                    <p>Botanic West is Bandar Botanic's very own 70-acre botanical parkland area encompassing a wide expanse of verdant greens, tree-lined boulevards and thematic gardens. It is this botanical parkland that gives the township its identity and provides the community with an avenue for rest, recreation and community interaction.</p><br>

                    <p>Every detail of Botanic West's Central Lake and Park was purposefully planned to give the township a distinct botanical parkland character. The 2-km Central Lake and Park is laden with luxurious thematic gardens that include a topiary garden, sundial garden, alphabet garden, children’s island and culinary garden as well as exciting water cascades beautifully landscaped for the community to enjoy. Other features include a flower bridge, flower tunnel, amphitheatre, waterwheel, gazebos and floating boardwalk.</p>
                </div>
                <div class="text-box-border" id="tab3-text">
                    <p>With its offer of a 100-acre waterfront-parkland setting and its resort-styled ambience, residents of Botanic East get to enjoy carefree evenings relishing the breathtaking views of pristine green surroundings and waterways.</p><br>

                    <p>Lush greenery and themed features are aplenty at Botanic East. With names such as Children's Island, Rainbow Lake, Floating Garden, Meandering Creek, Pocket Garden, Ribbon Lake, Crescent Lake and Hanami Garden, each themed feature adds its own distinct flavour to make this tranquil enclave a unique neighbourhood.</p>
                </div>
                <svg id="tab1-bg" height="630" width="630">
                    <polygon points="15,0 0,601 600,611 630,40" style="fill:rgba(7,129,122,0.8)"></polygon>
                </svg>
                <svg id="tab2-bg" height="500" width="650">
                    <polygon points="0,0 40,500 600,500 650,20" style="fill:rgba(7,129,122,0.8)"></polygon>
                </svg>
                <svg id="tab3-bg" height="410" width="650">
                    <polygon points="0,30 40,410 620,390 650,0" style="fill:rgba(7,129,122,0.8)"></polygon>
                </svg>
            </div>
        <div id="botanic-features-slide-1-left" class="pin-frame bg-wrap-left">
        </div>
        <div id="botanic-features-slide-1-right" class="pin-frame bg-wrap-right">
        </div>
        <div id="botanic-features-slide-2" class="pin-frame">
        </div>
        <div id="botanic-features-slide-3-left" class="pin-frame bg-wrap-left">
        </div>
        <div id="botanic-features-slide-3-right" class="pin-frame bg-wrap-right">
        </div>


    
        <!-- Bandar Botanic - Facilities Slide -->
        <div id="botanic-facilities-slide-left" class="pin-frame bg-wrap-left">
        </div>
        <div id="botanic-facilities-slide-right" class="pin-frame bg-wrap-right">
        </div>
        <div id="text-botanic-facilities" class="text-box">
            <h5>FACILITIES &amp; AMENITIES</h5>
            <div class="text-box-border">
                <p>Bandar Botanic was built to meet the needs of the community and everything residents need is at their doorstep.</p><br>

                <p>Residents have easy access to the largest Southeast Asian wholesale city - GM Klang, Aeon Mall, Giant and Tesco, plus a host of other retail and commercial facilities that dot the neighbourhood. Public transportation is readily available to take residents straight to urban centres like Klang and back.</p><br>

                <p>The Central Botanic Parkland has been designed as a social and recreational hub for the community and is situated within walking distance. On top of this, amenities such as schools, kindergartens, community halls, places of worship, local shops and bus stops are inter-connected with the parkland via series of "green fingers" to reduce pedestrian-vehicular conflict and to promote community living. </p><br>

                <p>In short, everything that is fundamental to enhancing a community’s lifestyle has been thought of.</p>
            </div>
            <!--<div id="facilities-list">
                <ul>
                    <li><div class="list-style-tick"></div>Kindergartens</li>
                    <li><div class="list-style-tick"></div>Sport/Recreational Space</li>
                    <li><div class="list-style-tick"></div>Multi-purpose Hall</li>
                    <li><div class="list-style-tick"></div>Post Office</li>
                    <li><div class="list-style-tick"></div>Police Station</li>
                    <li><div class="list-style-tick"></div>Public Transport Facilities</li>
                    <li><div class="list-style-tick"></div>Petrol Station</li>
                    <li><div class="list-style-tick"></div>Hotel/ Shopping Complex/ Clinics/ Banks</li>
                </ul>
            </div>-->
            <svg height="620" width="670">
                <polygon points="15,0 0,601 630,611 620,40" style="fill:rgba(7,129,122,0.8)"></polygon>
            </svg>
        </div>
    
        <!-- Bandar Botanic - Master Site Plan Slide -->
        <div id="text-botanic-master-1" class="text-box">
            <p>Bandar Botanic was conceived as a waterfront-parkland where the endless meandering lakes and parks would give the township a botanical feel.</p><br>

            <p>To achieve this concept, Gamuda Land’s team of creative and innovative engineers and architects began at the foundation by working on a meticulous and well-thought-out masterplan. They placed a major emphasis on providing a green and healthy environment for the community. To this end, more than 180 acres of the township were dedicated to greens, parks and lakes to provide the community a holistic, quality lifestyle.</p><br>

            <p>The plan and design for the township also incorporated a multitude of value-added features such as superior infrastructure, an abundance of parks and greenery, as well as a host of other community-centric facilities and amenities.</p>
        </div>
        <div id="map-botanic-master-1" class="text-box">
            <h5 style="color:#fff;">MASTER SITE PLAN</h5>
            <img src="/wp-content/uploads/2016/08/slide-master-slide-1-map.png">
        </div>
        <div id="botanic-master-slide-1" class="pin-frame">
        </div>
        
        <div id="botanic-master-slide-2" class="pin-frame">
        </div>
        
        <div id="text-botanic-master-2-left" class="text-box">
            <div class="text-box-border">
                <p>To underscore Gamuda Land’s commitment to providing ready connectivity to Bandar Botanic, we constructed a RM30 million dedicated interchange to link the township directly to the Shah Alam Expressway.</p><br>

				<p>In line with our efforts to uphold good environmental architecture practices, we brought several innovative solutions into play. Bandar Botanic was the first development to not only comply with, but also exceed the mandatory requirements of the Department of Irrigation and Drainage’s Urban Stormwater Management System (MASMA). Leveraging on a novel Floating Pile Raft Foundation System, we transformed the basic irrigation network within the development into an extensively landscaped park that elevated the standard of living in the area as well as the value of the surrounding properties. </p><br>

				<p>Today the Central Lake Park functions as a detention and regulating pond for surface water run-off, while its themed parks and gardens as well as leisure activity areas are well utilised by the community.</p>
            </div>
        </div>
        
        <div id="text-botanic-master-2-right" class="text-box">
            <div class="text-box-border">
                <p>Botanic West features a unique 70-acre botanical parkland with a 2-km lake. Homes here are designed to look over the Central Lake and are surrounded by beautifully landscaped pocket parks that truly add to the tranquillity and privacy of the botanical haven.</p><br>

                <p>Botanic East offers unique waterfront parkland with lush landscaping, private gardens and green lungs in a botanical environment.</p><br>

                <p>The waterfront with its lavish parks and gardens provide a serene and tranquil ambience to this enclave of bungalow homes, semi-detached homes and garden terraces.</p>
            </div>
        </div>
        
        <div id="text-botanic-master-3" class="text-box col-lg-5 col-lg-offset-5 col-md-8 col-md-offset-2">
            <div id="text-botanic-master-3-top" class="text-box col-lg-10 col-md-9 col-md-offset-1">
                <h5><span style="color:#000">MASTER SITE PLAN - </span><span style="color:#add043">RESIDENTIAL</span></h5>
                <img src="/wp-content/uploads/2016/08/slide-master-slide-3-map.png">
            </div>
            <div id="text-botanic-master-3-bottom" class="text-box">
                <div class="text-box-border">
                    <p>Each precinct is characterised by its natural setting which gives it a distinctive feature. Each of the homes, regardless of whether it is a luxury bungalow or garden terrace, is designed with novel features and exciting architectural designs that enhances its uniqueness.</p><br>

                    <p>The homes, which are designed and planned to benefit from the aesthetics of the natural world, offer close affinity to the surrounding greenery, landscaping and water features. Liberal features such as expansive terraces, spacious courtyards and tall windows allow greater freedom for each home to fuse with its surroundings. To ensure residents enjoy optimum views of the waterways and expansive greens, homes are designed with water frontages or garden frontages. </p><br>

                    <p>To ensure maximum benefit to the community, many of the development’s finer details have been meticulously planned. These include features such as underground utilities, roll-over kerbs, pedestrian-safe walkways and an efficient road and traffic management systems. Another effective neighbourhood feature to enhance the security and privacy of residents is the cul-de-sac road that ensures only one entry and exit point to homes.</p>
                </div>
            </div>
        </div>
        <div id="botanic-master-slide-3-left" class="pin-frame bg-wrap-left">
        </div>
        <div id="botanic-master-slide-3-right" class="pin-frame bg-wrap-right">
        </div>
        
        
        <div id="text-botanic-master-4" class="text-box col-lg-5 col-lg-offset-1 col-md-8 col-md-offset-2">
            <div id="text-botanic-master-4-top" class="text-box col-lg-10 col-md-9 col-md-offset-1">
                <h5><span style="color:#000">MASTER SITE PLAN - </span><span style="color:#1a4db3">COMMERCIAL</span></h5>
                <img src="/wp-content/uploads/2016/08/slide-master-slide-4-map.png">
            </div>
            <div id="text-botanic-master-4-bottom" class="text-box">
                <div class="text-box-border">
                    <p>The community at Bandar Botanic have ready access to a bevy of everyday conveniences. These include ample shopping, dining and entertainment centres such as Aeon Mall, Tesco, Giant and the GM Klang Wholesale City.</p><br>

                    <p>Within its boundaries, Bandar Botanic also offers exclusive and exciting shopping and business experiences at the Botanic Walk Lakefront Commercial Centre, Botanic Capital, Botanic Avenue and Botanic Business Gateway.</p><br>

                    <p>The commercial centre comprising Botanic Walk East and West is designed as a community venue for families to enjoy the pleasures of life be it by way of recreational activities, meeting friends and family, or just relaxing and enjoying a good meal. Strategically located alongside Central Lake Park, the centre provides residents a unique shopping experience.</p><br>

                    <p>Meanwhile, the commercial hub comprising Botanic Capital, Botanic Avenue and Botanic Business Gateway encompasses two, three and four-storey shop offices. These house a shopping mall, boutique shops, retail shop offices, an automotive showroom and corporate offices, among others.</p>
                </div>
            </div>
        </div>
        <div id="botanic-master-slide-4-left" class="pin-frame bg-wrap-left">
        </div>
        <div id="botanic-master-slide-4-right" class="pin-frame bg-wrap-right">
        </div>
		
        <div id="slide-bb-happenings" class="pin-frame">
			<?php echo do_shortcode('[vc_row bg_type="image" parallax_style="vcpb-default" css=".vc_custom_1472548408602{padding-bottom: 0px !important;}"][vc_column css=".vc_custom_1470954605359{padding-bottom: 0px !important;}"][vc_column_text]
<h3 style="text-align: left;"><strong><span style="color: #fff;">HAPPENINGS </span></strong></h3>
[/vc_column_text][svc_post_layout svc_excerpt_length="3" hide_showmore="yes" dcategory="yes" dmeta_data="yes" dsocial="yes" dpost_popup="yes" dimg_popup="yes" dfeatured="yes" query_loop="size:3|order_by:date|order:DESC|post_type:,post|categories:22" line_color="rgba(0,0,0,0.01)" tcolor="#000000"][us_btn text="Discover More" link="url:http%3A%2F%2F128.199.89.40%2Fhappenings%2F|||" style="outlined" color="teal" size="15" align="center" el_class="btn-KK-discovery"][/vc_column][/vc_row]'); ?>
        </div>
        <div id="slide-bb-other-dev" class="pin-frame">
			<?php echo do_shortcode('[vc_row columns_type="none" height="auto" width="full" bg_type="image" parallax_style="vcpb-default" css=".vc_custom_1472548427987{margin-right: 0px !important;margin-left: 0px !important;padding-right: 0px !important;padding-left: 0px !important;}" el_id="section-next-prev-proj"][vc_column css=".vc_custom_1470677939006{margin-top: 0px !important;margin-right: 0px !important;margin-bottom: 0px !important;margin-left: 0px !important;padding-top: 0px !important;padding-right: 0px !important;padding-bottom: 0px !important;padding-left: 0px !important;}" offset="vc_col-xs-6"][vc_row_inner columns_type="large" css=".vc_custom_1471266975577{padding-top: 40px !important;padding-bottom: 40px !important;}"][vc_column_inner][vc_raw_html]JTNDZGl2JTIwY2xhc3MlM0QlMjJjb250YWluZXIlMjIlM0UlM0NoMyUyMHN0eWxlJTNEJTIydGV4dC1hbGlnbiUzQSUyMGxlZnQlM0IlMjIlM0UlM0NzdHJvbmclM0UlM0NzcGFuJTIwc3R5bGUlM0QlMjJjb2xvciUzQSUyMCUyM2ZmZiUzQiUyMiUzRU9USEVSJTIwREVWRUxPUE1FTlRTJTNDJTJGc3BhbiUzRSUzQyUyRnN0cm9uZyUzRSUzQyUyRmgzJTNFJTNDJTJGZGl2JTNF[/vc_raw_html][/vc_column_inner][/vc_row_inner][vc_row_inner css=".vc_custom_1471267455851{padding-right: 120px !important;}"][vc_column_inner width="1/2" css=".vc_custom_1471267317228{padding-right: 0px !important;}"][us_single_image image="5725" size="full" link="url:http%3A%2F%2F128.199.89.40%3A32773%2Fdevelopments%2Ffully-sold-developments%2Fbukit-bantayan%2F|||" el_class="pp-img" css=".vc_custom_1472125461817{padding-top: 0px !important;padding-right: 0px !important;padding-bottom: 0px !important;padding-left: 0px !important;}"][vc_raw_html]JTNDZGl2JTIwY2xhc3MlM0QlMjJwcC1zZWN0aW9uJTIyJTNFJTBBJTNDaSUyMGNsYXNzJTNEJTIyRGVmYXVsdHMtYW5nbGUtbGVmdCUyMHBwLWljb24lMjIlM0UlM0MlMkZpJTNFJTBBJTNDc3BhbiUyMGNsYXNzJTNEJTIycHAtdGV4dCUyMiUzRVByZXZpb3VzJTIwRGV2ZWxvcG1lbnQlM0MlMkZzcGFuJTNFJTBBJTNDJTJGZGl2JTNF[/vc_raw_html][/vc_column_inner][vc_column_inner width="1/2" css=".vc_custom_1471267331474{padding-left: 0px !important;}"][us_single_image image="719" size="full" link="url:http%3A%2F%2F128.199.89.40%3A32773%2Fdevelopments%2Ffully-sold-developments%2Fmadge-mansions%2F|||" el_class="np-img"][vc_raw_html]JTNDZGl2JTIwY2xhc3MlM0QlMjJucC1zZWN0aW9uJTIyJTNFJTBBJTNDc3BhbiUyMGNsYXNzJTNEJTIybnAtdGV4dCUyMiUzRU5leHQlMjBEZXZlbG9wbWVudCUzQyUyRnNwYW4lM0UlMEElM0NpJTIwY2xhc3MlM0QlMjJEZWZhdWx0cy1hbmdsZS1yaWdodCUyMG5wLWljb24lMjIlM0UlM0MlMkZpJTNFJTBBJTNDJTJGZGl2JTNF[/vc_raw_html][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]'); ?>
        </div>
        
    </div>
</div>

<?php get_footer();
// Omit closing PHP tag to avoid "Headers already sent" issues.