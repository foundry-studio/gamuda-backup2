<?php
// Template Name:  Milestone Template
get_header(); ?>


<div class="page-breadcrumbs slider-breadcrumbs">
    <p><a href="http://tjclients.my/gamuda-staging/">Home</a> <span>/</span> <a href="http://tjclients.my/gamuda-staging/about-us/">About Us</a> <span>/</span> Milestone</p>
</div>

<div id="fullpage" class="milestones-scroll">

	
	<div class="section" id="section1">
        <div class="mouse">
            <div class="mouse-icon">
                <span class="mouse-wheel"></span>
            </div>
        </div>
        <div id="text-milestone-land" class="text-box">
            <h3 style="text-align:center;">OUR MILESTONES</h3>
        </div>
	</div>
	
	<div class="section moveDown" id="section2">
        <div class="container milestone-container">
            <div id="year-circle-2016-1" class="year-circle"><h3 class="dancing-script">2016</h3></div>
            <div id="text-2016-1" class="text-box">
                <h5>GEM RESIDENCES MARKS GAMUDA LAND'S MAIDEN FORAY INTO SINGAPORE</h5>
                <a href="#">Discover More</a>

            </div>
            <div id="line-2016-1" class="line-box"></div>
			
        </div>
	</div>
	
	<div class="section moveDown" id="section3">
        <div class="container milestone-container" style="z-index:2">
            <div id="year-circle-2016-5" class="year-circle"><h3 class="dancing-script">2016</h3></div>
            <div id="text-2016-5" class="text-box">
                <h5>KUNDANG ESTATES, THE EMBODIMENT OF MODERN COUNTRYSIDE LIVING, IS LAUNCHED</h5>

            </div>
            <div id="line-2016-5" class="line-box"></div>
        </div>
		<div class="scroll-long">
			<div class="half-houses-30000 bg-wrap-left"></div>
			<div class="half-kundang bg-wrap-left"></div>
		</div>
	</div>
	<div class="section moveDown" id="section4">
        <div class="container milestone-container milestone-container-middle">
            <div id="year-circle-2016-6" class="year-circle"><h3 class="dancing-script">2016</h3></div>
            <div id="text-2016-6" class="text-box">
                <h3>30,000 HOMES HANDED OVER</h3>
            </div>
        </div>
		<div class="houses-30000 bg-wrap-right"></div>
	</div>
	<div class="section moveDown" id="section5">
        <div class="container milestone-container">
            <div id="year-circle-2016-7" class="year-circle"><h3 class="dancing-script">2016</h3></div>
            <div id="text-2016-7" class="text-box">
                <h5>THE BUKIT BANTAYAN RESIDENCES DEVELOPMENT MARKS OUR MAIDEN ENTRY INTO EAST MALAYSIA</h5>

            </div>
            <div id="line-2016-7" class="line-box"></div>
        </div>
	</div>
	<div class="section moveDown" id="section6">
        <div class="container milestone-container">
            <div id="year-circle-2015-1" class="year-circle"><h3 class="dancing-script">2015</h3></div>
            <div id="text-2015-2" class="text-box">
                <h5>WE WIN THE TENDER FOR A 3-ACRE PLOT OF LAND IN SINGAPORE</h5>

            </div>
            <div id="line-2015-2" class="line-box"></div>
        </div>
	</div>
	<div class="section moveDown" id="section7">
        <div class="container milestone-container">
            <div id="year-circle-2015-2" class="year-circle"><h3 class="dancing-script">2015</h3></div>
            <div id="text-2015-1" class="text-box">
                <h5>GAMUDA LAND ENTERS THE AUSTRALIAN MARKET WITH 661 CHAPEL STREET</h5>
            </div>
            <div id="line-2015-1" class="line-box"></div>
        </div>
	</div>
	<div class="section moveDown" id="section8">
        <div class="container milestone-container">
            <div id="year-circle-2015-3" class="year-circle"><h3 class="dancing-script">2015</h3></div>
            <div id="text-2015-3" class="text-box">
                <h5>OUR FIRST HIGH-RISE WELLNESS DEVELOPMENT, HIGHPARK SUITES IN KELANA JAYA, IS LAUNCHED </h5>
            </div>
            <div id="line-2015-3" class="line-box"></div>
        </div>
	</div>
	<div class="section moveDown" id="section9">
        <div class="container milestone-container" style="z-index:2">
            <div id="year-circle-2013-1" class="year-circle"><h3 class="dancing-script">2013</h3></div>
            <div id="text-2013-1" class="text-box">
                <h5>THE ROBERTSON BECOMES OUR PIONEEER HIGH RISE INTEGRATED DEVELOPMENT</h5>
            </div>
            <div id="line-2013-1" class="line-box"></div>
        </div>
		<div class="scroll-long">
			<div class="half-robertson bg-wrap-left"></div>
			<div class="half-landbank bg-wrap-left"></div>
		</div>
	</div>
	<div class="section moveDown" id="section10">
        <div class="container milestone-container milestone-container-middle">
            <div id="year-circle-2013-2" class="year-circle"><h3 class="dancing-script">2013</h3></div>
            <div id="text-2013-2" class="text-box">
                <h3>OUR LAND BANK REACHES THE<br>5,000-ACRE MARK</h3>
            </div>
        </div>
	</div>
	<div class="section moveDown" id="section11">
        <div class="container milestone-container">
            <div id="year-circle-2012-1" class="year-circle"><h3 class="dancing-script">2012</h3></div>
            <div id="text-2012-1" class="text-box">
                <h5>MADGE MANSIONS HAS THE DISTINCTION OF BEING OUR MAIDEN LUXURY CONDOMINIUM</h5>
            </div>
            <div id="line-2012-1" class="line-box"></div>
        </div>
	</div>
	<div class="section moveDown" id="section12">
        <div class="container milestone-container">
            <div id="year-circle-2011-1" class="year-circle"><h3 class="dancing-script">2011</h3></div>
            <div id="text-2011-1" class="text-box">
                <h5>GAMUDA LAND STRENGTHENS FOOTPRINT IN VIETNAM WITH CELADON CITY DEVELOPMENT</h5>
            </div>
            <div id="line-2011-1" class="line-box"></div>
        </div>
	</div>
	<div class="section moveDown" id="section13">
        <div class="container milestone-container">
            <div id="year-circle-2008-1" class="year-circle"><h3 class="dancing-script">2008</h3></div>
            <div id="text-2008-1" class="text-box">
                <h5>JADE HILLS IN KAJANG, GAMUDA LAND'S FIRST THEMATIC DEVELOPMENT</h5>
            </div>
            <div id="line-2008-1" class="line-box"></div>
        </div>
	</div>
	<div class="section moveDown" id="section14">
        <div class="container milestone-container">
            <div id="year-circle-2007-1" class="year-circle"><h3 class="dancing-script">2007</h3></div>
            <div id="text-2007-1" class="text-box">
                <h5>GAMUDA LAND ENTERS ISKANDAR MALAYSIA WITH ITS FIRST PROPERTY DEVELOPMENT - HORIZON HILLS</h5>
            
            </div>
            <div id="line-2007-1" class="line-box"></div>
        </div>
	</div>
	<div class="section moveDown" id="section15">
        <div class="container milestone-container" style="z-index:2">
            <div id="year-circle-2007-2" class="year-circle"><h3 class="dancing-script">2007</h3></div>
            <div id="text-2007-2" class="text-box">
                <h5>GAMUDA CITY IN VIETNAM BECOMES GAMUDA LAND'S FIRST INTERNATIONAL DEVELOPMENT</h5>
            
            </div>
            <div id="line-2007-2" class="line-box"></div>
        </div>
		<div class="scroll-long">
			<div class="half-gamudacity bg-wrap-left"></div>
			<div class="half-gamudaland bg-wrap-left"></div>
		</div>
	</div>
	<div class="section moveDown" id="section16">
        <div class="container milestone-container milestone-container-middle">
            <div id="year-circle-2005-1" class="year-circle"><h3 class="dancing-script">2005</h3></div>
            <div id="text-2005-1" class="text-box">
                <h3>GAMUDA LAND HANDS OVER<br>10,000 HOUSES</h3>
            </div>
        </div>
	</div>
	<div class="section moveDown" id="section17">
        <div class="container milestone-container">
            <div id="year-circle-2001-1" class="year-circle"><h3 class="dancing-script">2001</h3></div>
            <div id="text-2001-1" class="text-box">
                <h5>BANDAR BOTANIC BECOMES THE FIRST FIABCI AWARD-WINNING TOWNSHIP IN KLANG</h5>
            </div>
            <div id="line-2001-1" class="line-box"></div>
        </div>
	</div>
	<div class="section moveDown" id="section18">
        <div class="container milestone-container">
            <div id="year-circle-2000-1" class="year-circle"><h3 class="dancing-script">2000</h3></div>
            <div id="text-2000-1" class="text-box">
                <h5>WE INTRODUCE THE FIRST RESORT-INSPIRED DEVELOPMENT WITH A PRIVATE GOLF COURSE</h5>
            </div>
            <div id="line-2000-1" class="line-box"></div>
        </div>
	</div>
	<div class="section moveDown" id="section19">
        <div class="container milestone-container">
            <div id="year-circle-1995-1" class="year-circle"><h3 class="dancing-script">1995</h3></div>
            <div id="text-1995-1" class="text-box">
                <h5>KOTA KEMUNING, THE FIRST INTEGRATED TOWNSHIP DEVELOPMENT IS LAUNCHED</h5>
            </div>
            <div id="line-1995-1" class="line-box"></div>
        </div>
	</div>
	<div class="section moveDown" id="section20">
        <div class="container milestone-container">
            <div id="year-circle-1995-2" class="year-circle"><h3 class="dancing-script">1995</h3></div>
            <div id="text-1995-2" class="text-box">
                <h5>GAMUDA LAND SDN BHD IS ESTABLISHED</h5>
            </div>
        </div>
	</div>
	<div class="section moveDown" id="section21">
		<div class="more-about-us-section">
			<div class="container" style="padding:0;min-width:1200px"><h3>MORE ABOUT US</h3></div>
			<div class="container-fluid" style="padding:0;">
				<div class="col-sm-6 pp-img"><a href="http://128.199.89.40/about-us/awards/"><img src="/wp-content/uploads/2016/08/awards.jpg" alt="Go to Awards Page"></a></div>
				<div class="col-sm-6 np-img"><a href="http://128.199.89.40/about-us/philosophy/"><img src="/wp-content/uploads/2016/08/philosophy.jpg" alt="Go to Philosophy Page"></a></div>
			</div>
		</div>
	</div>
</div>
<script>
    jQuery(document).ready(function(){
        jQuery('.page-id-349 .l-footer').wrap("<div class='section moveDown fp-section milestone-footer' id='section22'></div>");
        jQuery('.page-id-349 #section22').appendTo('.page-id-349 #fullpage')
    });
</script>
<?php get_footer();
// Omit closing PHP tag to avoid "Headers already sent" issues.