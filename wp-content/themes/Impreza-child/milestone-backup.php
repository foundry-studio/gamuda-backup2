<?php
// Template Name:  Milestone Template Backup
get_header(); ?>

<!-- Side Bar -->
<!--
<div class="side-banner">
    <div class="buyers-guide">
        <a href="http://192.168.99.100:32773/buyers-guide/">
        <img src="/wp-content/uploads/2016/07/icon-buyers-guide.png" alt="Buyer's Guide">
        </a>
        <p>Buyer's<br>Guide</p>
    </div>
    <hr class="side-banner-separator">
    <div class="calculator">
        <img src="/wp-content/uploads/2016/07/icon-calculator.png" alt="Calculator">
        <p>Calculator</p>
    </div>
</div>

<div class="calculator-overlay"></div>
<div class="row calculator-popup">
    <div class="row calculator-box">
        <div class="col-md-12 calculator-title">
            <h2>MORGAGE CALCULATOR</h2>
            <span class="glyphicon glyphicon-remove-circle close-calculator" aria-hidden="true"></span>
        </div>
        <div class="row calculator-row">
            <div class="col-md-6 calculator-item">
                <h3>Property Price <span>(MYR)</span></h3>
            </div>
            <div class="col-md-6 calculator-input">
                <input class="form-control input-lg property-price" type="number" min="0">
            </div>
        </div>
        <div class="row calculator-row">
            <div class="col-md-6 calculator-item">
                <h3>Loan Period <span>(Years)</span></h3>
            </div>
            <div class="col-md-6 calculator-input">
                <select class="form-control input-lg loan-period">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                    <option value="13">13</option>
                    <option value="14">14</option>
                    <option value="15">15</option>
                    <option value="16">16</option>
                    <option value="17">17</option>
                    <option value="18">18</option>
                    <option value="19">19</option>
                    <option value="20">20</option>
                    <option value="21">21</option>
                    <option value="22">22</option>
                    <option value="23">23</option>
                    <option value="24">24</option>
                    <option value="25">25</option>
                    <option value="26">26</option>
                    <option value="27">27</option>
                    <option value="28">28</option>
                    <option value="29">29</option>
                    <option value="30">30</option>
                    <option value="31">31</option>
                    <option value="32">32</option>
                    <option value="33">33</option>
                    <option value="34">34</option>
                    <option value="35">35</option>
                    <option value="36">36</option>
                    <option value="37">37</option>
                    <option value="38">38</option>
                    <option value="39">39</option>
                    <option value="40">40</option>
                </select>
            </div>
        </div>
        <div class="row calculator-row">
            <div class="col-md-6 calculator-item">
                <h3>Expected Interest Rate <span>(%)</span></h3>
            </div>
            <div class="col-md-6 calculator-input">
                <input class="form-control input-lg interest-rate" type="number" min="0">
            </div>
        </div>
        <div class="row calculator-row">
            <div class="col-md-6 calculator-item">
                <h3>Down Payment <span>(MYR)</span></h3>
            </div>
            <div class="col-md-6 calculator-input">
                <input class="form-control input-lg down-payment" type="number" min="0">
            </div>
        </div>
        <div class="row calculator-btn">
            <div class="col-md-6 reset-btn">
                <button class="calculator-reset" type="reset" onclick="propertyReset()">Reset</button>
            </div>
            <div class="col-md-6 submit-btn">
                <button class="calculator-submit" type="Submit" onclick="propertyCalculator()">Calculate</button>
            </div>
        </div>
        <div class="row disclaimer-text">
            <p><span>Disclaimer: </span>The above is an estimation of the repayment amount.</p>
        </div>
    </div>
    <div class="row result-box">
        <div class="row result-box-seperator">
            <hr class="col-md-12">
        </div>
        <div class="row calculator-row">
            <div class="col-md-6 calculator-item">
                <h3>Monthly Installment <span>(MYR)</span></h3>
            </div>
            <div class="col-md-6 calculator-input">
                <input class="form-control input-lg monthly-installment" type="text" readonly>
            </div>
        </div>
        <div class="row calculator-row">
            <div class="col-md-6 calculator-item">
                <h3>Total Interest <span>(MYR)</span></h3>
            </div>
            <div class="col-md-6 calculator-input">
                <input class="form-control input-lg total-interest" type="text" readonly>
            </div>
        </div>
        <div class="row calculator-row">
            <div class="col-md-6 calculator-item">
                <h3>Monthly Average Interest <span>(MYR)</span></h3>
            </div>
            <div class="col-md-6 calculator-input">
                <input class="form-control input-lg monthly-average-interest" type="text" readonly>
            </div>
        </div>
    </div>
</div>
-->

<div class="page-breadcrumbs slider-breadcrumbs">
    <p><a href="http://192.168.99.100:32773/">Home</a> <span>/</span> <a href="http://192.168.99.100:32773/about-us/">About Us</a> <span>/</span> Milestone</p>
</div>
<div id="ms-indicator" class="scroll-indicator">
    <ul>
        <li id="ms-indicator-home">Home<div class="indicator-circle"></div></li>
        <li id="ms-indicator-2016">2016<div class="indicator-circle"></div></li>
        <li id="ms-indicator-2015">2015<div class="indicator-circle"></div></li>
        <li id="ms-indicator-2013">2013<div class="indicator-circle"></div></li>
        <li id="ms-indicator-2012">2012<div class="indicator-circle"></div></li>
        <li id="ms-indicator-2010">2010<div class="indicator-circle"></div></li>
        <li id="ms-indicator-2007">2007<div class="indicator-circle"></div></li>
        <li id="ms-indicator-2006">2006<div class="indicator-circle"></div></li>
        <li id="ms-indicator-2005">2005<div class="indicator-circle"></div></li>
        <li id="ms-indicator-1999">1999<div class="indicator-circle"></div></li>
        <li id="ms-indicator-1998">1998<div class="indicator-circle"></div></li>
        <li id="ms-indicator-1995">1995<div class="indicator-circle"></div></li>
        <li id="ms-indicator-learn">Learn more<div class="indicator-circle"></div></li>
    </ul>
</div>


<div id="content-wrapper">
    <!-- Scroll Animation -->
    <div id="about-us-scroll-anim">
        <!-- Milestone - Landing slide -->
        <div id="text-milestone-land" class="text-box">
            <h3>OUR MILESTONES</h3>
            <p>Now and then</p>
        </div>
        <div id="milestone-land-left" class="pin-frame bg-wrap-left"></div>
        <div id="milestone-land-right" class="pin-frame bg-wrap-right"></div>

        <!-- Milestone - 2016 Slide 1 -->
        <div id="year-circle-2016-1" class="year-circle"><h3 class="dancing-script">2016</h3></div>
        <div id="text-2016-1" class="text-box">
            <h5>GEM RESIDENCES MARKS GAMUDA LAND'S MAIDEN FORAY INTO SINGAPORE</h5>
            
        </div>
        <div id="line-2016-1" class="line-box"></div>
        <div id="milestone-slide-2016-1-left" class="pin-frame bg-wrap-left"></div>
        <div id="milestone-slide-2016-1-right" class="pin-frame bg-wrap-right"></div>

        <!-- Milestone - 2016 Slide 2 -->
        <div id="year-circle-2016-2" class="year-circle"><h3 class="dancing-script">2016</h3></div>
        <div id="text-2016-2" class="text-box">
            <h5>NEW LAUNCH: GAMUDA COVE</h5>
            
        </div>
        <div id="line-2016-2" class="line-box"></div>
        <div id="milestone-slide-2016-2-left" class="pin-frame bg-wrap-left"></div>
        <div id="milestone-slide-2016-2-right" class="pin-frame bg-wrap-right"></div>
        
        <!-- Milestone - 2016 Slide 3 -->
        <div id="year-circle-2016-3" class="year-circle"><h3 class="dancing-script">2016</h3></div>
        <div id="text-2016-3" class="text-box">
            <h5>LAUNCH OF TWENTYFIVE.7 - THE VIBRANT NEW PULSE OF KOTA KEMUNING</h5>
            
        </div>
        <div id="line-2016-3" class="line-box"></div>
        <div id="milestone-slide-2016-3-left" class="pin-frame bg-wrap-left"></div>
        <div id="milestone-slide-2016-3-right" class="pin-frame bg-wrap-right"></div>
        
        <!-- Milestone - 2016 Slide 4 -->
        <div id="year-circle-2016-4" class="year-circle"><h3 class="dancing-script">2016</h3></div>
        <div id="text-2016-4" class="text-box">
            <h5>NEW LAUNCH: GAMUDA GARDENS</h5>
            
        </div>
        <div id="line-2016-4" class="line-box"></div>
        <div id="milestone-slide-2016-4-left" class="pin-frame bg-wrap-left"></div>
        <div id="milestone-slide-2016-4-right" class="pin-frame bg-wrap-right"></div>
        
        <!-- Milestone - 2016 Slide 5 -->
        <div id="year-circle-2016-5" class="year-circle"><h3 class="dancing-script">2016</h3></div>
        <div id="text-2016-5" class="text-box">
            <h5>KUNDANG ESTATES, THE EMBODIMENT OF MODERN COUNTRYSIDE LIVING, IS LAUNCHED</h5>
            
        </div>
        <div id="line-2016-5" class="line-box"></div>
        <div id="milestone-slide-2016-5-left" class="pin-frame bg-wrap-left"></div>
        <div id="milestone-slide-2016-5-right" class="pin-frame bg-wrap-right"></div>
        
        <!-- Milestone - 2016 Slide 6 -->
        <div id="year-circle-2016-6" class="year-circle"><h3 class="dancing-script">2016</h3></div>
        <div id="text-2016-6" class="text-box">
            <h3>30,000 HOUSES HANDED OVER</h3>
        </div>
        <div id="milestone-slide-2016-6-left" class="pin-frame bg-wrap-left"></div>
        <div id="milestone-slide-2016-6-right" class="pin-frame bg-wrap-right"></div>
        
        <!-- Milestone - 2016 Slide 7 -->
        <div id="year-circle-2016-7" class="year-circle"><h3 class="dancing-script">2016</h3></div>
        <div id="text-2016-7" class="text-box">
            <h5>THE BUKIT BANTAYAN RESIDENCES DEVELOPMENT MARKS OUR MAIDEN ENTRY INTO EAST MALAYSIA</h5>
            
        </div>
        <div id="line-2016-7" class="line-box"></div>
        <div id="milestone-slide-2016-7-left" class="pin-frame bg-wrap-left"></div>
        <div id="milestone-slide-2016-7-right" class="pin-frame bg-wrap-right"></div>
                
        
        <!-- Milestone - 2015 Slide 1 -->
        <div id="year-circle-2015-1" class="year-circle"><h3 class="dancing-script">2015</h3></div>
        <div id="text-2015-1" class="text-box">
            <h5>GAMUDA LAND ENTERS THE AUSTRALIAN MARKET WITH 661 CHAPEL STREET</h5>
            
        </div>
        <div id="line-2015-1" class="line-box"></div>
        <div id="milestone-slide-2015-1-left" class="pin-frame bg-wrap-left"></div>
        <div id="milestone-slide-2015-1-right" class="pin-frame bg-wrap-right"></div>
        
        <!-- Milestone - 2015 Slide 2 -->
        <div id="year-circle-2015-2" class="year-circle"><h3 class="dancing-script">2015</h3></div>
        <div id="text-2015-2" class="text-box">
            <h5>WE WIN THE TENDER FOR A 3-ACRE PLOT OF LAND IN SINGAPORE</h5>
            
        </div>
        <div id="line-2015-2" class="line-box"></div>
        <div id="milestone-slide-2015-2-left" class="pin-frame bg-wrap-left"></div>
        <div id="milestone-slide-2015-2-right" class="pin-frame bg-wrap-right"></div>
        
        <!-- Milestone - 2015 Slide 3 -->
        <div id="year-circle-2015-3" class="year-circle"><h3 class="dancing-script">2015</h3></div>
        <div id="text-2015-3" class="text-box">
            <h5>OUR FIRST HIGH-RISE WELLNESS DEVELOPMENT, HIGHPARK SUITES IN KELANA JAYA, IS LAUNCHED </h5>
            
        </div>
        <div id="milestone-slide-2015-3-left" class="pin-frame bg-wrap-left"></div>
        <div id="milestone-slide-2015-3-right" class="pin-frame bg-wrap-right"></div>
        
        <!-- Milestone - 2013 Slide 1 -->
        <div id="year-circle-2013-1" class="year-circle"><h3 class="dancing-script">2013</h3></div>
        <div id="text-2013-1" class="text-box">
            <h3>OUR LAND BANK REACHES THE<br>5,000-ACRE MARK</h3>
        </div>
        <div id="milestone-slide-2013-1-left" class="pin-frame bg-wrap-left"></div>
        <div id="milestone-slide-2013-1-right" class="pin-frame bg-wrap-right"></div>
        
        <!-- Milestone - 2013 Slide 2 -->
        <div id="year-circle-2013-2" class="year-circle"><h3 class="dancing-script">2013</h3></div>
        <div id="text-2013-2" class="text-box">
            <h5>JADE HILLS, GAMUDA LAND'S FIRST THEMATIC DEVELOPMENT IN KAJANG, IS INTRODUCED</h5>
            
        </div>
        <div id="line-2013-2" class="line-box"></div>
        <div id="milestone-slide-2013-2-left" class="pin-frame bg-wrap-left"></div>
        <div id="milestone-slide-2013-2-right" class="pin-frame bg-wrap-right"></div>
        
        <!-- Milestone - 2013 Slide 3 -->
        <div id="year-circle-2013-3" class="year-circle"><h3 class="dancing-script">2013</h3></div>
        <div id="text-2013-3" class="text-box">
            <h5>THE ROBERTSON BECOMES OUR PIONEEER HIGH RISE INTEGRATED DEVELOPMENT</h5>
            
        </div>
        <div id="line-2013-3" class="line-box"></div>
        <div id="milestone-slide-2013-3-left" class="pin-frame bg-wrap-left"></div>
        <div id="milestone-slide-2013-3-right" class="pin-frame bg-wrap-right"></div>
        
        <!-- Milestone - 2012 Slide 1 -->
        <div id="year-circle-2012-1" class="year-circle"><h3 class="dancing-script">2012</h3></div>
        <div id="text-2012-1" class="text-box">
            <h5>MADGE MANSIONS HAS THE DISTINCTION OF BEING OUR MAIDEN HIGH-RISE PREMIUM RESIDENTIAL DEVELOPMENT</h5>
            
        </div>
        <div id="line-2012-1" class="line-box"></div>
        <div id="milestone-slide-2012-1-left" class="pin-frame bg-wrap-left"></div>
        <div id="milestone-slide-2012-1-right" class="pin-frame bg-wrap-right"></div>
        
        <!-- Milestone - 2010 Slide 1 -->
        <div id="year-circle-2010-1" class="year-circle"><h3 class="dancing-script">2010</h3></div>
        <div id="text-2010-1" class="text-box">
            <h5>GAMUDA LAND STRENGTHENS FOOTPRINT IN VIETNAM WITH CELADON CITY DEVELOPMENT</h5>
            
        </div>
        <div id="line-2010-1" class="line-box"></div>
        <div id="milestone-slide-2010-1-left" class="pin-frame bg-wrap-left"></div>
        <div id="milestone-slide-2010-1-right" class="pin-frame bg-wrap-right"></div>
        
        <!-- Milestone - 2007 Slide 1 -->
        <div id="year-circle-2007-1" class="year-circle"><h3 class="dancing-script">2007</h3></div>
        <div id="text-2007-1" class="text-box">
            <h5>GAMUDA CITY IN VIETNAM BECOMES GAMUDA LAND'S FIRST INTERNATIONAL DEVELOPMENT</h5>
            
        </div>
        <div id="line-2007-1" class="line-box"></div>
        <div id="milestone-slide-2007-1-left" class="pin-frame bg-wrap-left"></div>
        <div id="milestone-slide-2007-1-right" class="pin-frame bg-wrap-right"></div>
        
        <!-- Milestone - 2006 Slide 1 -->
        <div id="year-circle-2006-1" class="year-circle"><h3 class="dancing-script">2006</h3></div>
        <div id="text-2006-1" class="text-box">
            <h5>GAMUDA LAND ENTERS ISKANDAR MALAYSIA WITH ITS FIRST PROPERTY DEVELOPMENT - HORIZON HILLS</h5>
            
        </div>
        <div id="line-2006-1" class="line-box"></div>
        <div id="milestone-slide-2006-1-left" class="pin-frame bg-wrap-left"></div>
        <div id="milestone-slide-2006-1-right" class="pin-frame bg-wrap-right"></div>
        
        <!-- Milestone - 2005 Slide 1 -->
        <div id="year-circle-2005-1" class="year-circle"><h3 class="dancing-script">2005</h3></div>
        <div id="text-2005-1" class="text-box">
            <h3>GAMUDA LAND HANDS OVER<br>10,000 HOUSES</h3>
        </div>
        <div id="milestone-slide-2005-1-left" class="pin-frame bg-wrap-left"></div>
        <div id="milestone-slide-2005-1-right" class="pin-frame bg-wrap-right"></div>
        
        <!-- Milestone - 1999 Slide 1 -->
        <div id="year-circle-1999-1" class="year-circle"><h3 class="dancing-script">1999</h3></div>
        <div id="text-1999-1" class="text-box">
            <h5>BANDAR BOTANIC BECOMES THE FIRST FIABCI AWARD-WINNING TOWNSHIP IN KLANG</h5>
            
        </div>
        <div id="line-1999-1" class="line-box"></div>
        <div id="milestone-slide-1999-1-left" class="pin-frame bg-wrap-left"></div>
        <div id="milestone-slide-1999-1-right" class="pin-frame bg-wrap-right"></div>
        
        <!-- Milestone - 1998 Slide 1 -->
        <div id="year-circle-1998-1" class="year-circle"><h3 class="dancing-script">1998</h3></div>
        <div id="text-1998-1" class="text-box">
            <h5>INTRODUCTION OF THE FIRST RESORT-INSPIRED DEVELOPMENT WITH A PRIVATE GOLF COURSE</h5>
            
        </div>
        <div id="milestone-slide-1998-1-left" class="pin-frame bg-wrap-left"></div>
        <div id="milestone-slide-1998-1-right" class="pin-frame bg-wrap-right"></div>
        
        <!-- Milestone - 1995 Slide 1 -->
        <div id="year-circle-1995-1" class="year-circle"><h3 class="dancing-script">1995</h3></div>
        <div id="text-1995-1" class="text-box">
            <h5>THE LAUNCH OF KOTA KEMUNING, THE FIRST INTEGRATED TOWNSHIP DEVELOPMENT</h5>
            
        </div>
        <div id="line-1995-1" class="line-box"></div>
        <div id="milestone-slide-1995-1-left" class="pin-frame bg-wrap-left"></div>
        <div id="milestone-slide-1995-1-right" class="pin-frame bg-wrap-right"></div>
        
        <!-- Milestone - 1995 Slide 2 -->
        <div id="year-circle-1995-2" class="year-circle"><h3 class="dancing-script">1995</h3></div>
        <div id="text-1995-2" class="text-box">
            <h5>GAMUDA LAND SDN BHD IS ESTABLISHED</h5>
            
        </div>
        <div id="milestone-slide-1995-2-left" class="pin-frame bg-wrap-left"></div>
        <div id="milestone-slide-1995-2-right" class="pin-frame bg-wrap-right"></div>
        
        <!-- About Us - Learn More -->
        <div id="year-circle-1995-2" class="year-circle"><h3 class="dancing-script">1995</h3></div>
        <div id="text-1995-2" class="text-box">
            <h5>FOUNDED IN 1995</h5>
            <p>Established in 1995, Gamuda Land is the property development arm of Gamuda Berhad, the monumental nation builder with a spectrum of experience and expertise. Right from the very beginning, Gamuda Land attained recognition as a far-sighted developer with the supportive backbone of its parent company in building not just homes, but a living experience beyond mere roofs. Synonymous with holistic lifestyle developments, Gamuda Land places incredible emphasis on superior infrastructure to ensure that each and every project embarked upon resonates the true values that Gamuda Land dearly embraces.</p>
        </div>
        <div id="slide-learn-more" class="pin-frame">
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<?php the_content(); ?>
<?php endwhile; endif; ?>
        </div>
        
    </div>
    
</div>

<?php get_footer();
// Omit closing PHP tag to avoid "Headers already sent" issues.