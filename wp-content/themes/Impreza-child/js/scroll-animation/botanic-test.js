jQuery(document).ready(function(){
    jQuery("#text-botanic-land-1").velocity({opacity: 1}, {delay: 0, duration: 200});		
    
	//jQuery("#demosMenu").change(function(){
	//  window.location.href = jQuery(this).find("option:selected").attr("id") + '.html';
	//});
	
	jQuery('#fullpage.botanic-scroll').fullpage({
		
        'verticalCentered': false,
        'css3': true,
        'navigation': true,
        'navigationPosition': 'right',
        'navigationTooltips': ['Home', 'Home', 'Overview', 'Features', 'Facilities', 'Master Site Plan', 'Master Site Plan', 'Master Site Plan', 'Gallery', 'Happenings', 'More Projects'],

        'afterLoad': function(anchorLink, index){
            if(index == 2){
                jQuery('#text-botanic-land-2').addClass('active');				
				jQuery('#botanic-land-slide-2-left').velocity({opacity: 1}, {delay: 0, duration: 200});		
				jQuery('#botanic-land-slide-2-right').velocity({opacity: 1}, {delay: 0, duration: 200});
            }
            else if(index == 3){
                jQuery('#text-2016-5, #line-2016-5').addClass('active');
            }
        },

        'onLeave': function(index, nextIndex, direction){
            if (index == 3 && direction == 'down'){                
                jQuery('.section').eq(index -1).removeClass('moveDown').addClass('moveUp');
//                    jQuery('.section').moveTo('footer', 2);
            }
            else if(index == 3 && direction == 'up'){
                jQuery('.section').eq(index -1).removeClass('moveUp').addClass('moveDown');
            }
        }
    });
});