
jQuery(document).ready(function(){
	jQuery(".milestones-swipe-nav ul").velocity({marginLeft:'-80%'}, {duration:400, delay:300});
	jQuery(".milestones-swipe-nav ul").velocity({marginLeft: 0}, {duration:400, delay:300});
	jQuery('.milestones-swipe-nav a').click(function(){
		jQuery('.milestones-swipe-nav li').removeClass('active');
		jQuery(this).parent('li').toggleClass('active');
		jQuery('html, body').stop().animate({
			scrollTop: jQuery( jQuery(this).attr('href') ).offset().top - 100
		}, 800);
		return false;
	});
})
	
jQuery(document).ready(function(){	
   var tabList = jQuery('.mobile-tab-group ul li');
   jQuery(tabList).click(function() {
	  for(var i = 1; i <= 3; i++) {
		  if(jQuery(this).data("num") == i) {
			  tabList.removeClass('active');
			  jQuery(this).addClass('active');
			  jQuery('.mobile-tab').velocity({opacity: 0}, {display: 'none', delay: 0, duration: 0});
			jQuery('.mobile-tab' + i).velocity({opacity: 1}, {display: 'block', delay: 0, duration: 0});
		  }
		  if(jQuery(this).data("num") == 1) {
			  jQuery('.mobile-tab-group').css('backgroundColor', '#158179');
			  tabList.css('color', 'white');
			  jQuery(this).css('color', '#158179');
		  }
		  if(jQuery(this).data("num") == 2) {
			  jQuery('.mobile-tab-group').css('backgroundColor', '#29bbc4');
			  tabList.css('color', 'white');
			  jQuery(this).css('color', '#29bbc4');
		  }
		  if(jQuery(this).data("num") == 3) {
			  jQuery('.mobile-tab-group').css('backgroundColor', '#12ad95');
			  tabList.css('color', 'white');
			  jQuery(this).css('color', '#12ad95');
		  }
	  }
   })
})
jQuery(document).ready(function() {
   jQuery('.philosophy-tab-title a').click(function() {
       jQuery('.philosophy-tab-title a').css('background-color', '#eb312e');
       jQuery('.philosophy-tab-content').hide();
       jQuery(this).parent().siblings('.philosophy-tab-content').show();
       if(jQuery(this).parent().hasClass('philosophy-tab-title1')) {
           jQuery('.mobile-tab-cover').css('background-image', 'url("/wp-content/themes/Impreza-child/img/philosophy-tab1-bg.jpg")');
           jQuery(this).css('background-color', '#0072bc');
       } else if (jQuery(this).parent().hasClass('philosophy-tab-title2')) {
           jQuery('.mobile-tab-cover').css('background-image', 'url("/wp-content/themes/Impreza-child/img/philosophy-tab2-bg.jpg")');
           jQuery(this).css('background-color', '#598527');
       } else if (jQuery(this).parent().hasClass('philosophy-tab-title3')) {
           jQuery('.mobile-tab-cover').css('background-image', 'url("/wp-content/themes/Impreza-child/img/philosophy-tab3-bg.jpg")');
           jQuery(this).css('background-color', '#00746b');
       } else {
           jQuery(this).css('background-color', '#eb312e');
       }
       if(jQuery(this).parent().hasClass('philosophy-single-tab-title')) {
           jQuery('.mobile-tab-cover').css('background-image', 'url("/wp-content/themes/Impreza-child/img/philosophy-tab-single-bg.jpg")');
           jQuery('.philosophy-tab-title1, .philosophy-tab-title2, .philosophy-tab-title3').find('a').css('background-color', '#555');
       }
       jQuery('html, body').stop().animate({
           scrollTop: jQuery( jQuery(this).attr('href') ).offset().top - 90
       }, 800);
       return false;
   });
});
