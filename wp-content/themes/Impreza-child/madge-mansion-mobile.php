<?php
// Template Name:  Madge Mansions Mobile Template 
get_header(); ?>

<div class="mobile-main-nav mobile-main-nav-all">
    <div class="mobile-main-nav-header">
        <a class="mobile-main-nav-logo" href="http://128.199.89.40/"><img src="http://128.199.89.40/wp-content/uploads/2016/07/logo-1.png" alt="Gamuda Land Logo"></a>
		<div class="mobile-main-nav-close"><img src="/wp-content/uploads/2016/09/nav-close-btn.png"></div>
    </div>
    <ul class="list-unstyled">
        <li><a href="http://128.199.89.40/about-us/">ABOUT US</a></li>
        <li class="mobile-sub-nav-open">
            <a href="http://128.199.89.40/our-developments/">OUR DEVELOPMENTS</a>
            <i class="fa fa-angle-right" aria-hidden="true"></i>
        </li>
        <li><a href="http://128.199.89.40/happenings/">HAPPENINGS</a></li>
        <li><a href="http://128.199.89.40/contact-us/">CONTACT US</a></li>
    </ul>
    <a class="mobile-main-nav-register" href="https://webreg.gamudaland.com.my/contact/_publicwebregistration/" target="_blank">REGISTER</a>
    <div class="mobile-main-nav-sns">
        <a href="https://www.facebook.com/GamudaLand/?fref=ts&ref=br_tf"><i class="fa fa-facebook" aria-hidden="true"></i></a>
        <a href="https://www.instagram.com/gamudaland/"><i class="fa fa-instagram" aria-hidden="true"></i></a>
    </div>
</div>

<div class="mobile-main-nav mobile-main-nav-projects">

    <div class="mobile-main-nav-header">
        <a class="mobile-main-nav-logo" href="http://128.199.89.40/"><img src="http://128.199.89.40/wp-content/uploads/2016/07/logo-1.png" alt="Gamuda Land Logo"></a>
		<div class="mobile-main-nav-close"><img src="/wp-content/uploads/2016/09/nav-close-btn.png"></div>
    </div>
    <div class="mobile-main-nav-projects-list">
        <ul class="list-unstyled">
            <li class="mobile-sub-nav-close">
                <b>FEATURED</b> DEVELOPMENTS
                <i class="fa fa-angle-left" aria-hidden="true"></i>
            </li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/highpark-suites/">HIGHPARK SUITES<span>Kelana Jaya</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/the-robertson/">THE ROBERTSON<span>Kuala Lumpur</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/jade-hills/">JADE HILLS<span>Kajang</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/horizon-hills/">HORIZON HILLS<span>Iskandar Puteri</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/bukit-bantayan/">BUKIT BANTAYAN<span>Kota Kinabalu</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/gamuda-walk/">GAMUDA WALK<span>Shah Alam</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/gem-residences/">GEM RESIDENCES<span>Toa Payoh</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/661-chapel-st/">661 CHAPEL ST.<span>Melbourne</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/gamuda-city/">GAMUDA CITY<span>Hanoi</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/celadon-city/">CELADON CITY<span>Ho Chi Minh</span></a></li>
        </ul>
        <ul class="list-unstyled">
            <li><b>UPCOMING</b> DEVELOPMENTS</li>
            <li><a href="http://gamudaland.com.my/kundangestates/">KUNDANG ESTATES<span>Kuang</span></a></li>
            <li><a href="#" class="navi-disable">GAMUDA GARDENS<span>Kuang</span></a></li>
            <li><a href="http://gamudaland.com.my/twentyfive7/">TWENTYFIVE7<span>Kota Kemuning</span></a></li>
        </ul>
        <ul class="list-unstyled">
            <li><b>FULLY SOLD</b> DEVELOPMENTS</li>
            <li><a href="http://128.199.89.40/our-developments/fully-sold-developments/kota-kemuning/">KOTA KEMUNING<span>Shah Alam</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/fully-sold-developments/bandar-botanic/">BANDAR BOTANIC<span>Klang</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/fully-sold-developments/valencia/">VALENCIA<span>Sungai Buloh</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/fully-sold-developments/madge-mansions/">MADGE MANSIONS<span>Kuala Lumpur</span></a></li>
        </ul>
        <ul class="list-unstyled">
            <li><b>GOLF &amp; COUNTRY</b> CLUBS</li>
            <li><a href="http://www.kotapermai.com.my/" target="_blank">KOTA PERMAI<span>Golf &amp; Country Club</span></a></li>
            <li><a href="http://hhgcc.com.my/" target="_blank">HORIZON HILLS<span>Golf &amp; Country Club</span></a></li>
            <li><a href="http://www.botanicresortclub.com.my/" target="_blank">BOTANIC<span>Resort Club</span></a></li>
        </ul>
    </div>
</div>

<div id="mobile-pages" class="mobile-slide-page madge-mansion mobile-page">
<div class="container-fluid document">
    <div class="row mobile-cover mobile-madge-mansion-main-cover">
        <div class="mobile-cover-content">
            <img class="mobile-main-logo" src="/wp-content/themes/Impreza-child/img/madge-mansion-logo.png" alt="Madge Mansion Logo">
            <h2 class="md-font">THE EMBODIMENT<br>OF REFINDED LIVING<br>IN A SECURE SETTING</h2>
            <p class="xs-font">a heartbeat away from the city</p>
        </div>
		<div class="mouse">
			<div class="mouse-icon">
				<span class="mouse-wheel"></span>
			</div>
		</div>
        <div class="mobile-cover-bg">
            <!--<img src="/wp-content/themes/Impreza-child/img/madge-mansion-main-cover.jpg" alt="Mobile Cover Image">-->
        </div>
    </div>

    <div class="row mobile-madge-mansion-overview">
        <h2 class="md-font">OVERVIEW</h2>
        <p class="sm-font">The luxurious Madge Mansions offers refined living spaces together with uncompromising privacy and security along Kuala Lumpur’s Embassy Row. Designed to mirror the living experience of a landed property, Madge Mansions comprises 52 signature residences each with its own exclusive private lift lobby. This development’s comprehensive three-tier security system is second to none. From the point of entry, stringent measures are in place with centralised security monitoring all movements in and out of the property.</p>
        <p class="sm-font">This sanctuary from the bustle of the city offers residents an exclusive and exquisite lifestyle along with absolute peace of mind. The property’s signature residences, which are among the most spacious condominiums in the city, integrate generous living spaces with lofty ceilings and full height windows. Together with a private jacuzzi dip pool or lanai for each residence, plus state-of-the art amenities, dedicated concierge services, as well as lush landscaped gardens, Madge Mansions is the epitome of refined living amidst a tranquil and secure setting.</p>
        <div class="mobile-madge-mansion-list">
            <h3 class="mobile-list-title">THE PRESTIGE OF AN AFFLUENT ADDRESS</h3>
            <ul class="sm-font">
                <li>Located within the prestigious embassy row, Jalan Madge</li>
                <li>52 signature suites on 2.16 acres of Freehold Land</li>
                <li>Built-ups from 3,500sf to 8,400sf</li>
            </ul>
        </div>
        <div class="mobile-madge-mansion-list">
            <h3 class="mobile-list-title">THE FINE BALANCE IN LIFE</h3>
            <ul class="sm-font">
                <li>Designed to mirror the living experience of a landed property</li>
                <li>Generous design layout with lofty ceilings and full height glazed windows</li>
                <li>Choice of private Jacuzzi dip pool or private lanai</li>
                <li>Private lift lobby that is exclusive to each unit</li>
            </ul>
        </div>
    </div>

    <div class="row single-image">
        <img src="/wp-content/themes/Impreza-child/img/madge-mansion-banner1.jpg" alt="I believe green is more than a colour">
    </div>

    <div class="row mobile-madge-mansion-security">
        <h2 class="md-font">SECURITY</h2>
        <div class="mobile-madge-mansion-list">
            <ul class="sm-font">
                <li>Outdoor CCTV</li>
                <li>Dome camera for internal surveillance</li>
                <li>Guardhouse to manage and monitor all incoming and outgoing traffic</li>
                <li>Residents’ card access to lobby and lift</li>
                <li>Video intercom system to guard control centre</li>
            </ul>
        </div>
        <img class="mobile-madge-mansion-security-img1" src="/wp-content/themes/Impreza-child/img/madge-mansion-security-img1.jpg" alt="Madge Mansion Security Image">
        <img class="mobile-madge-mansion-security-img2" src="/wp-content/themes/Impreza-child/img/madge-mansion-security-img2.jpg" alt="Madge Mansion Security Image">
    </div>

    <div class="row single-image">
        <img src="/wp-content/themes/Impreza-child/img/madge-mansion-banner2.jpg" alt="I'm Secure">
    </div>

    <div class="row mobile-madge-mansion-security">
        <img src="/wp-content/themes/Impreza-child/img/madge-mansion-security-img3.jpg" alt="Madge Mansion Security Image">
        <table class="xs-font">
            <tr>
                <td><img src="/wp-content/themes/Impreza-child/img/madge-mansion-security-table-img1.jpg" alt="Madge Mansion Security Image"></td>
                <td>a comprehensive 3-tier security system frees me to experience life to the fullest</td>
            </tr>
            <tr>
                <td><img src="/wp-content/themes/Impreza-child/img/madge-mansion-security-table-img2.jpg" alt="Madge Mansion Security Image"></td>
                <td>the watchfull eyes of numerous CCTVs along the outer perimeter see to my security day and night</td>
            </tr>
            <tr>
                <td><img src="/wp-content/themes/Impreza-child/img/madge-mansion-security-table-img3.jpg" alt="Madge Mansion Security Image"></td>
                <td>Closer home, Residents' card access points and 24-hour guard patrols prevented unwanted</td>
            </tr>
        </table>
    </div>

    <div class="row mobile-madge-mansion-landscapes">
        <h2 class="md-font">LANDSCAPES</h2>
        <img src="/wp-content/themes/Impreza-child/img/madge-mansion-landscapes-img1.jpg" alt="Madge Mansion Landscapes Image">
        <img src="/wp-content/themes/Impreza-child/img/madge-mansion-landscapes-img2.jpg" alt="Madge Mansion Landscapes Image">
        <div class="mobile-madge-mansion-list">
            <ul class="sm-font">
                <li>Arrival court lined with lush plantings and landscapes</li>
                <li>Bamboo hedges, vertical green curtains and intermediary courtyards enhance privacy</li>
                <li>Vertical plantings throughout provide natural cooling and cross ventilation</li>
                <li>Sensitive selection of plants and shrubs for a softer ambience</li>
				<li>Accented plantings at sun decks provide shade</li>
				<li>Solitary trees at cabana level for enhance privacy</li>
            </ul>
        </div>
        <img src="/wp-content/themes/Impreza-child/img/madge-mansion-landscapes-img3.jpg" alt="Madge Mansion Landscapes Image">
        <div class="row mobile-madge-mansion-landscapes-grid">
            <div class="col-xs-6">
                <img src="/wp-content/themes/Impreza-child/img/madge-mansion-landscapes-grid-img1.jpg" alt="Madge Mansion Landscapes Image">
                <img src="/wp-content/themes/Impreza-child/img/madge-mansion-landscapes-grid-img2.jpg" alt="Madge Mansion Landscapes Image">
                <img src="/wp-content/themes/Impreza-child/img/madge-mansion-landscapes-grid-img3.jpg" alt="Madge Mansion Landscapes Image">
            </div>
            <div class="col-xs-6">
                <p class="xs-font">Statuesque trees line up along the boundry, while bamboo hedges, vertical green curtains and sweeping courtyards cocoon my privacy my life</p>
                <img src="/wp-content/themes/Impreza-child/img/madge-mansion-landscapes-grid-img4.jpg" alt="Madge Mansion Landscapes Image">
                <p class="xs-font">A gentle breeze glidesthrough my home as I enjoy its open spaces, the air infused with the scent of warm timber and jasmine.</p>
            </div>
        </div>
        <img src="/wp-content/themes/Impreza-child/img/madge-mansion-landscapes-img4.jpg" alt="Madge Mansion Landscapes Image">
        <img src="/wp-content/themes/Impreza-child/img/madge-mansion-landscapes-img5.jpg" alt="Madge Mansion Landscapes Image">
    </div>

    <div class="row single-image">
        <img src="/wp-content/themes/Impreza-child/img/madge-mansion-banner3.jpg" alt="I seek space and enlightment">
    </div>

    <div class="row mobile-madge-mansion-architect">
        <h2 class="md-font">ARCHITECHTURE</h2>
        <img src="/wp-content/themes/Impreza-child/img/madge-mansion-architect-img1.jpg" alt="Madge Mansion Architecture Image">
        <div class="mobile-madge-mansion-list">
            <ul class="sm-font">
                <li>Private dip pool or spacious lanai for entertaining and personal rejuvenation</li>
                <li>Luxurious master bath layout with cantilevered bathtub and twin share shower suite</li>
                <li>High ceilings on all floors</li>
            </ul>
        </div>
        <img src="/wp-content/themes/Impreza-child/img/madge-mansion-architect-img2.jpg" alt="Madge Mansion Architecture Image">
        <div class="row mobile-madge-mansion-architect-grid">
            <div class="col-xs-8">
                <img src="/wp-content/themes/Impreza-child/img/madge-mansion-architect-grid-img1.jpg" alt="Madge Mansion Architecture Image">
            </div>
            <div class="col-xs-4">
                <img src="/wp-content/themes/Impreza-child/img/madge-mansion-architect-grid-img2.jpg" alt="Madge Mansion Architecture Image">
                <img src="/wp-content/themes/Impreza-child/img/madge-mansion-architect-grid-img3.jpg" alt="Madge Mansion Architecture Image">
                <img src="/wp-content/themes/Impreza-child/img/madge-mansion-architect-grid-img4.jpg" alt="Madge Mansion Architecture Image">
            </div>
        </div>
        <p class="sm-font">At home, I soak in the pleasures of my own private dip pool and sink into a moment of blissful ignorance.</p>
        <img src="/wp-content/themes/Impreza-child/img/madge-mansion-architect-img3.jpg" alt="Madge Mansion Architecture Image">
    </div>

    <div class="row single-image">
        <img src="/wp-content/themes/Impreza-child/img/madge-mansion-banner4.jpg" alt="I contemplate my personal style in the vast expanses of space, accented with a unique play of matt and reflective surfaces.">
    </div>

    <div class="row mobile-madge-mansion-architect">
        <img src="/wp-content/themes/Impreza-child/img/madge-mansion-architect-img4.jpg" alt="Madge Mansion Architecture Image">
        <table class="xs-font mobile-madge-mansion-architect-grid2">
            <tr>
                <td><img src="/wp-content/themes/Impreza-child/img/madge-mansion-architect-grid-img5.jpg" alt="Madge Mansion Architecture Image"></td>
                <td><img src="/wp-content/themes/Impreza-child/img/madge-mansion-architect-grid-img8.jpg" alt="Madge Mansion Architecture Image"></td>
            </tr>
            <tr>
                <td><img src="/wp-content/themes/Impreza-child/img/madge-mansion-architect-grid-img6.jpg" alt="Madge Mansion Architecture Image"></td>
                <td>Tall glazed windows greet the light and turn away the tropical heat, inviting clarity and comfort to my personal abode.</td>
            </tr>
            <tr>
                <td><img src="/wp-content/themes/Impreza-child/img/madge-mansion-architect-grid-img7.jpg" alt="Madge Mansion Architecture Image"></td>
                <td>Architectural fins spread their wings, teasing the light into a dance of shadows.</td>
            </tr>
        </table>
    </div>

    <div class="row single-image">
        <img src="/wp-content/themes/Impreza-child/img/madge-mansion-banner5.jpg" alt="As I enter, a symphony of watery reflections resonates a sense of calm and serenity in my mind.">
    </div>

    <div class="row mobile-madge-mansion-features">
        <h2 class="md-font">DESIGN FEATURES</h2>
        <div class="mobile-madge-mansion-list">
            <ul class="sm-font">
                <li>Black basalt entrance driveway to set the tone of luxury and opulence</li>
                <li>Covered porte-cochere</li>
                <li>Elegant pergolas with glass panels at entrance provide stylish relief from the tropical heat</li>
                <li>Private lift lobby</li>
                <li>Refreshing private lanai and dip pool in selected units</li>
                <li>Spacious wet and dry kitchens</li>
                <li>3 car park bays standard, 6 car park bays for Penthouse units</li>
				<li>Designated private lockers at basement</li>
				<li>Chauffeur’s lounge</li>
            </ul>
        </div>
        <img src="/wp-content/themes/Impreza-child/img/madge-mansion-features-img1.jpg" alt="Madge Mansion Design Features Image">
        <img src="/wp-content/themes/Impreza-child/img/madge-mansion-features-img2.jpg" alt="Madge Mansion Design Features Image">
        <div class="row mobile-madge-mansion-features-grid">
            <div class="col-xs-4"><img src="/wp-content/themes/Impreza-child/img/madge-mansion-features-img3.jpg" alt="Madge Mansion Design Features Image"></div>
            <div class="col-xs-4"><img src="/wp-content/themes/Impreza-child/img/madge-mansion-features-img4.jpg" alt="Madge Mansion Design Features Image"></div>
            <div class="col-xs-4"><img src="/wp-content/themes/Impreza-child/img/madge-mansion-features-img5.jpg" alt="Madge Mansion Design Features Image"></div>
        </div>
        <img src="/wp-content/themes/Impreza-child/img/madge-mansion-features-img6.jpg" alt="Madge Mansion Design Features Image">
    </div>

    <div class="row single-image">
        <img src="/wp-content/themes/Impreza-child/img/madge-mansion-banner6.jpg" alt="I experience things beyond words.">
    </div>
	<div class="row">
		<div class="mobile-other-projects">
			<h2 class="md-font">OTHER DEVELOPMENTS</h2>
			<div class="col-xs-6 mobile-prev-project">
				<a href="http://128.199.89.40/our-developments/fully-sold-developments/valencia/"><img src="/wp-content/uploads/2016/09/m-other-dev-banner-valencia.jpg" alt="Valencia"></a>
			</div>
			<div class="col-xs-6 mobile-next-project">
				<a href="http://128.199.89.40/our-developments/fully-sold-developments/kota-kemuning/"><img src="/wp-content/uploads/2016/09/m-other-dev-banner-kota-kemuning.jpg" alt="Kota Kemuning"></a>
			</div>
		</div>
	</div>
</div>

</div>
<?php get_footer();