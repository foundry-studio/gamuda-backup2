<?php

function add_fullpage_ori_style() {
    wp_enqueue_style( 'jquery.fullpage.css', get_stylesheet_directory_uri() . '/css/fullpage/jquery.fullpage.css' );
}
add_action( 'wp_enqueue_scripts', 'add_fullpage_ori_style' );

//Bootstrap CSS
function add_bootstrap() {
    wp_enqueue_style( 'bootstrap.min.css', get_stylesheet_directory_uri() . '/css/bootstrap.min.css' );
}
add_action( 'wp_enqueue_scripts', 'add_bootstrap' );

//Child General CSS
/*
function add_style() {
    wp_enqueue_style( 'style.css', get_stylesheet_directory_uri() . '/style.css' );
}
add_action( 'wp_enqueue_scripts', 'add_style' );*/

//Child Theme About Us CSS
function add_about_us_style() {
    wp_enqueue_style( 'about-us-style.css', get_stylesheet_directory_uri() . '/css/about-us-style.css' );
}
add_action( 'wp_enqueue_scripts', 'add_about_us_style' );

//Child Theme Our Projects CSS
function add_our_projects_style() {
    wp_enqueue_style( 'our-projects-style.css', get_stylesheet_directory_uri() . '/css/our-projects-style.css' );
}
add_action( 'wp_enqueue_scripts', 'add_our_projects_style' );

//Child Theme Our Projects Inner CSS
function add_our_projects_inner_style() {
    wp_enqueue_style( 'our-projects-inner-style.css', get_stylesheet_directory_uri() . '/css/our-projects-inner-style.css' );
}
add_action( 'wp_enqueue_scripts', 'add_our_projects_inner_style' );

//Child Theme Our Projects Inner Mobile CSS
function add_our_projects_inner_mobile_style() {
    wp_enqueue_style( 'our-projects-inner-mobile-style.css', get_stylesheet_directory_uri() . '/css/our-projects-inner-mobile-style.css' );
}
add_action( 'wp_enqueue_scripts', 'add_our_projects_inner_mobile_style' );

//Child Theme Collection Card Page CSS
function add_collection_card_page_style() {
    wp_enqueue_style( 'collection-card-page-style.css', get_stylesheet_directory_uri() . '/css/collection-card-page-style.css' );
}
add_action( 'wp_enqueue_scripts', 'add_collection_card_page_style' );

//Child Theme Happenings inner CSS
function add_happenings_article_pages_style() {
   wp_enqueue_style( 'happenings-article-pages-style.css', get_stylesheet_directory_uri() . '/css/happenings-article-pages-style.css' );
}
add_action( 'wp_enqueue_scripts', 'add_happenings_article_pages_style' );

//Child Theme Happenings CSS
function add_happenings_inner_pages_style() {
    wp_enqueue_style( 'happenings-inner-pages-style.css', get_stylesheet_directory_uri() . '/css/happenings-inner-pages-style.css' );
}
add_action( 'wp_enqueue_scripts', 'add_happenings_inner_pages_style' );
//Child Theme Happenings CSS
function add_happenings_style() {
    wp_enqueue_style( 'happenings-style.css', get_stylesheet_directory_uri() . '/css/happenings-style.css' );
}
add_action( 'wp_enqueue_scripts', 'add_happenings_style' );

//Child Theme Contact Us CSS
function add_contact_us_style() {
    wp_enqueue_style( 'contact-us-style.css', get_stylesheet_directory_uri() . '/css/contact-us-style.css' );
}
add_action( 'wp_enqueue_scripts', 'add_contact_us_style' );

//Child Theme Superscrollorama CSS
function add_about_milestones_style() {
    wp_enqueue_style( 'about-milestones.css', get_stylesheet_directory_uri() . '/css/scroll-animation/about-milestones.css' );
}
add_action( 'wp_enqueue_scripts', 'add_about_milestones_style' );

function add_milestones_style() {
    wp_enqueue_style( 'milestones.css', get_stylesheet_directory_uri() . '/css/scroll-animation/milestones.css' );
}
add_action( 'wp_enqueue_scripts', 'add_milestones_style' );

//Child Theme Superscrollorama Innovation CSS
function add_about_innovation_style() {
    wp_enqueue_style( 'about-innovation.css', get_stylesheet_directory_uri() . '/css/scroll-animation/about-innovation.css' );
}
add_action( 'wp_enqueue_scripts', 'add_about_innovation_style' );

function add_about_philosophy_style() {
    wp_enqueue_style( 'about-philosophy.css', get_stylesheet_directory_uri() . '/css/scroll-animation/about-philosophy.css' );
}
add_action( 'wp_enqueue_scripts', 'add_about_philosophy_style' );

function add_bandar_botanic_style() {
    wp_enqueue_style( 'bandar-botanic.css', get_stylesheet_directory_uri() . '/css/scroll-animation/bandar-botanic.css' );
}
add_action( 'wp_enqueue_scripts', 'add_bandar_botanic_style' );

function add_bandar_botanic_try_style() {
    wp_enqueue_style( 'bandar-botanic-try.css', get_stylesheet_directory_uri() . '/css/scroll-animation/bandar-botanic-try.css' );
}
add_action( 'wp_enqueue_scripts', 'add_bandar_botanic_try_style' );

function add_kota_kemuning_style() {
    wp_enqueue_style( 'kota-kemuning.css', get_stylesheet_directory_uri() . '/css/scroll-animation/kota-kemuning.css' );
}
add_action( 'wp_enqueue_scripts', 'add_kota_kemuning_style' );

function add_mobile_style() {
    wp_enqueue_style( 'mobile-style.css', get_stylesheet_directory_uri() . '/css/mobile-style.css' );
}
add_action( 'wp_enqueue_scripts', 'add_mobile_style' );

function add_mobile2_style() {
    wp_enqueue_style( 'mobile-style2.css', get_stylesheet_directory_uri() . '/css/mobile-style2.css' );
}
add_action( 'wp_enqueue_scripts', 'add_mobile2_style' );

function add_buyer_guide_style() {
    wp_enqueue_style( 'buyer-guide-style.css', get_stylesheet_directory_uri() . '/css/buyer-guide-style.css' );
}
add_action( 'wp_enqueue_scripts', 'add_buyer_guide_style' );

function add_botanic_test_style() {
    wp_enqueue_style( 'botanic-test.css', get_stylesheet_directory_uri() . '/css/scroll-animation/botanic-test.css' );
}
add_action( 'wp_enqueue_scripts', 'add_botanic_test_style' );


/*

// General Javascript

function add_my_script() {
    wp_register_script('my_script', home_url() . '/wp-content/themes/Impreza-child/js/my_script.js', array('jquery'));
    wp_enqueue_script('my_script');
}
add_action('wp_enqueue_scripts', 'add_my_script');

function add_w3data_script() {
    wp_register_script('w3data_script', home_url() . '/wp-content/themes/Impreza-child/js/w3data_script.js', array('jquery'));
    wp_enqueue_script('w3data_script');
}
add_action('wp_enqueue_scripts', 'add_w3data_script');



// Add bootstrap script
function add_bootstrap_script() {
    wp_register_script('bootstrap', home_url() . '/wp-content/themes/Impreza-child/js/bootstrap.min.js', array('jquery'));
    wp_enqueue_script('bootstrap');
}
add_action('wp_enqueue_scripts', 'add_bootstrap_script');

function add_range_slider_script() {
    wp_register_script('bootstrap-slider', home_url() . '/wp-content/themes/Impreza-child/js/bootstrap-slider.js', array('jquery'));
    wp_enqueue_script('bootstrap-slider');
}
add_action('wp_enqueue_scripts', 'add_range_slider_script');



function add_velocity_min_script() {
    wp_register_script('velocity_min_script', home_url() . '/wp-content/themes/Impreza-child/js/velocity.min.js', array('jquery'));
    wp_enqueue_script('velocity_min_script');
}
add_action('wp_enqueue_scripts', 'add_velocity_min_script');

function add_velocity_ui_script() {
    wp_register_script('velocity_ui_script', home_url() . '/wp-content/themes/Impreza-child/js/velocity.ui.js', array('jquery'));
    wp_enqueue_script('velocity_ui_script');
}
add_action('wp_enqueue_scripts', 'add_velocity_ui_script');


//Scroll Animation Library
function add_tween_max() {
    wp_register_script('tween_max', home_url() . '/wp-content/themes/Impreza-child/js/lib/greensock/TweenMax.min.js', array('jquery'));
    wp_enqueue_script('tween_max');
}
add_action('wp_enqueue_scripts', 'add_tween_max');

function add_scrollmagic() {
    wp_register_script('scrollmagic', home_url() . '/wp-content/themes/Impreza-child/js/scrollmagic/ScrollMagic.js', array('jquery'));
    wp_enqueue_script('scrollmagic');
}
add_action('wp_enqueue_scripts', 'add_scrollmagic');

function add_anim_gsap() {
    wp_register_script('animation-gsap', home_url() . '/wp-content/themes/Impreza-child/js/scrollmagic/plugins/animation.gsap.js', array('jquery'));
    wp_enqueue_script('animation-gsap');
}
add_action('wp_enqueue_scripts', 'add_anim_gsap');

function add_debug_addindicators() {
    wp_register_script('debug-addindicators', home_url() . '/wp-content/themes/Impreza-child/js/scrollmagic/plugins/debug.addIndicators.js', array('jquery'));
    wp_enqueue_script('debug-addindicators');
}
add_action('wp_enqueue_scripts', 'add_debug_addindicators');

function add_superscrollorama() {
    wp_register_script('superscrollorama', home_url() . '/wp-content/themes/Impreza-child/js/jquery.superscrollorama.js', array('jquery'));
    wp_enqueue_script('superscrollorama');
}
add_action('wp_enqueue_scripts', 'add_superscrollorama');

function add_fullpage() {
    wp_register_script('fullpage', home_url() . '/wp-content/themes/Impreza-child/js/fullpage/jquery.fullpage.js', array('jquery'));
    wp_enqueue_script('fullpage');
}
add_action('wp_enqueue_scripts', 'add_fullpage');

function add_scrolloverflow() {
    wp_register_script('scrolloverflow', home_url() . '/wp-content/themes/Impreza-child/vendors/scrolloverflow.js', array('jquery'));
    wp_enqueue_script('scrolloverflow');
}
add_action('wp_enqueue_scripts', 'add_scrolloverflow');



//Scroll Animation Javascript

//function add_milestones_script() {
//    wp_register_script('milestones', home_url() . '/wp-content/themes/Impreza-child/js/scroll-animation/milestones.js', array('jquery'));
//    wp_enqueue_script('milestones');
//}
//add_action('wp_enqueue_scripts', 'add_milestones_script');

//function add_about_milestones_script() {
//    wp_register_script('about-milestones', home_url() . '/wp-content/themes/Impreza-child/js/scroll-animation/about-milestones.js', array('jquery'));
//    wp_enqueue_script('about-milestones');
//}
//add_action('wp_enqueue_scripts', 'add_about_milestones_script');

function add_about_innovation_script() {
    wp_register_script('about-innovation', home_url() . '/wp-content/themes/Impreza-child/js/scroll-animation/about-innovation.js', array('jquery'));
    wp_enqueue_script('about-innovation');
}
add_action('wp_enqueue_scripts', 'add_about_innovation_script');

function add_bandar_botanic_script() {
    wp_register_script('bandar-botanic', home_url() . '/wp-content/themes/Impreza-child/js/scroll-animation/bandar-botanic.js', array('jquery'));
    wp_enqueue_script('bandar-botanic');
}
add_action('wp_enqueue_scripts', 'add_bandar_botanic_script');

function add_kota_kemuning_script() {
    wp_register_script('kota-kemuning', home_url() . '/wp-content/themes/Impreza-child/js/scroll-animation/kota-kemuning.js', array('jquery'));
    wp_enqueue_script('kota-kemuning');
}
add_action('wp_enqueue_scripts', 'add_kota_kemuning_script');

function add_about_philosophy_script() {
    wp_register_script('about-philosophy', home_url() . '/wp-content/themes/Impreza-child/js/scroll-animation/about-philosophy.js', array('jquery'));
    wp_enqueue_script('about-philosophy');
}
add_action('wp_enqueue_scripts', 'add_about_philosophy_script');

*/

//function add_botanic_test_script() {
//   wp_register_script('botanic-test', home_url() . '/wp-content/themes/Impreza-child/js/scroll-animation/botanic-test.js', array('jquery'));
//    wp_enqueue_script('botanic-test');
//}
//add_action('wp_enqueue_scripts', 'add_botanic_test_script');




