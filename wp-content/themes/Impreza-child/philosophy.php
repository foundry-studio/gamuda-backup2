<?php
// Template Name:  Philosophy Template
get_header(); ?>


<div class="page-breadcrumbs slider-breadcrumbs">
	<p><a href="http://192.168.99.100:32773/">Home</a> <span>/</span> <a href="http://192.168.99.100:32773/about-us/">About Us</a> <span>/</span> Philosophy</p>
</div>
<div class="custom-container philosophy-desktop" style="width:100%;">
	<div class="container-fluid philosophy-background-one"></div>

	<div class="container-fluid philosophy-background-two">
		<div class="col-md-12">
			<div class="philosophy-left col-md-6 col-lg-6">

			</div>

			<div class="straight-white-line-two"></div>

			<div class="philosophy-right col-md-6 col-lg-6 timeline-init">
				<div class="philosophy-first-message">
					<p>Our commitment to delivering<br>
					sustainable value to communities<br>
					is underpinned by these three key<br>
					principles that guide the creation<br>
					of all our developments.</p>
				</div>

				<div class="row philo">
					<div class="col-md-2 philosophy-a linker">I</div>
					<div class="col-md-10 philosophy-a-title">STRONG MASTER<br>PLANNING</div>
				</div>

				<div class="row philo-2">
					<div class="col-md-2 philosophy-a">II</div>
					<div class="col-md-10 philosophy-a-title">BEAUTIFULLY CRAFTED<br>ENVIRONMENT</div>
				</div>

				<div class="row philo-3">
					<div class="col-md-2 philosophy-a">III</div>
					<div class="col-md-10 philosophy-a-title big-one-line-correction">GOOD LOCATION</div>
				</div>

				<div class="row philo-4">
					<div class="col-md-12 philosophy-value" style="width:300px">VALUE CREATION</div>
				</div>
			</div>
		</div>
	</div>

	<div class="container-fluid philosophy-background-three">
			<div class="col-md-12">
				<div class="philosophy-left col-md-6 col-lg-6">
					<div class="philosophy-second-message">
						<p>Every project at Gamuda Land begins<br>
						with a holistic and comprehensive<br>
						Master Plan to create a well-laid out<br>
						development that is able to sustain<br>
						several generations.</p>
					</div>
				</div>

				<div class="straight-white-line-two straight-white-line-three"></div>

				<div class="philosophy-right col-md-6 timeline-init col-lg-6">
					<div class="row philo-first-a">
						<div class="col-md-2 philosophy-a">I</div>
						<div class="col-md-10 philosophy-a-title-enlarge large-title-correction">STRONG MASTER PLANNING</div>
					</div>
					<div class="row philo-first-a-description">
						<div class="col-md-2 philosophy-a-empty"></div>
						<div class="col-md-10 philosophy-a-description">
							<ul>
								<li>We plan and design each development for long-term
								viability and growth with at least 40% of the land area
								earmarked for greenscapes. This allows communities
								to grow and expand organically in terms of population
								and activities.</li>
								<li>We tap innovative solutions to deliver designs that are
								workable and which will be well utilised by residents.</li>
								<li>We focus on the preservation of the natural
								topography and incorporate well-planned,
								multi-functional natural features to ensure the
								sustainability of the natural environment.</li>
								<li>We incorporate designs of the highest quality that
								features the highest CONQUAS scores, Green Building
								Index or GBI-certification, plus recognition from other
								professional bodies.</li>
							</ul>
						</div>
					</div>

					<div class="row philo-first-b">
						<div class="col-md-2 philosophy-small">II</div>
						<div class="col-md-10 philosophy-small-title two-small-line-correction">BEAUTIFULLY CRAFTED<br>ENVIRONMENT</div>
					</div>

					<div class="row philo-first-c">
						<div class="col-md-2 philosophy-small">III</div>
						<div class="col-md-10 philosophy-small-title one-small-line-correction">GOOD LOCATION</div>
					</div>

					<div class="row philo-first-value">
						<div class="col-md-12 philosophy-value">VALUE CREATION</div>
					</div>
				</div>
			</div>
	</div>

	<div class="container-fluid philosophy-background-four">
			<div class="col-md-12">
				<div class="philosophy-left col-md-6 col-lg-6">
					<div class="philosophy-third-message">
						<p>We place great emphasis on place making.<br>
						We go all out to ensure a happy and healthy<br>
						community by providing a beautiful and<br>
						conductive environment that contains the right<br>
						mix and hierarchy of community facilities and<br>
						amenities. All of these aim to provide a<br>
						complete and enhanced quality of life.</p>
					</div>
				</div>

				<div class="straight-white-line-two straight-white-line-four"></div>

				<div class="philosophy-right col-md-6 timeline-init col-lg-6">
					<div class="row philo-second-a">
						<div class="col-md-2 philosophy-small">I</div>
						<div class="col-md-10 philosophy-small-title two-small-line-correction">STRONG MASTER<br>PLANNING</div>
					</div>
					<div class="row philo-second-b">
						<div class="col-md-2 philosophy-a">II</div>
						<div class="col-md-10 philosophy-b-title-enlarge large-title-correction">BEAUTIFULLY CRAFTED ENVIRONMENT</div>
					</div>
					<div class="row philo-first-a-description">
						<div class="col-md-2 philosophy-empty"></div>
						<div class="col-md-10 philosophy-b-description">
							<ul>
								<li>By incorporating clubhouses and communal spaces
								(i.e. parks, lakes and playgrounds) at the right locations,
								we encourage community interaction.</li>
								<li>Our offer of the right mix of lifestyle and retail services,
								health and recreational facilities, entertainment centres
								and public/private schools enable us to consistently meet
								the lifestyle needs of our communities.</li>
								<li>To safeguard residents and ensure their safety, we continue
								to adopt a two-pronged approach:</li>
								<ul class="list-smaller-font circle-type">
									<li>We were the pioneers of the gated and guarded community
									concept and today we continue to offer a three-tiered security system comprising perimeter fencing with camera surveillance, panic buttons in homes, plus guardhouses with 24-hour guard patrols.</li>
									<li>To ensure resident safety, we incorporate features such as curve roads and speed breakers to slow down traffic as well as cul-de-sacs to minimise pass-through traffic.</li>
								</ul>
							</ul>
						</div>
					</div>

					<div class="row philo-second-c">
						<div class="col-md-2 philosophy-small">III</div>
						<div class="col-md-10 philosophy-small-title one-small-line-correction">GOOD LOCATION</div>
					</div>

					<div class="row philo-second-value">
						<div class="col-md-12 philosophy-value">VALUE CREATION</div>
					</div>
				</div>
			</div>
	</div>

	<div class="container-fluid philosophy-background-five">
			<div class="col-md-12">
				<div class="philosophy-left col-md-6 col-lg-6">
					<div class="philosophy-fourth-message">
						<p>We focus on good locations.<br>
						Together with a well-planned sustainable<br>
						environment, as well as the right mix of<br>
						features and amenities befitting a holistic<br>
						lifestyle, this lends to the makings of a<br>
						development with good growth value.<br>
						To date, our track record of producing<br>
						such assets speaks for itself.</p>
					</div>
				</div>

				<div class="straight-white-line-two straight-white-line-five"></div>

				<div class="philosophy-right col-md-6 col-lg-6 timeline-init">
					<div class="row philo-third-a">
						<div class="col-md-2 philosophy-small">I</div>
						<div class="col-md-10 philosophy-small-title two-small-line-correction">STRONG MASTER<br>PLANNING</div>
					</div>
					<div class="row philo-third-b">
						<div class="col-md-2 philosophy-small">II</div>
						<div class="col-md-10 philosophy-small-title two-small-line-correction">BEAUTIFULLY CRAFTED<br>ENVIRONMENT</div>
					</div>
					<div class="row philo-third-c">
						<div class="col-md-2 philosophy-a">III</div>
						<div class="col-md-10 philosophy-c-title-enlarge large-title-correction">GOOD LOCATION</div>
					</div>
					<div class="row philo-first-a-description">
						<div class="col-md-2 philosophy-empty"></div>
						<div class="col-md-10 philosophy-c-description">
							<ul>
								<li>Our developments are built in areas with ready accessibility (i.e. via highways or public transportation) thus providing easy connectivity to key areas.</li>
								<li>Our properties are strategically located in areas of high growth potential thereby positioning them for sustainable value growth. These include:</li>
								<ul class="list-smaller-font circle-type">
									<li>Mature townships and established lifestyle areas for the convinience of residents and good rental yields;</li>
									<li>City fringe developments close to or directly linked to key highways to ease travel to city centres and areas undergoing expansion; and</li>
									<li>Commercial sites with good frontage.</li>
								</ul>
							</ul>
						</div>
					</div>

					<div class="row philo-third-value">
						<div class="col-md-12 philosophy-value">VALUE CREATION</div>
					</div>
				</div>
			</div>
	</div>

	<div class="container-fluid philosophy-background-six">
		<div class="col-md-12">
			<div class="philosophy-left col-md-6 col-lg-6"></div>

			<div class="straight-white-line-final"></div>

			<div class="philosophy-right col-md-6 col-lg-6 timeline-init">
				<div class="row philo-final-a">
					<div class="col-md-2 philosophy-dark">I</div>
					<div class="col-md-10 philosophy-dark-title">STRONG MASTER<br>PLANNING</div>
				</div>

				<div class="row philo-final-b">
					<div class="col-md-2 philosophy-dark">II</div>
					<div class="col-md-10 philosophy-dark-title">BEAUTIFULLY CRAFTED<br>ENVIRONMENT</div>
				</div>

				<div class="row philo-final-c">
					<div class="col-md-2 philosophy-dark">III</div>
					<div class="col-md-10 philosophy-dark-title one-small-line-correction">GOOD LOCATION</div>
				</div>

				<div class="row philo-enlarged">
					<div class="col-md-12 philosophy-value-bigger">VALUE CREATION</div>
					<div class="col-md-12 philosophy-value-description">
						<p class="red-text">I + II + III  =  Value Creation</p>
						<p class="value-description" style="padding-top: 10px;">Strong master planning, a beautiful crafted<br>
						environment, plus good location are the winning attributes that offer residents a complete and sustainable lifestyle for today and tomorrow. All these elements also lend to the makings of developments that have capital values that continue to appreciate over time.<br><br>
						As testament to our efforts, Gamuda Land has successfully won The Edge-PFPS Value Creation Excellence Award for four consecutive years. This award measures the capital appreciation of properties between their selling and sebsequent resale prices, and our properties have proven their mettle time and time again.</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="container-fluid philosophy-background-seven"></div>

	<div class="container-fluid philosophy-one">
		<div class="philosophy-first-part">
			<p class="philosophy-header-one">OUR PHILOSOPHY</p>
		</div>
		<div class="mouse">
			<div class="mouse-icon">
			<span class="mouse-wheel"></span>
			</div>
		</div>
	</div>

	<div class="container-fluid philosophy-two" id="philosophy-trigger-one"></div>
	<div class="container-fluid philosophy-three" id="philosophy-trigger-two"></div>
	<div class="container-fluid philosophy-four" id="philosophy-trigger-three"></div>
	<div class="container-fluid philosophy-five" id="philosophy-trigger-four"></div>
	<div class="container-fluid philosophy-six" id="philosophy-trigger-five"></div>
	<div class="container-fluid philosophy-seven" id="philosophy-trigger-six">
		<div class="more-about-us-section">
			<div class="container" style="padding:0;min-width:1200px"><h3>MORE ABOUT US</h3></div>
			<div class="row">
				<div class="col-sm-6 np-img"><a href="http://128.199.89.40/about-us/milestones/"><img src="/wp-content/uploads/2016/08/milestones.jpg" alt="Go to Milestones Page"></a></div>
				<div class="col-sm-6 pp-img"><a href="http://128.199.89.40/about-us/awards/"><img src="/wp-content/uploads/2016/08/awards.jpg" alt="Go to Awards Page"></a></div>
			</div>
		</div>
	</div>
</div>
<?php get_footer();
// Omit closing PHP tag to avoid "Headers already sent" issues.