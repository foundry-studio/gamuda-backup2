<?php
// Template Name:  Bandar Botanic Template
get_header(); ?>
<div id="botanic-indicator" class="scroll-indicator">
    <ul>
        <li id="bb-indicator-home">Home<div class="indicator-circle" data-num="1"></div></li>
        <li id="bb-indicator-overview">Overview<div class="indicator-circle" data-num="2"></div></li>
        <li id="bb-indicator-features">Features<div class="indicator-circle" data-num="3"></div></li>
        <li id="bb-indicator-facilities">Facilities<div class="indicator-circle" data-num="4"></div></li>
        <li id="bb-indicator-master">Master Site Plan<div class="indicator-circle" data-num="5"></div></li>
        <li id="bb-indicator-happenings">Happenings<div class="indicator-circle" data-num="6"></div></li>
        <li id="bb-indicator-more">Other Developments<div class="indicator-circle" data-num="7"></div></li>
    </ul>
</div>
<div class="page-breadcrumbs slider-breadcrumbs">
	<p><a href="http://128.199.89.40/">Home</a> <span>/</span> <a href="./our-developments/">Developments</a> <span>/</span> <a href="./our-developments/#ourdev-land-fullysold-section">Fully Sold Developments</a> <span>/</span> Bandar Botanic</p>
</div>
<div id="botanic-box-wrap" class="botanic-desktop">
	<div id="botanic-box-main" class="botanic-section-wrap">
		<div class="bg-full-wrap"></div>
		<div class="bg-halfwrap-left"></div>
		<div class="bg-halfwrap-right"></div>
		<div class="main-text-box1">
			<div class="main-text-box-left"><img src="/wp-content/uploads/2016/07/logo-bandar-botanic.png" class="pull-right" style="width:70%"></div>
			<div class=" main-text-box-right"><h5>WHERE TRANQUILITY<br>SURROUNDS YOU</h5></div>
		</div>
		<div class="main-text-box2">
			<div class="main-text-box-left"><h5>ELEGANCE WITHIN A<br>SCENIC TOWNSHIP</h5><svg height="100" width="350"> <polygon points="0,0 20,100 350,90 350,15" style="fill:#099f93"></polygon> </svg></div>
			<div class=" main-text-box-right"><p>One of the most sought-after developments in the Klang Valley, Bandar Botanic offers you a holistic lifestyle with its meticulously planned "Home in A Garden' concept and host of amenities. Having set the benchmark for developments of its kind, this premier integrated self-sustaining township continues to receive accolades for sustainable value appreciation year after year.</p></div>
		</div>
	</div>
	<div id="botanic-box-overview" class="botanic-section-wrap">
		<div class="bg-halfwrap-left"></div>
		<div class="bg-halfwrap-right"></div>
        <div class="botanic-text-box col-md-7 col-md-offset-2 col-lg-4 col-lg-offset-6">
			<h5>OVERVIEW</h5>
			<div class="text-box-border">
				<p>A multi-year winner of the Edge-PEPS Value Creation Excellence Award, Bandar Botanic continues to thrive as a premier integrated self-sustaining township. Conceived in the year 2000, this development pioneered the concept of bringing the outdoors, indoors by weaving nature seamlessly into the everyday lives of its community.</p>
				<p>Encompassing comprehensive retail and commercial services as well as community amenities, this township has become has become the new benchmark for property developments in the Klang Valley. Built with nature in mind and seeking to make a difference in the way people live, Bandar Botanic offers a harmonious, sustainable environment that will be enjoyed for generations to come.</p>
			</div>
		</div>
	</div> 
	<div id="botanic-box-features" class="botanic-section-wrap">
		<div class="features1 bg-halfwrap-left"></div>
		<div class="features1 bg-halfwrap-right"></div>
        <div class="botanic-text-box desktop-tab-group col-md-4 col-md-offset-1">
            <ul class="xs-font list-unstyled list-inline">
                <li class="active" data-num="1">FEATURES</li>
                <li data-num="2">BOTANIC WEST</li>
                <li data-num="3">BOTANIC EAST</li>
            </ul>
            <div class="text-box-border desktop-tab desktop-tab1">
                <p>The appeal of Bandar Botanic lies in its rich, tropical landscape. Conceived as "A Home in A Garden", an emphasis has been placed on creating a lush green environment for the community to enjoy the serene beauty of nature from the comfort of their homes.</p>
				<p>To this end, a significant portion of the development has been dedicated to green lungs, parks, lakes and open spaces, all with the aim of encouraging healthy and active lifestyles as well as harmonious community living.</p>
				<p>The Botanic West and Botanic East precincts within Bandar Botanic have been meticulously planned. Each embody their own distinct characteristics, parkland environment, amenities and facilities to provide residents a holistic lifestyle.</p>
				<p>In 2004, Bandar Botanic’s 2-km signature Central Lake and Park, the community’s main recreational hub, was awarded the prestigious Malaysia National Landscape Award 2004 under the City Garden category.</p>
            </div>
            <div class="text-box-border desktop-tab desktop-tab2">
                <p>Botanic West is Bandar Botanic's very own 70-acre botanical parkland area encompassing a wide expanse of verdant greens, tree-lined boulevards and thematic gardens. It is this botanical parkland that gives the township its identity and provides the community with an avenue for rest, recreation and community interaction.</p>
				<p>Every detail of Botanic West's Central Lake and Park was purposefully planned to give the township a distinct botanical parkland character. The 2-km Central Lake and Park is laden with luxurious thematic gardens that include a topiary garden, sundial garden, alphabet garden, children’s island and culinary garden as well as exciting water cascades beautifully landscaped for the community to enjoy. Other features include a flower bridge, flower tunnel, amphitheatre, waterwheel, gazebos and floating boardwalk.</p>
            </div>
            <div class="text-box-border desktop-tab desktop-tab3">
                <p>With its offer of a 100-acre waterfront-parkland setting and its resort-styled ambience, residents of Botanic East get to enjoy carefree evenings relishing the breathtaking views of pristine green surroundings and waterways.</p>
				<p>Lush greenery and themed features are aplenty at Botanic East. With names such as Children's Island, Rainbow Lake, Floating Garden, Meandering Creek, Pocket Garden, Ribbon Lake, Crescent Lake and Hanami Garden, each themed feature adds its own distinct flavour to make this tranquil enclave a unique neighbourhood.</p>
            </div>
        </div>
	</div> 
	<div id="botanic-box-facilities" class="botanic-section-wrap">
		<div class="bg-halfwrap-left"></div>
		<div class="bg-halfwrap-right"></div>
        <div class="botanic-text-box col-md-7 col-md-offset-2 col-lg-4 col-lg-offset-6">
			<h5>FACILITIES &amp; AMENITIES</h5>
			<div class="text-box-border">
				<p>Bandar Botanic was built to meet the needs of the community and everything residents need is at their doorstep.</p>
				<p>Residents have easy access to the largest Southeast Asian wholesale city - GM Klang, Aeon Mall, Giant and Tesco, plus a host of other retail and commercial facilities that dot the neighbourhood. Public transportation is readily available to take residents straight to urban centres like Klang and back.</p>
				<p>The Central Botanic Parkland has been designed as a social and recreational hub for the community and is situated within walking distance. On top of this, amenities such as schools, kindergartens, community halls, places of worship, local shops and bus stops are inter-connected with the parkland via series of "green fingers" to reduce pedestrian-vehicular conflict and to promote community living.</p>
				<p>In short, everything that is fundamental to enhancing a community’s lifestyle has been thought of.</p>
			</div>
		</div>
	</div>
	<div id="botanic-box-master" class="botanic-section-wrap">
		<div class="grey-box-bg"></div>
		<div class="master1 bg-full-wrap"></div>
		<div class="master2 bg-full-wrap"></div>
		<div class="master3 bg-halfwrap-left"></div>
		<div class="master3 bg-halfwrap-right"></div>
		<div class="master4 bg-halfwrap-left"></div>
		<div class="master4 bg-halfwrap-right"></div>
		<div class="master-title-box1"><h5>MASTER SITE PLAN</h5></div>
		<div class="master-map-box1"><img src="/wp-content/uploads/2016/09/bandar-botanic-master1-map.png"></div>
		<div class="master-text-box1">
			<div class="text-box-border">
				<p>Bandar Botanic was conceived as a waterfront-parkland where the endless meandering lakes and parks would give the township a botanical feel.</p>
				<p>To achieve this concept, Gamuda Land’s team of creative and innovative engineers and architects began at the foundation by working on a meticulous and well-thought-out masterplan. They placed a major emphasis on providing a green and healthy environment for the community. To this end, more than 180 acres of the township were dedicated to greens, parks and lakes to provide the community a holistic, quality lifestyle.</p>
				<p>The plan and design for the township also incorporated a multitude of value-added features such as superior infrastructure, an abundance of parks and greenery, as well as a host of other community-centric facilities and amenities.</p>
			</div>
		</div>
		<div class="text-box master-text-box2-left">
			<div class="text-box-border"><p>To underscore Gamuda Land’s commitment to providing ready connectivity to Bandar Botanic, we constructed a RM30 million dedicated interchange to link the township directly to the Shah Alam Expressway.</p><br>
			<p>In line with our efforts to uphold good environmental architecture practices, we brought several innovative solutions into play. Bandar Botanic was the first development to not only comply with, but also exceed the mandatory requirements of the Department of Irrigation and Drainage’s Urban Stormwater Management System (MASMA). Leveraging on a novel Floating Pile Raft Foundation System, we transformed the basic irrigation network within the development into an extensively landscaped park that elevated the standard of living in the area as well as the value of the surrounding properties.</p><br>
			<p>Today the Central Lake Park functions as a detention and regulating pond for surface water run-off, while its themed parks and gardens as well as leisure activity areas are well utilised by the community.</p></div>
		</div>
		<div class="text-box master-text-box2-right">
			<div class="text-box-border"><p>Botanic West features a unique 70-acre botanical parkland with a 2-km lake. Homes here are designed to look over the Central Lake and are surrounded by beautifully landscaped pocket parks that truly add to the tranquillity and privacy of the botanical haven.</p><br>
			<p>Botanic East offers unique waterfront parkland with lush landscaping, private gardens and green lungs in a botanical environment.</p>
			<p>The waterfront with its lavish parks and gardens provide a serene and tranquil ambience to this enclave of bungalow homes, semi-detached homes and garden terraces.</p></div>
		</div>
		
		<div class="master-text-box3 text-box col-lg-5 col-lg-offset-5 col-md-8 col-md-offset-2">
			<div class="text-box col-lg-10 col-md-9 col-md-offset-1 master-text-box3-top">
				<h5><span style="color:#000">MASTER SITE PLAN - </span><span style="color:#add043">RESIDENTIAL</span></h5>
				<img src="/wp-content/uploads/2016/08/slide-master-slide-3-map.png">
			</div>
			<div class="text-box master-text-box3-bottom">
				<div class="text-box-border"><p>Each precinct is characterised by its natural setting which gives it a distinctive feature. Each of the homes, regardless of whether it is a luxury bungalow or garden terrace, is designed with novel features and exciting architectural designs that enhances its uniqueness.</p><br>
				<p>The homes, which are designed and planned to benefit from the aesthetics of the natural world, offer close affinity to the surrounding greenery, landscaping and water features. Liberal features such as expansive terraces, spacious courtyards and tall windows allow greater freedom for each home to fuse with its surroundings. To ensure residents enjoy optimum views of the waterways and expansive greens, homes are designed with water frontages or garden frontages.</p><br>
				<p>To ensure maximum benefit to the community, many of the development’s finer details have been meticulously planned. These include features such as underground utilities, roll-over kerbs, pedestrian-safe walkways and an efficient road and traffic management systems. Another effective neighbourhood feature to enhance the security and privacy of residents is the cul-de-sac road that ensures only one entry and exit point to homes.</p></div>
			</div>
		</div>
		<div class="master-text-box4 text-box col-lg-5 col-lg-offset-1 col-md-8 col-md-offset-2">
			<div class="text-box col-lg-10 col-md-9 col-md-offset-1 master-text-box4-top">
				<h5><span style="color:#000">MASTER SITE PLAN - </span><span style="color:#1a4db3">COMMERCIAL</span></h5>
				<img src="/wp-content/uploads/2016/08/slide-master-slide-4-map.png">
			</div>
			<div class="text-box master-text-box3-bottom">
				<div class="text-box-border"><p>The community at Bandar Botanic have ready access to a bevy of everyday conveniences. These include ample shopping, dining and entertainment centres such as Aeon Mall, Tesco, Giant and the GM Klang Wholesale City.</p><br>
				<p>Within its boundaries, Bandar Botanic also offers exclusive and exciting shopping and business experiences at the Botanic Walk Lakefront Commercial Centre, Botanic Capital, Botanic Avenue and Botanic Business Gateway.</p><br>
				<p>The commercial centre comprising Botanic Walk East and West is designed as a community venue for families to enjoy the pleasures of life be it by way of recreational activities, meeting friends and family, or just relaxing and enjoying a good meal. Strategically located alongside Central Lake Park, the centre provides residents a unique shopping experience.</p><br>
				<p>Meanwhile, the commercial hub comprising Botanic Capital, Botanic Avenue and Botanic Business Gateway encompasses two, three and four-storey shop offices. These house a shopping mall, boutique shops, retail shop offices, an automotive showroom and corporate offices, among others.</p></div>
			</div>
		</div>
	</div>		
	<div id="botanic-box-happenings" class="botanic-section-wrap">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<?php the_content(); ?>
<?php endwhile; endif; ?>
	</div>
	<div id="botanic-box-other-dev" class="botanic-section-wrap">
		<div class="other-dev-section">
			<div class="container" style="padding:0;min-width:1200px"><h3 style="color:#099f93"><strong>OTHER DEVELOPMENTS</strong></h3></div>
			<div class="row">
				<div class="col-sm-6 np-img"><a href="http://128.199.89.40/our-developments/fully-sold-developments/kota-kemuning/"><img src="/wp-content/uploads/2016/09/other-dev-banner-kota-kemuning.jpg" alt="Go to Milestones Page"></a></div>
				<div class="col-sm-6 pp-img"><a href="http://128.199.89.40/our-developments/fully-sold-developments/valencia/"><img src="/wp-content/uploads/2016/09/other-dev-banner-valencia.jpg" alt="Go to Awards Page"></a></div>
			</div>
		</div>
	</div>
</div>

<script>
    jQuery(document).ready(function(){
        jQuery('.page-id-6933 .l-footer').wrap("<div id='botanic-box-footer'></div>");
        jQuery('.page-id-6933 #botanic-box-footer').appendTo('.page-id-6933 #botanic-box-wrap')
    });
</script>
<?php get_footer();