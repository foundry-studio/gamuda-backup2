<?php
// Template Name:  Philosophy Mobile Template 
get_header(); ?>
<div class="mobile-main-nav mobile-main-nav-all">
    <div class="mobile-main-nav-header">
        <a class="mobile-main-nav-logo" href="http://128.199.89.40/"><img src="http://128.199.89.40/wp-content/uploads/2016/07/logo-1.png" alt="Gamuda Land Logo"></a>
		<div class="mobile-main-nav-close"><img src="/wp-content/uploads/2016/09/nav-close-btn.png"></div>
    </div>
    <ul class="list-unstyled">
        <li><a href="http://128.199.89.40/about-us/">ABOUT US</a></li>
        <li class="mobile-sub-nav-open">
            <a href="http://128.199.89.40/our-developments/">OUR DEVELOPMENTS</a>
            <i class="fa fa-angle-right" aria-hidden="true"></i>
        </li>
        <li><a href="http://128.199.89.40/happenings/">HAPPENINGS</a></li>
        <li><a href="http://128.199.89.40/contact-us/">CONTACT US</a></li>
    </ul>
    <a class="mobile-main-nav-register" href="https://webreg.gamudaland.com.my/contact/_publicwebregistration/" target="_blank">REGISTER</a>
    <div class="mobile-main-nav-sns">
        <a href="https://www.facebook.com/GamudaLand/?fref=ts&ref=br_tf"><i class="fa fa-facebook" aria-hidden="true"></i></a>
        <a href="https://www.instagram.com/gamudaland/"><i class="fa fa-instagram" aria-hidden="true"></i></a>
    </div>
</div>

<div class="mobile-main-nav mobile-main-nav-projects">

    <div class="mobile-main-nav-header">
        <a class="mobile-main-nav-logo" href="http://128.199.89.40/"><img src="http://128.199.89.40/wp-content/uploads/2016/07/logo-1.png" alt="Gamuda Land Logo"></a>
		<div class="mobile-main-nav-close"><img src="/wp-content/uploads/2016/09/nav-close-btn.png"></div>
    </div>
    <div class="mobile-main-nav-projects-list">
        <ul class="list-unstyled">
            <li class="mobile-sub-nav-close">
                <b>FEATURED</b> DEVELOPMENTS
                <i class="fa fa-angle-left" aria-hidden="true"></i>
            </li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/highpark-suites/">HIGHPARK SUITES<span>Kelana Jaya</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/the-robertson/">THE ROBERTSON<span>Kuala Lumpur</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/jade-hills/">JADE HILLS<span>Kajang</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/horizon-hills/">HORIZON HILLS<span>Iskandar Puteri</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/bukit-bantayan/">BUKIT BANTAYAN<span>Kota Kinabalu</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/gamuda-walk/">GAMUDA WALK<span>Shah Alam</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/gem-residences/">GEM RESIDENCES<span>Toa Payoh</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/661-chapel-st/">661 CHAPEL ST.<span>Melbourne</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/gamuda-city/">GAMUDA CITY<span>Hanoi</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/celadon-city/">CELADON CITY<span>Ho Chi Minh</span></a></li>
        </ul>
        <ul class="list-unstyled">
            <li><b>UPCOMING</b> DEVELOPMENTS</li>
            <li><a href="http://gamudaland.com.my/kundangestates/">KUNDANG ESTATES<span>Kuang</span></a></li>
            <li><a href="#" class="navi-disable">GAMUDA GARDENS<span>Kuang</span></a></li>
            <li><a href="http://gamudaland.com.my/twentyfive7/">TWENTYFIVE7<span>Kota Kemuning</span></a></li>
        </ul>
        <ul class="list-unstyled">
            <li><b>FULLY SOLD</b> DEVELOPMENTS</li>
            <li><a href="http://128.199.89.40/our-developments/fully-sold-developments/kota-kemuning/">KOTA KEMUNING<span>Shah Alam</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/fully-sold-developments/bandar-botanic/">BANDAR BOTANIC<span>Klang</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/fully-sold-developments/valencia/">VALENCIA<span>Sungai Buloh</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/fully-sold-developments/madge-mansions/">MADGE MANSIONS<span>Kuala Lumpur</span></a></li>
        </ul>
        <ul class="list-unstyled">
            <li><b>GOLF &amp; COUNTRY</b> CLUBS</li>
            <li><a href="http://www.kotapermai.com.my/" target="_blank">KOTA PERMAI<span>Golf &amp; Country Club</span></a></li>
            <li><a href="http://hhgcc.com.my/" target="_blank">HORIZON HILLS<span>Golf &amp; Country Club</span></a></li>
            <li><a href="http://www.botanicresortclub.com.my/" target="_blank">BOTANIC<span>Resort Club</span></a></li>
        </ul>
    </div>
</div>
<div id="mobile-pages" class="mobile-slide-page mobile-philosophy mobile-page">
<div class="container-fluid document">
    <div class="row mobile-header-test"></div>

    <div class="row mobile-swipe-wrap no-timeline">
        <!-------------------- Main Cover -------------------->
        <div class="mobile-swipe-cover mobile-philosophy-main-cover">
            <div class="mobile-swipe-cover-title">
                <h1 class="lg-font" style="color:#fff">OUR PHILOSOPHY</h1>
                <p classs="sm-font">Value Creation</p>
                <div class="mobile-main-vertical-line"></div>
            </div>
			<div class="mouse">
				<div class="mouse-icon">
					<span class="mouse-wheel"></span>
				</div>
			</div>
<!--            <img class="cover-bg" src="/wp-content/themes/Impreza-child/img/philosophy-main-cover.jpg" alt="Philosophy Background Image">-->
        </div>

        <!-------------------- Philosophy Tab -------------------->
        <div class="mobile-tab-cover">
            <div class="mobile-full-vertical-line"></div>
            <p class="sm-font">Our commitment to delivering sustainable value to communities is underpinned by these three key principles that guide the creation of all our developments.</p>
            <div class="philosophy-tab-group">
                <div class="philosophy-tab">
                    <div class="philosophy-tab-title philosophy-tab-title1">
                        <a href="#philosophy-tab-content1">
                            <h2>A</h2>
                            <h3>STRONG MASTER PLANNING</h3>
                        </a>
                    </div>
                    <div class="philosophy-tab-content philosophy-tab-content1" id="philosophy-tab-content1">
                        <ul class="sm-font">
                            <li>We plan and design each development for long-term viability and growth with at least 40% of the land area earmarked for greenscapes. This allows communities to grow and expand organically in terms of population and activities.</li>
                            <li>We tap innovative solutions to deliver designs that are workable and which will be well utilised by residents.</li>
                            <li>We focus on the preservation of the natural topography and incorporate well-planned, multi-functional natural features to ensure the sustainability of the natural environment.</li>
                            <li>We incorporate designs of the highest quality that features the highest CONQUAS scores, Green Building Index or GBI-certification, plus recognition from other professional bodies.</li>
                        </ul>
                        <p class="sm-font">Every project at Gamuda Land begins with a holistic and comprehensive Master Plan to create a well-laid out development that is able to sustain several generations.</p>
                    </div>
                </div>
                <div class="philosophy-tab">
                    <div class="philosophy-tab-title philosophy-tab-title2">
                        <a href="#philosophy-tab-content2">
                            <h2>B</h2>
                            <h3>BEAUTIFULLY CRAFTED ENVIRONMENT</h3>
                        </a>
                    </div>
                    <div class="philosophy-tab-content philosophy-tab-content2" id="philosophy-tab-content2">
                        <ul class="sm-font">
                            <li>By incorporating clubhouses and communal spaces (i.e. parks, lakes and playgrounds) at the right locations, we encourage community interaction.</li>
                            <li>Our offer of the right mix of lifestyle and retail services, health and recreational facilities, entertainment centres and public/private schools enable us to consistently meet the lifestyle needs of our communities.</li>
                            <li>To safeguard residents and ensure their safety, we continue to adopt a two-pronged approach:
                                <ul>
                                    <li>We were the pioneers of the gated and guarded community concept and today we continue to offer a three-tiered security system comprising perimeter fencing with camera surveillance, panic buttons in homes, plus guardhouses with 24-hour guard patrols.</li>
                                    <li>To ensure resident safety, we incorporate features such as curve roads and speed breakers to slow down traffic as well as cul-de-sacs to minimise pass-through traffic.
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <p class="sm-font">We place great emphasis on place making. We go all out to ensure a happy and healthy community by providing a beautiful and conductive environment that contains the right mix and hierarchy of community facilities and amenities. All of these aim to provide a complete and enhanced quality of life.</p>
                    </div>
                </div>
                <div class="philosophy-tab">
                    <div class="philosophy-tab-title philosophy-tab-title3">
                        <a href="#philosophy-tab-content3">
                            <h2>C</h2>
                            <h3>GOOD LOCATION</h3>
                        </a>
                    </div>
                    <div class="philosophy-tab-content philosophy-tab-content3" id="philosophy-tab-content3">
                        <ul class="sm-font">
                            <li>Our developments are built in areas with ready accessibility (i.e. via highways or public transportation) thus providing easy connectivity to key areas.</li>
                            <li>Our properties are strategically located in areas of high growth potential thereby positioning them for sustainable value growth. These include:
                                <ul>
                                    <li>Mature townships and established lifestyle areas for the convinience of residents and good rental yields;</li>
                                    <li>City fringe developments close to or directly linked to key highways to ease travel to city centres and areas undergoing expansion; and</li>
                                    <li>Commercial sites with good frontage.</li>
                                </ul>
                            </li>
                        </ul>
                        <p class="sm-font">We focus on good locations. Together with a well-planned sustainable environment, as well as the right mix of features and amenities befitting a holistic lifestyle, this lends to the makings of a development with good growth value. To date, our track record of producing such assets speaks for itself.</p>
                    </div>
                </div>
            </div>
            <div class="philosophy-single-tab">
                <div class="philosophy-tab-title philosophy-single-tab-title">
                    <a href="#philosophy-single-tab-content">
                        <h3>VALUE CREATION</h3>
                    </a>
                </div>
                <div class="philosophy-tab-content philosophy-single-tab-content" id="philosophy-single-tab-content">
                    <h3 class="lg-font">A + B + C = Value Creation</h3>
                    <p class="sm-font">Strong master planning, a beautiful crafted environment, plus good location are the winning attributes that offer residents a complete and sustainable lifestyle for today and tomorrow. All these elements also lend to the makings of developments that have capital values that continue to appreciate over time.</p>
                    <p class="sm-font">As testament to our efforts, Gamuda Land has successfully won The Edge-PFPS Value Creation Excellence Award for four consecutive years. This award measures the capital appreciation of properties between their selling and sebsequent resale prices, and our properties have proven their mettle time and time again.</p>
                </div>
            </div>
        </div>
		<div class="mobile-other-projects">
			<h2 class="md-font">OTHER ABOUT US</h2>
			<div class="col-xs-6 mobile-prev-project">
				<a href="http://128.199.89.40/about-us/milestones/"><img src="/wp-content/uploads/2016/09/m-about-us-banner-milestones.jpg" alt="Milestones"></a>
			</div>
			<div class="col-xs-6 mobile-next-project">
				<a href="http://128.199.89.40/about-us/awards/"><img src="/wp-content/uploads/2016/09/m-about-us-banner-awards.jpg" alt="Awards"></a>
			</div>
		</div>
    </div>
</div>

</div>
<?php get_footer();