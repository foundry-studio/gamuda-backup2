jQuery(document).ready(function(){
    jQuery("#text-milestone-land").css("opacity",1);
    
	//jQuery("#demosMenu").change(function(){
	//  window.location.href = jQuery(this).find("option:selected").attr("id") + '.html';
	//});
    
    jQuery('#fullpage.milestones-scroll').fullpage({
        'verticalCentered': false,
        'css3': true,
        'sectionsColor': ['#F0F2F4', '#fff', '#fff', '#fff'],
        'navigation': true,
        'navigationPosition': 'right',
        'navigationTooltips': ['Home', '2016', '2016', '2016', '2016', '2015', '2015', '2015', '2013', '2013', '2012', '2011', '2008', '2007', '2007', '2005', '2001', '2000', '1995', '1995', 'More About Us'],

        'afterLoad': function(anchorLink, index){
            if(index >= 2){
                jQuery('.page-id-349 header').addClass('sticky');
            }
			if(index == 1){
                jQuery('.page-id-349 header').removeClass('sticky');
            }else if(index == 2){
                jQuery('#text-2016-1, #line-2016-1').addClass('active');
            }
            else if(index == 3){
                jQuery('#text-2016-5, #line-2016-5').addClass('active');   
                jQuery('#section3 .scroll-long').velocity({opacity:1}, {delay: 50, duration: 100});    
                jQuery('#section3 .scroll-long .half-houses-30000').velocity({opacity:1}, {delay: 50, duration: 100});      
            }
            else if(index == 4){
                jQuery('#text-2016-6').addClass('active');
            }
            else if(index == 5){
                jQuery('#text-2016-7, #line-2016-7').addClass('active');  
            }
            else if(index == 6){
                jQuery('#text-2015-2, #line-2015-2').addClass('active');
            }
            else if(index == 7){
                jQuery('#text-2015-1, #line-2015-1').addClass('active');
            }
            else if(index == 8){
                jQuery('#text-2015-3, #line-2015-3').addClass('active');
            }
            else if(index == 9){
                jQuery('#text-2013-1, #line-2013-1').addClass('active'); 
                jQuery('#section9 .scroll-long').velocity({opacity:1}, {delay: 50, duration: 100});    
                jQuery('#section9 .scroll-long .half-landbank').velocity({opacity:1}, {delay: 50, duration: 100});      
            }
            else if(index == 10){
                jQuery('#text-2013-2').addClass('active');
            }
            else if(index == 11){
                jQuery('#text-2012-1, #line-2012-1').addClass('active');
            }
            else if(index == 12){
                jQuery('#text-2011-1, #line-2011-1').addClass('active');
            }
            else if(index == 13){
                jQuery('#text-2008-1, #line-2008-1').addClass('active');
            }
            else if(index == 14){
                jQuery('#text-2007-1, #line-2007-1').addClass('active');
            }
            else if(index == 15){
                jQuery('#text-2007-2, #line-2007-2').addClass('active');
                jQuery('#section15 .scroll-long').velocity({opacity:1}, {delay: 50, duration: 100});    
                jQuery('#section15 .scroll-long .half-gamudaland').velocity({opacity:1}, {delay: 50, duration: 100});      
            }
            else if(index == 16){
                jQuery('#text-2005-1').addClass('active');
            }
            else if(index == 17){
                jQuery('#text-2001-1, #line-2001-1').addClass('active');
            }
            else if(index == 18){
                jQuery('#text-2000-1, #line-2000-1').addClass('active');
            }
            else if(index == 19){
                jQuery('#text-1995-1, #line-1995-1').addClass('active');
            }
            else if(index == 20){
                jQuery('#text-1995-2').addClass('active');
                jQuery('.page-id-349 .page-breadcrumbs').velocity({opacity:1});
            }
            else if(index == 21){
                jQuery('#text-1995-2').addClass('active');
                jQuery('.page-id-349 .page-breadcrumbs').velocity({opacity:0});
            }
        },

        'onLeave': function(index, nextIndex, direction){
            if (index == 3 && direction == 'down'){    
                //jQuery('#section3 .scroll-long').addClass('scroll-long1');
                jQuery('#section3 .scroll-long').velocity({bottom:'-200vh'}, {delay: 0, duration: 700, easing: 'ease'});           
                jQuery('#section3 .scroll-long .half-kundang').velocity({opacity:0}, {delay: 800, duration: 100});  
            }
            else if(index == 3 && direction == 'up'){
                //jQuery('.section').eq(index -1).removeClass('moveUp').addClass('moveDown');
                jQuery('#section3 .scroll-long .half-houses-30000').velocity({opacity:0}, {delay: 0, duration: 100});
            }
            if (index == 4 && direction == 'down'){                
                //jQuery('.section').eq(index -1).removeClass('moveDown').addClass('moveUp');
            }
            else if(index == 4 && direction == 'up'){ 
                //jQuery('#section3 .scroll-long').removeClass('scroll-long1'); 
                jQuery('#section3 .scroll-long').velocity({bottom:0}, {delay: 0, duration: 700, easing: 'ease'});
                jQuery('#section3 .scroll-long .half-kundang').velocity({opacity:1}, {delay: 0, duration: 50});   
            }
            
            if (index == 9 && direction == 'down'){    
                //jQuery('#section9 .scroll-long').addClass('scroll-long1'); 
                jQuery('#section9 .scroll-long').velocity({bottom:'-200vh'}, {delay: 0, duration: 700, easing: 'ease'});            
                jQuery('#section9 .scroll-long .half-robertson').velocity({opacity:0}, {delay: 800, duration: 100});  
            }
            else if(index == 9 && direction == 'up'){
                jQuery('#section9 .scroll-long .half-landbank').velocity({opacity:0}, {delay: 0, duration: 100});
            }
            if (index == 10 && direction == 'down'){                
            }
            else if(index == 10 && direction == 'up'){ 
                //jQuery('#section9 .scroll-long').removeClass('scroll-long1'); 
                jQuery('#section9 .scroll-long').velocity({bottom:0}, {delay: 0, duration: 700, easing: 'ease'});
                jQuery('#section9 .scroll-long .half-robertson').velocity({opacity:1}, {delay: 0, duration: 50});   
            }
            
            if (index == 15 && direction == 'down'){    
                //jQuery('#section15 .scroll-long').addClass('scroll-long1'); 
                jQuery('#section15 .scroll-long').velocity({bottom:'-200vh'}, {delay: 0, duration: 700, easing: 'ease'});           
                jQuery('#section15 .scroll-long .half-gamudacity').velocity({opacity:0}, {delay: 800, duration: 100});  
            }
            else if(index == 15 && direction == 'up'){
                jQuery('#section15 .scroll-long .half-gamudaland').velocity({opacity:0}, {delay: 0, duration: 100});
            }
            if (index == 16 && direction == 'down'){                
            }
            else if(index == 16 && direction == 'up'){ 
                //jQuery('#section15 .scroll-long').removeClass('scroll-long1');
                jQuery('#section15 .scroll-long').velocity({bottom:0}, {delay: 0, duration: 700, easing: 'ease'});
                jQuery('#section15 .scroll-long .half-gamudacity').velocity({opacity:1}, {delay: 0, duration: 50});   
            }
        }
    });
});