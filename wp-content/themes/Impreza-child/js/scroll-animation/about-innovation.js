jQuery(document).ready(function() {
    jQuery('.section1').on('DOMMouseScroll mousewheel', function (e) {
        if(e.originalEvent.detail > 0 || e.originalEvent.wheelDelta < 0) {
            
            // Controller Section 1 Out
            jQuery('#section-controller').velocity({top:'-100vh'});
            
            // Innovation & Creativitity In 
            jQuery('.bg-behind').css('background-image', 'url(/wp-content/uploads/2016/08/innovation-creativity-bg.jpg)');
            jQuery('.bg-front').velocity({top:'20%'}, {delay: 50, duration: 500});
            jQuery('#inno-inno-chart').velocity({opacity: 1}, {delay: 500, duration: 1000});
        } else {
            // UP
//            jQuery('.section-wrap').velocity({top: '0'});
        }
        return false;
    });

    jQuery('.section2').on('DOMMouseScroll mousewheel', function (e) {
        if(e.originalEvent.detail > 0 || e.originalEvent.wheelDelta < 0) {
            
            // Controller Section 2 Out
            jQuery('#section-controller').velocity({top:'-200vh'});
            
            // Innovation & Creativitity Out
            jQuery('.bg-front').velocity({top:'100%'}, {delay: 100, duration: 600});
            jQuery('#inno-inno-chart').velocity({opacity: 0}, {delay: 100, duration: 500});
            jQuery('#inno-home').velocity({delay: 500, duration: 0},{ display:'none'});
            
            // Masterplanning In 
            jQuery('#inno-master').velocity({delay: 0, duration: 0},{ display:'block'});
            jQuery('#inno-master-left').velocity({top:0});
            jQuery('#inno-master-right').velocity({bottom:0});
//            jQuery('#inno-master-front').velocity({opacity:1, bottom:'-5%'},{delay: 300, duration: 100});
            jQuery('#text-inno-master').velocity({bottom:'-8%', opacity:1},{delay: 400, duration:500});
            jQuery('#text-inno-master-sm').velocity({opacity:1},{delay: 600, duration:300});
            
        } else {
            
            // Controller Section 1 In
            jQuery('#section-controller').velocity({top: '0'});  
            
            // Home In Reverse      
            jQuery('.bg-behind').css('background-image', 'url(/wp-content/uploads/2016/08/innovation-home-bg.jpg)')
            jQuery('.bg-front').velocity({top:'0'});
            jQuery('#inno-inno-chart').velocity({opacity: 0}, {delay: 100, duration: 300});
        }
        return false;
    });

    jQuery('.section3').on('DOMMouseScroll mousewheel', function (e) {
        if(e.originalEvent.detail > 0 || e.originalEvent.wheelDelta < 0) {
            
            // Controller Section 3 Out
            jQuery('#section-controller').velocity({top:'-300vh'});
            
            // Masterplanning Out            
            jQuery('#inno-master-left').velocity({top:'-100%'});
            jQuery('#inno-master-right').velocity({bottom:'-100%'});
            jQuery('#text-inno-master').velocity({bottom:'-5%', opacity:0});
            jQuery('#text-inno-master-sm').velocity({opacity:0});
            
            // Hydrology In 
            jQuery('#inno-hydro').velocity({delay: 0, duration: 0},{ display:'block'});
            jQuery('#inno-hydro-left').velocity({top:0});
            jQuery('#inno-hydro-right').velocity({bottom:0});
            jQuery('#text-inno-hydro').velocity({top:'35%', opacity:1},{delay: 400, duration:500});
        } else {
            
            // Controller Section 2 In
            jQuery('#section-controller').velocity({top: '-100vh'});
            
            // Masterplanning Out            
            jQuery('#inno-master-left').velocity({top:'100%'});
            jQuery('#inno-master-right').velocity({bottom:'100%'});
            jQuery('#text-inno-master').velocity({bottom:'-5%', opacity:0});
            jQuery('#text-inno-master-sm').velocity({opacity:0});
            
            // Innovation & Creativitity In Reverse
            jQuery('.bg-front').velocity({top:'20%'});
            jQuery('#inno-inno-chart').velocity({opacity: 1}, {delay: 100, duration: 500});
            jQuery('#inno-home').velocity({delay: 500, duration: 0},{ display:'block'});
        }
        return false;
    });

    jQuery('.section4').on('DOMMouseScroll mousewheel', function (e) {
        if(e.originalEvent.detail > 0 || e.originalEvent.wheelDelta < 0) {
            jQuery('html, body').animate({
                scrollTop: $("footer").offset().top
            }, 600);
            // Controller Section 3 Out
//            jQuery('#section-controller').velocity({top:'-400vh'});
            
            // Masterplanning Out            
//            jQuery('#inno-master-left').velocity({top:'-100%'});
//            jQuery('#inno-master-right').velocity({bottom:'-100%'});
//            jQuery('#text-inno-master').velocity({bottom:'-5%', opacity:0});
//            jQuery('#text-inno-master-sm').velocity({opacity:0});
            
            // Hydrology In 
//            jQuery('#inno-hydro').velocity({delay: 0, duration: 0},{ display:'block'});
//            jQuery('#inno-hydro-left').velocity({top:0});
//            jQuery('#inno-hydro-right').velocity({bottom:0});
            
        } else {
            
            // Controller Section 2 In
            jQuery('#section-controller').velocity({top: '-200vh'});
            
            // Masterplanning Out            
            jQuery('#inno-master').velocity({delay: 0, duration: 0},{ display:'block'});
            jQuery('#inno-master-left').velocity({top:0});
            jQuery('#inno-master-right').velocity({bottom:0});
            jQuery('#text-inno-master').velocity({bottom:'-8%', opacity:1},{delay: 400, duration:500});
            jQuery('#text-inno-master-sm').velocity({opacity:1},{delay: 600, duration:300});
            
            // Hydrology In 
            jQuery('#inno-hydro').velocity({delay: 0, duration: 0},{ display:'none'});
            jQuery('#inno-hydro-left').velocity({top:'100%'});
            jQuery('#inno-hydro-right').velocity({bottom:'100%'});            
        }
        return false;
    });
            
});