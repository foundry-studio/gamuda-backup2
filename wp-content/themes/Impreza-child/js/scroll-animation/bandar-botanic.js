
//Scroll Animation
jQuery(document).ready(function() {

    
    jQuery('body').css('visibility','visible');

    // TimelineLite for title animation, then start up superscrollorama when complete
    (new TimelineLite({onComplete:initScrollAnimations}))
        .to( jQuery('#text-botanic-land-1'), .4, {delay: .5, css:{opacity:1}});
		
    
  
    
    function initScrollAnimations() {
        var controller = jQuery.superscrollorama();

        // set duration, in pixels scrolled, for pinned element
        var pinDur = 10000;

        // create animation timeline for pinned element
        controller.pin(jQuery('#botanic-scroll-anim'), pinDur, {
//            var pinAnimations = new TimelineLite();
//            pinAnimations
            anim: (new TimelineLite())    
            .append([
                TweenMax.to(jQuery('#text-botanic-land-1'), 2, {css:{top:'56%', opacity:0}}),
                TweenMax.to(jQuery('#text-botanic-land-1'), 2, {css:{top:'56%', opacity:0}}),
                TweenMax.to(jQuery('#botanic-land-slide-2-left'), 10, {css:{opacity:1}}),
                TweenMax.to(jQuery('#botanic-land-slide-2-right'), 10, {css:{opacity:1}}),
                TweenMax.to(jQuery('#text-botanic-land-2'), 10, {css:{top:'53%', opacity:1}})
            ], 10) 
            .append([
                TweenMax.to(jQuery('#text-botanic-land-2'), 2, {css:{top:'56%', opacity:0}}),
                TweenMax.to(jQuery('#botanic-land-slide-2-left'), 10, {css:{ top:'-100%'}}),
                TweenMax.to(jQuery('#botanic-land-slide-2-right'), 10, {css:{ bottom:'-100%'}}),
                TweenMax.to(jQuery('#botanic-overview-slide-left'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#botanic-overview-slide-right'), 10, {css:{bottom:0}}),
                TweenMax.to(jQuery('#bb-indicator-home'), 1, {css:{color:'#d7d7d7'}}),
                TweenMax.to(jQuery('#bb-indicator-home .indicator-circle'), 1, {css:{backgroundColor:'#d7d7d7'}}),
                TweenMax.to(jQuery('#bb-indicator-overview'), 1, {css:{color:'#ee2e24'}}),
                TweenMax.to(jQuery('#bb-indicator-overview .indicator-circle'), 1, {css:{backgroundColor:'#ee2e24'}})  
            ], 10)
            .append([
                TweenMax.to(jQuery('#text-botanic-overview'), 2, {css:{right:'19%', opacity:1}})
            ], 10)
            .append([
                TweenMax.to(jQuery('#text-botanic-overview'), 2, {css:{right:'19%', opacity:1}})
            ], 20)
            .append([
                TweenMax.to(jQuery('#text-botanic-overview'), 2, {css:{right:'23%', opacity:0}}),
                TweenMax.to(jQuery('#botanic-overview-slide-left'), 10, {css:{top:'-100%'}}),
                TweenMax.to(jQuery('#botanic-overview-slide-right'), 10, {css:{bottom:'-100%'}}),
                TweenMax.to(jQuery('#botanic-features-slide-1-left'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#botanic-features-slide-1-right'), 10, {css:{bottom:0}}),
                TweenMax.to(jQuery('#bb-indicator-overview'), 1, {css:{color:'#d7d7d7'}}),
                TweenMax.to(jQuery('#bb-indicator-overview .indicator-circle'), 1, {css:{backgroundColor:'#d7d7d7'}}),
                TweenMax.to(jQuery('#bb-indicator-features'), 1, {css:{color:'#ee2e24'}}),
                TweenMax.to(jQuery('#bb-indicator-features .indicator-circle'), 1, {css:{backgroundColor:'#ee2e24'}})  
            ], 10)
            .append([
                TweenMax.to(jQuery('#text-botanic-features'), 2, {css:{right:'50%', opacity:1}})
            ], 10)
            .append([
                TweenMax.to(jQuery('#text-botanic-features #tab1'), 4, {css:{fontSize:'10px', backgroundColor:'transparent', color:'#fff'}}),

                TweenMax.to(jQuery('#botanic-features-slide-1-left'), 4, {css:{opacity:0}}),
                TweenMax.to(jQuery('#botanic-features-slide-1-right'), 4, {css:{opacity:0}}),
                TweenMax.to(jQuery('#tab1-bg'), 4, {css:{opacity:0}}),
                TweenMax.to(jQuery('#text-botanic-features #tab2'), 4, {css:{fontSize:'30px', backgroundColor:'#fff', color:'#16827c'}}),
                TweenMax.to(jQuery('#botanic-features-slide-2'), 4, {css:{opacity:1}}),
                TweenMax.to(jQuery('#tab2-bg'), 4, {css:{opacity:1}}),
                TweenMax.to(jQuery('#text-botanic-features'), .5, {css:{width:'630px'}}),
                TweenMax.to(jQuery('#text-botanic-features #tab1-text'), .5, {css:{display:'none'}}),
                TweenMax.to(jQuery('#text-botanic-features #tab2-text'), 4, {css:{display:'block'}}),
                TweenMax.to(jQuery('#text-botanic-features #tab2-text'), 4, {css:{opacity:'1'}})
            ], 10)
            .append([
                TweenMax.to(jQuery('#text-botanic-features #tab2-text'), 4, {css:{display:'block'}})
            ], 10)
            .append([
                TweenMax.to(jQuery('#text-botanic-features #tab2'), 4, {css:{fontSize:'10px', backgroundColor:'transparent', color:'#fff'}}),
                TweenMax.to(jQuery('#botanic-features-slide-2'), 4, {css:{opacity:0}}),
                TweenMax.to(jQuery('#text-botanic-features #tab2-text'), .5, {css:{display:'none'}}),
                TweenMax.to(jQuery('#text-botanic-features #tab2-bg'), 4, {css:{opacity:0}}),
                TweenMax.to(jQuery('#text-botanic-features'), 4, {css:{padding:'45px 50px'}}),
                TweenMax.to(jQuery('#text-botanic-features #tab3'), 4, {css:{fontSize:'30px', backgroundColor:'#fff', color:'#16827c'}}),
                TweenMax.to(jQuery('#text-botanic-features #tab3-bg'), 4, {css:{opacity:1}}),
                TweenMax.to(jQuery('#botanic-features-slide-3-left'), 4, {css:{opacity:1}}),
                TweenMax.to(jQuery('#botanic-features-slide-3-right'), 4, {css:{opacity:1}}),
                TweenMax.to(jQuery('#text-botanic-features #tab3-text'), 4, {css:{display:'block'}})
            ], 10)
            .append([
                TweenMax.to(jQuery('#text-botanic-features'), 2, {css:{right:'53%', opacity:0}}),
                TweenMax.to(jQuery('#botanic-features-slide-3-left'), 4, {css:{top:'-100%'}}),
                TweenMax.to(jQuery('#botanic-features-slide-3-right'), 4, {css:{bottom:'-100%'}}),
                TweenMax.to(jQuery('#botanic-facilities-slide-left'), 4, {css:{top:0}}),
                TweenMax.to(jQuery('#botanic-facilities-slide-right'), 4, {css:{bottom:0}}),
                TweenMax.to(jQuery('#bb-indicator-features'), 1, {css:{color:'#d7d7d7'}}),
                TweenMax.to(jQuery('#bb-indicator-features .indicator-circle'), 1, {css:{backgroundColor:'#d7d7d7'}}),
                TweenMax.to(jQuery('#bb-indicator-facilities'), 1, {css:{color:'#ee2e24'}}),
                TweenMax.to(jQuery('#bb-indicator-facilities .indicator-circle'), 1, {css:{backgroundColor:'#ee2e24'}})
            ], 10)
            .append([
                TweenMax.to(jQuery('#text-botanic-facilities'), 2, {css:{right:'19%', opacity:1}})
            ], 10)
            .append([
                TweenMax.to(jQuery('#facilities-list'), 2, {css:{marginTop:'30px', opacity:1}})
            ], 10)
            .append([
                TweenMax.to(jQuery('#text-botanic-facilities'), 2, {css:{left:'47%', opacity:0}}),
                TweenMax.to(jQuery('#text-botanic-master-1'), 4, {css:{display:'block'}}),
                TweenMax.to(jQuery('#botanic-master-slide-1'), 4, {css:{transform:'scale(1, 1)', opacity:1}}),
                TweenMax.to(jQuery('#map-botanic-master-1'), 4, {css:{opacity:1}}),
                TweenMax.to(jQuery('#bb-indicator-facilities'), 1, {css:{color:'#d7d7d7'}}),
                TweenMax.to(jQuery('#bb-indicator-facilities .indicator-circle'), 1, {css:{backgroundColor:'#d7d7d7'}}),
                TweenMax.to(jQuery('#bb-indicator-master'), 1, {css:{color:'#ee2e24'}}),
                TweenMax.to(jQuery('#bb-indicator-master .indicator-circle'), 1, {css:{backgroundColor:'#ee2e24'}})
            ], 10)
            .append([
                TweenMax.to(jQuery('#text-botanic-master-1'), 4, {css:{left:0, right:0, opacity:1}})
            ], 10)
            .append([
                TweenMax.to(jQuery('#text-botanic-master-1 p'), 4, {css:{opacity:1}}),
                TweenMax.to(jQuery('#map-botanic-master-1 img'), 4, {css:{transform:'scale(1,1)', opacity:1}})
            ], 10)
            .append([
                TweenMax.to(jQuery('#botanic-master-slide-1'), 10, {css:{backgroundPosition:'bottom'}}),
                TweenMax.to(jQuery('#text-botanic-master-1'), 4, {css:{opacity:0}}),
                TweenMax.to(jQuery('#map-botanic-master-1 img'), 4, {css:{opacity:0}})
            ], 10)
            .append([
                TweenMax.to(jQuery('#botanic-master-slide-2'), 10, {css:{height:'100vh'}}),
                TweenMax.to(jQuery('#text-botanic-master-2-left'), 8, {css:{left:0, opacity:1}}),
                TweenMax.to(jQuery('#text-botanic-master-2-right'), 10, {css:{right:0, opacity:1}})
            ], 10)
            .append([
                TweenMax.to(jQuery('#botanic-master-slide-3-left'), 10, {css:{opacity:1}}),
                TweenMax.to(jQuery('#botanic-master-slide-3-right'), 10, {css:{opacity:1}}),
                TweenMax.to(jQuery('#text-botanic-master-3'), 10, {css:{opacity:1, top:'30px'}}),
                TweenMax.to(jQuery('#text-botanic-master-3-bottom'), 10, {css:{opacity:1}}),
                TweenMax.to(jQuery('#map-botanic-master-1'), 8, {css:{opacity:0}}),
                TweenMax.to(jQuery('#text-botanic-master-2-left'), 8, {css:{left:'-100%', opacity:0}}),
                TweenMax.to(jQuery('#text-botanic-master-2-right'), 10, {css:{right:'-100%', opacity:0}})
            ], 10)
            .append([
                TweenMax.to(jQuery('#botanic-master-slide-2'), 10, {css:{display:'none'}}),
                TweenMax.to(jQuery('#text-botanic-master-2-left'), 8, {css:{display:'none'}}),
                TweenMax.to(jQuery('#text-botanic-master-2-right'), 10, {css:{display:'none'}})
            ], 10)
            .append([
                TweenMax.to(jQuery('#botanic-master-slide-3-left'), 10, {css:{top:'-100%'}}),
                TweenMax.to(jQuery('#botanic-master-slide-3-right'), 10, {css:{bottom:'-100%'}}),
                TweenMax.to(jQuery('#text-botanic-master-3'), 10, {css:{opacity:0, top:'60px'}}),
                TweenMax.to(jQuery('#botanic-master-slide-4-left'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#botanic-master-slide-4-right'), 10, {css:{bottom:0}})
            ], 10)
            .append([
                TweenMax.to(jQuery('#text-botanic-master-4'), 10, {css:{opacity:1, top:'50px'}}),
                TweenMax.to(jQuery('#text-botanic-master-4-bottom'), 10, {css:{opacity:1}})
            ], 10)
            .append([
                TweenMax.to(jQuery('#botanic-master-slide-3'), 10, {css:{display:'none'}}),
                TweenMax.to(jQuery('#text-botanic-master-3-left'), 8, {css:{display:'none'}}),
                TweenMax.to(jQuery('#text-botanic-master-3-right'), 10, {css:{display:'none'}}),
                TweenMax.to(jQuery('#text-botanic-master-4'), 5, {css:{opacity:0, top:'-50%'}}),
                TweenMax.to(jQuery('#botanic-master-slide-4-left'), 10, {css:{top:'-100%'}}),
                TweenMax.to(jQuery('#botanic-master-slide-4-right'), 10, {css:{bottom:'100%'}}),
                TweenMax.to(jQuery('#slide-bb-happenings'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#bb-indicator-master'), 1, {css:{color:'#d7d7d7'}}),
                TweenMax.to(jQuery('#bb-indicator-master .indicator-circle'), 1, {css:{backgroundColor:'#d7d7d7'}}),
                TweenMax.to(jQuery('#bb-indicator-happenings'), 1, {css:{color:'#ee2e24'}}),
                TweenMax.to(jQuery('#bb-indicator-happenings .indicator-circle'), 1, {css:{backgroundColor:'#ee2e24'}})
            ], 10)
            .append([
                TweenMax.to(jQuery('#slide-bb-happenings'), 10, {css:{top:'-100%'}}),
				TweenMax.to(jQuery('#slide-bb-other-dev'), 10, {css:{top:0}}),
                TweenMax.to(jQuery('#bb-indicator-happenings'), 1, {css:{color:'#d7d7d7'}}),
                TweenMax.to(jQuery('#bb-indicator-happenings .indicator-circle'), 1, {css:{backgroundColor:'#d7d7d7'}}),
                TweenMax.to(jQuery('#bb-indicator-more'), 1, {css:{color:'#ee2e24'}}),
                TweenMax.to(jQuery('#bb-indicator-more .indicator-circle'), 1, {css:{backgroundColor:'#ee2e24'}})
            ])
        })


    }
});