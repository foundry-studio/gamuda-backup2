<?php
// Template Name:  Bandar Botanic Mobile Template 
get_header(); ?>

<div class="mobile-main-nav mobile-main-nav-all">
    <div class="mobile-main-nav-header">
        <a class="mobile-main-nav-logo" href="http://128.199.89.40/"><img src="http://128.199.89.40/wp-content/uploads/2016/07/logo-1.png" alt="Gamuda Land Logo"></a>
		<div class="mobile-main-nav-close"><img src="/wp-content/uploads/2016/09/nav-close-btn.png"></div>
    </div>
    <ul class="list-unstyled">
        <li><a href="http://128.199.89.40/about-us/">ABOUT US</a></li>
        <li class="mobile-sub-nav-open">
            <a href="http://128.199.89.40/our-developments/">OUR DEVELOPMENTS</a>
            <i class="fa fa-angle-right" aria-hidden="true"></i>
        </li>
        <li><a href="http://128.199.89.40/happenings/">HAPPENINGS</a></li>
        <li><a href="http://128.199.89.40/contact-us/">CONTACT US</a></li>
    </ul>
    <a class="mobile-main-nav-register" href="https://webreg.gamudaland.com.my/contact/_publicwebregistration/" target="_blank">REGISTER</a>
    <div class="mobile-main-nav-sns">
        <a href="https://www.facebook.com/GamudaLand/?fref=ts&ref=br_tf"><i class="fa fa-facebook" aria-hidden="true"></i></a>
        <a href="https://www.instagram.com/gamudaland/"><i class="fa fa-instagram" aria-hidden="true"></i></a>
    </div>
</div>

<div class="mobile-main-nav mobile-main-nav-projects">

    <div class="mobile-main-nav-header">
        <a class="mobile-main-nav-logo" href="http://128.199.89.40/"><img src="http://128.199.89.40/wp-content/uploads/2016/07/logo-1.png" alt="Gamuda Land Logo"></a>
		<div class="mobile-main-nav-close"><img src="/wp-content/uploads/2016/09/nav-close-btn.png"></div>
    </div>
    <div class="mobile-main-nav-projects-list">
        <ul class="list-unstyled">
            <li class="mobile-sub-nav-close">
                <b>FEATURED</b> DEVELOPMENTS
                <i class="fa fa-angle-left" aria-hidden="true"></i>
            </li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/highpark-suites/">HIGHPARK SUITES<span>Kelana Jaya</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/the-robertson/">THE ROBERTSON<span>Kuala Lumpur</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/jade-hills/">JADE HILLS<span>Kajang</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/horizon-hills/">HORIZON HILLS<span>Iskandar Puteri</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/bukit-bantayan/">BUKIT BANTAYAN<span>Kota Kinabalu</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/gamuda-walk/">GAMUDA WALK<span>Shah Alam</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/gem-residences/">GEM RESIDENCES<span>Toa Payoh</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/661-chapel-st/">661 CHAPEL ST.<span>Melbourne</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/gamuda-city/">GAMUDA CITY<span>Hanoi</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/celadon-city/">CELADON CITY<span>Ho Chi Minh</span></a></li>
        </ul>
        <ul class="list-unstyled">
            <li><b>UPCOMING</b> DEVELOPMENTS</li>
            <li><a href="http://gamudaland.com.my/kundangestates/">KUNDANG ESTATES<span>Kuang</span></a></li>
            <li><a href="#" class="navi-disable">GAMUDA GARDENS<span>Kuang</span></a></li>
            <li><a href="http://gamudaland.com.my/twentyfive7/">TWENTYFIVE7<span>Kota Kemuning</span></a></li>
        </ul>
        <ul class="list-unstyled">
            <li><b>FULLY SOLD</b> DEVELOPMENTS</li>
            <li><a href="http://128.199.89.40/our-developments/fully-sold-developments/kota-kemuning/">KOTA KEMUNING<span>Shah Alam</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/fully-sold-developments/bandar-botanic/">BANDAR BOTANIC<span>Klang</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/fully-sold-developments/valencia/">VALENCIA<span>Sungai Buloh</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/fully-sold-developments/madge-mansions/">MADGE MANSIONS<span>Kuala Lumpur</span></a></li>
        </ul>
        <ul class="list-unstyled">
            <li><b>GOLF &amp; COUNTRY</b> CLUBS</li>
            <li><a href="http://www.kotapermai.com.my/" target="_blank">KOTA PERMAI<span>Golf &amp; Country Club</span></a></li>
            <li><a href="http://hhgcc.com.my/" target="_blank">HORIZON HILLS<span>Golf &amp; Country Club</span></a></li>
            <li><a href="http://www.botanicresortclub.com.my/" target="_blank">BOTANIC<span>Resort Club</span></a></li>
        </ul>
    </div>
</div>

<div id="mobile-pages" class="mobile-slide-page bandar-botanic mobile-page">
    <div class="container-fluid document">
        <div class="row mobile-cover mobile-bandar-botanic-main-cover">
            <div class="mobile-cover-content">
                <img class="mobile-main-logo" src="/wp-content/themes/Impreza-child/img/bandar-botanic-logo.png" alt="Bandar Botanic Logo">
                <hr class="short-separator">
                <h2 class="md-font">WHERE TRANQUILITY<br>SURROUNDS YOU</h2>
            </div>
			<div class="mouse">
				<div class="mouse-icon">
					<span class="mouse-wheel"></span>
				</div>
			</div>
            <div class="mobile-cover-bg mobile-cover-bg-main1">
                <!--<img src="/wp-content/themes/Impreza-child/img/bandar-botanic-main-cover.jpg" alt="Mobile Cover Image">-->
            </div>
        </div>

        <div class="row mobile-cover">
            <div class="mobile-cover-content">
                <div class="mobile-bandar-botanic-cover col-sm-6 col-xs-8">
                    <div class="mobile-bandar-botanic-cover-title">
                        <h2 class="md-font">ELEGANCE WITHIN<br>A SCENIC TOWNSHIP</h2>
                    </div>
                    <p class="sm-font mobile-bandar-botanic-cover-text">One of the most sought-after developments in the Klang Valley, Bandar Botanic offers you a holistic lifestyle with its meticulously planned "Home in A Garden' concept and host of amenities. Having set the benchmark for developments of its kind, this premier integrated self-sustaining township continues to receive accolades for sustainable value appreciation year after year.</p>
                </div>
            </div>
            <div class="mobile-cover-bg mobile-cover-bg-main2">
                <!--<img src="/wp-content/themes/Impreza-child/img/bandar-botanic-cover.jpg" alt="Bandar Botanic Overview Background Image">-->
            </div>
        </div>

        <div class="row bandar-botanic-overview">
            <h2 class="md-font">OVERVIEW</h2>
            <div class="sm-font bandar-botanic-overview-box">
                <p>A multi-year winner of the Edge-PEPS Value Creation Excellence Award, Bandar Botanic continues to thrive as a premier integrated self-sustaining township. Conceived in the year 2000, this development pioneered the concept of bringing the outdoors, indoors by weaving nature seamlessly into the everyday lives of its community.</p>
                <p>Encompassing comprehensive retail and commercial services as well as community amenities, this township has become has become the new benchmark for property developments in the Klang Valley. Built with nature in mind and seeking to make a difference in the way people live, Bandar Botanic offers a harmonious, sustainable environment that will be enjoyed for generations to come.</p>
            </div>
        </div>

        <div class="row single-image bandar-botanic-overview-img">
            <img src="/wp-content/themes/Impreza-child/img/bandar-botanic-overview-img.jpg" alt="Bandar Botanic Image">
        </div>

        <div class="row mobile-tab-group">
            <ul class="xs-font list-unstyled list-inline">
                <li class="active" data-num="1">FEATURES</li>
                <li data-num="2">BOTANIC WEST</li>
                <li data-num="3">BOTANIC EAST</li>
            </ul>
            <div class="sm-font mobile-tab mobile-tab1">
                <p>The appeal of Bandar Botanic lies in its rich, tropical landscape. Conceived as "A Home in A Garden", an emphasis has been placed on creating a lush green environment for the community to enjoy the serene beauty of nature from the comfort of their homes.</p>
                <p>To this end, a significant portion of the development has been dedicated to green lungs, parks, lakes and open spaces, all with the aim of encouraging healthy and active lifestyles as well as harmonious community living.</p>
                <p>The Botanic West and Botanic East precincts within Bandar Botanic have been meticulously planned. Each embody their own distinct characteristics, parkland environment, amenities and facilities to provide residents a holistic lifestyle.</p>
                <p>In 2004, Bandar Botanic’s 2-km signature Central Lake and Park, the community’s main recreational hub, was awarded the prestigious Malaysia National Landscape Award 2004 under the City Garden category.</p>
            </div>
            <div class="sm-font mobile-tab mobile-tab2">
                <p>Botanic West is Bandar Botanic's very own 70-acre botanical parkland area encompassing a wide expanse of verdant greens, tree-lined boulevards and thematic gardens. It is this botanical parkland that gives the township its identity and provides the community with an avenue for rest, recreation and community interaction.</p>
                <p>Every detail of Botanic West's Central Lake and Park was purposefully planned to give the township a distinct botanical parkland character. The 2-km Central Lake and Park is laden with luxurious thematic gardens that include a topiary garden, sundial garden, alphabet garden, children’s island and culinary garden as well as exciting water cascades beautifully landscaped for the community to enjoy. Other features include a flower bridge, flower tunnel, amphitheatre, waterwheel, gazebos and floating boardwalk.</p>
            </div>
            <div class="sm-font mobile-tab mobile-tab3">
                <p>With its offer of a 100-acre waterfront-parkland setting and its resort-styled ambience, residents of Botanic East get to enjoy carefree evenings relishing the breathtaking views of pristine green surroundings and waterways.</p>
                <p>Lush greenery and themed features are aplenty at Botanic East. With names such as Children's Island, Rainbow Lake, Floating Garden, Meandering Creek, Pocket Garden, Ribbon Lake, Crescent Lake and Hanami Garden, each themed feature adds its own distinct flavour to make this tranquil enclave a unique neighbourhood.</p>
            </div>
        </div>

        <div class="row mobile-bandar-botanic-facility">
            <h2 class="md-font">FACILITIES &amp; AMENITIES</h2>
            <div class="sm-font mobile-bandar-botanic-facility-text">
                <p>Bandar Botanic was built to meet the needs of the community and everything residents need is at their doorstep.</p>
                <p>Residents have easy access to the largest Southeast Asian wholesale city - GM Klang, Aeon Mall, Giant and Tesco, plus a host of other retail and commercial facilities that dot the neighbourhood. Public transportation is readily available to take residents straight to urban centres like Klang and back.</p>
                <p>The Central Botanic Parkland has been designed as a social and recreational hub for the community and is situated within walking distance. On top of this, amenities such as schools, kindergartens, community halls, places of worship, local shops and bus stops are inter-connected with the parkland via series of "green fingers" to reduce pedestrian-vehicular conflict and to promote community living.</p>
				<p>In short, everything that is fundamental to enhancing a community’s lifestyle has been thought of.</p>
            </div>
        </div>

        <div class="row mobile-bandar-botanic-site-plan-wrap">
            <div class="mobile-bandar-botanic-site-plan1">
                <div class="mobile-bandar-botanic-site-plan-content1">
                    <h2 class="md-font">MASTER SITE PLAN</h2>
                    <img src="/wp-content/uploads/2016/09/bandar-botanic-master1-map.png" alt="Bandar Botanic Master Site Plan Image">
                </div>
                <div class="mobile-bandar-botanic-site-plan-img1 ">
                    <img src="/wp-content/themes/Impreza-child/img/bandar-botanic-site-plan-img1.jpg" alt="Background-Image">
                </div>
            </div>
            <div class="mobile-bandar-botanic-site-plan-box1">
                <div class="sm-font mobile-bandar-botanic-site-plan-box-content">
                    <p>Bandar Botanic was conceived as a waterfront-parkland where the endless meandering lakes and parks would give the township a botanical feel.</p>
                    <p>To achieve this concept, Gamuda Land’s team of creative and innovative engineers and architects began at the foundation by working on a meticulous and well-thought-out masterplan. They placed a major emphasis on providing a green and healthy environment for the community. To this end, more than 180 acres of the township were dedicated to greens, parks and lakes to provide the community a holistic, quality lifestyle.</p>
                    <p>The plan and design for the township also incorporated a multitude of value-added features such as superior infrastructure, an abundance of parks and greenery, as well as a host of other community-centric facilities and amenities.</p>
                </div>
            </div>
            <div class="mobile-bandar-botanic-site-plan-img2">
                <img src="/wp-content/themes/Impreza-child/img/bandar-botanic-site-plan-img2.jpg" alt="Bandar Botanic Image">
            </div>
            <div class="mobile-bandar-botanic-site-plan-box2">
                <div class="sm-font mobile-bandar-botanic-site-plan-box-content">
                    <p>To underscore Gamuda Land’s commitment to providing ready connectivity to Bandar Botanic, we constructed a RM30 million dedicated interchange to link the township directly to the Shah Alam Expressway.</p>
                    <p>In line with our efforts to uphold good environmental architecture practices, we brought several innovative solutions into play. Bandar Botanic was the first development to not only comply with, but also exceed the mandatory requirements of the Department of Irrigation and Drainage’s Urban Stormwater Management System (MASMA). Leveraging on a novel Floating Pile Raft Foundation System, we transformed the basic irrigation network within the development into an extensively landscaped park that elevated the standard of living in the area as well as the value of the surrounding properties.</p>
                    <p>Today the Central Lake Park functions as a detention and regulating pond for surface water run-off, while its themed parks and gardens as well as leisure activity areas are well utilised by the community.</p>
                </div>
            </div>
            <div class="mobile-bandar-botanic-site-plan-img3">
                <img src="/wp-content/themes/Impreza-child/img/bandar-botanic-site-plan-img3.jpg" alt="Bandar Botanic Image">
            </div>
            <div class="mobile-bandar-botanic-site-plan-box3">
                <div class="sm-font mobile-bandar-botanic-site-plan-box-content">
                    <p>Botanic West features a unique 70-acre botanical parkland with a 2-km lake. Homes here are designed to look over the Central Lake and are surrounded by beautifully landscaped pocket parks that truly add to the tranquility and privacy of the botanical haven.</p>
                    <p>Botanic East offers unique waterfront parkland with lush landscaping, private gardens and green lungs in a botanical environment.</p>
                    <p>The waterfront with its lavish parks and gardens provide a serene and tranquil ambience to this enclave of bungalow homes, semi-detached homes and garden terraces.</p>
                </div>
            </div>
            <div class="mobile-bandar-botanic-site-plan-img4">
                <img src="/wp-content/themes/Impreza-child/img/bandar-botanic-site-plan-img4.jpg" alt="Bandar Botanic Image">
            </div>
        </div>

        <div class="row mobile-bandar-botanic-residential-plan">
            <h2 class="md-font">MASTER SITE PLAN-<span>RESIDENTIAL</span></h2>
            <img src="/wp-content/themes/Impreza-child/img/bandar-botanic-residential-plan.png" alt="Bandar Botanic Master Site Plan-Residential Image">
        </div>

        <div class="row mobile-bandar-botanic-residential-plan-wrap">
            <div class="sm-font mobile-bandar-botanic-residential-plan-text">
                <p>Each precinct is characterised by its natural setting which gives it a distinctive feature. Each of the homes, regardless of whether it is a luxury bungalow or garden terrace, is designed with novel features and exciting architectural designs that enhances its uniqueness.</p>
                <p>The homes, which are designed and planned to benefit from the aesthetics of the natural world, offer close affinity to the surrounding greenery, landscaping and water features. Liberal features such as expansive terraces, spacious courtyards and tall windows allow greater freedom for each home to fuse with its surroundings. To ensure residents enjoy optimum views of the waterways and expansive greens, homes are designed with water frontages or garden frontages.</p>
                <p>To ensure maximum benefit to the community, many of the development’s finer details have been meticulously planned. These include features such as underground utilities, roll-over kerbs, pedestrian-safe walkways and an efficient road and traffic management systems. Another effective neighbourhood feature to enhance the security and privacy of residents is the cul-de-sac road that ensures only one entry and exit point to homes.</p>
            </div>
        </div>

        <div class="row single-image">
            <img src="/wp-content/themes/Impreza-child/img/bandar-botanic-site-plan-img5.jpg" alt="Bandar Botanic Image">
        </div>

        <div class="row mobile-bandar-botanic-commercial-plan">
            <h2 class="md-font">MASTER SITE PLAN-<span>COMMERCIAL</span></h2>
            <img src="/wp-content/themes/Impreza-child/img/bandar-botanic-commercial-plan.png" alt="Bandar Botanic Master Site Plan-Commercial Image">
        </div>

        <div class="row mobile-bandar-botanic-commercial-plan-wrap">
            <div class="sm-font mobile-bandar-botanic-commercial-plan-text">
                <p>The community at Bandar Botanic have ready access to a bevy of everyday conveniences. These include ample shopping, dining and entertainment centres such as Aeon Mall, Tesco, Giant and the GM Klang Wholesale City.</p>
                <p>Within its boundaries, Bandar Botanic also offers exclusive and exciting shopping and business experiences at the Botanic Walk Lakefront Commercial Centre, Botanic Capital, Botanic Avenue and Botanic Business Gateway.</p>
                <p>The commercial centre comprising Botanic Walk East and West is designed as a community venue for families to enjoy the pleasures of life be it by way of recreational activities, meeting friends and family, or just relaxing and enjoying a good meal. Strategically located alongside Central Lake Park, the centre provides residents a unique shopping experience.</p>
				<p>Meanwhile, the commercial hub comprising Botanic Capital, Botanic Avenue and Botanic Business Gateway encompasses two, three and four-storey shop offices. These house a shopping mall, boutique shops, retail shop offices, an automotive showroom and corporate offices, among others.</p>
            </div>
        </div>
		<div class="row">
			<div class="mobile-other-projects">
				<h2 class="md-font">OTHER DEVELOPMENTS</h2>
				<div class="col-xs-6 mobile-prev-project">
					<a href="http://128.199.89.40/our-developments/fully-sold-developments/kota-kemuning/"><img src="/wp-content/uploads/2016/09/m-other-dev-banner-kota-kemuning.jpg" alt="Kota Kemuning"></a>
				</div>
				<div class="col-xs-6 mobile-next-project">
					<a href="http://128.199.89.40/our-developments/fully-sold-developments/valencia/"><img src="/wp-content/uploads/2016/09/m-other-dev-banner-valencia.jpg" alt="Valencia"></a>
				</div>
			</div>
		</div>
    </div>
</div>

<?php get_footer();