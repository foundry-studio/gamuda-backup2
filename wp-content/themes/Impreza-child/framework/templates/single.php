<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );
/**
 * The template for displaying pages
 */
$us_layout = US_Layout::instance();
get_header();
us_load_template( 'templates/titlebar' );
?>

<div class="mobile-main-nav mobile-main-nav-all">
    <div class="mobile-main-nav-header">
        <a class="mobile-main-nav-logo" href="http://128.199.89.40/"><img src="http://128.199.89.40/wp-content/uploads/2016/07/logo-1.png" alt="Gamuda Land Logo"></a>
		<div class="mobile-main-nav-close"><img src="/wp-content/uploads/2016/09/nav-close-btn.png"></div>
    </div>
    <ul class="list-unstyled">
        <li><a href="http://128.199.89.40/about-us/">ABOUT US</a></li>
        <li class="mobile-sub-nav-open">
            <a href="http://128.199.89.40/our-developments/">OUR DEVELOPMENTS</a>
            <i class="fa fa-angle-right" aria-hidden="true"></i>
        </li>
        <li><a href="http://128.199.89.40/happenings/">HAPPENINGS</a></li>
        <li><a href="http://128.199.89.40/contact-us/">CONTACT US</a></li>
    </ul>
    <a class="mobile-main-nav-register" href="https://webreg.gamudaland.com.my/contact/_publicwebregistration/" target="_blank">REGISTER</a>
    <div class="mobile-main-nav-sns">
        <a href="https://www.facebook.com/GamudaLand/?fref=ts&ref=br_tf"><i class="fa fa-facebook" aria-hidden="true"></i></a>
        <a href="https://www.instagram.com/gamudaland/"><i class="fa fa-instagram" aria-hidden="true"></i></a>
    </div>
</div>

<div class="mobile-main-nav mobile-main-nav-projects">

    <div class="mobile-main-nav-header">
        <a class="mobile-main-nav-logo" href="http://128.199.89.40/"><img src="http://128.199.89.40/wp-content/uploads/2016/07/logo-1.png" alt="Gamuda Land Logo"></a>
		<div class="mobile-main-nav-close"><img src="/wp-content/uploads/2016/09/nav-close-btn.png"></div>
    </div>
    <div class="mobile-main-nav-projects-list">
        <ul class="list-unstyled">
            <li class="mobile-sub-nav-close">
                <b>FEATURED</b> DEVELOPMENTS
                <i class="fa fa-angle-left" aria-hidden="true"></i>
            </li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/highpark-suites/">HIGHPARK SUITES<span>Kelana Jaya</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/the-robertson/">THE ROBERTSON<span>Kuala Lumpur</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/jade-hills/">JADE HILLS<span>Kajang</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/horizon-hills/">HORIZON HILLS<span>Iskandar Puteri</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/bukit-bantayan/">BUKIT BANTAYAN<span>Kota Kinabalu</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/gamuda-walk/">GAMUDA WALK<span>Shah Alam</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/gem-residences/">GEM RESIDENCES<span>Toa Payoh</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/661-chapel-st/">661 CHAPEL ST.<span>Melbourne</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/gamuda-city/">GAMUDA CITY<span>Hanoi</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/celadon-city/">CELADON CITY<span>Ho Chi Minh</span></a></li>
        </ul>
        <ul class="list-unstyled">
            <li><b>UPCOMING</b> DEVELOPMENTS</li>
            <li><a href="http://gamudaland.com.my/kundangestates/">KUNDANG ESTATES<span>Kundang</span></a></li>
            <li><a href="#" class="navi-disable">GAMUDA GARDENS<span>Kuang</span></a></li>
            <li><a href="http://gamudaland.com.my/twentyfive7/">TWENTYFIVE7<span>Kota Kemuning</span></a></li>
        </ul>
        <ul class="list-unstyled">
            <li><b>FULLY SOLD</b> DEVELOPMENTS</li>
            <li><a href="#">KOTA KEMUNING<span>Shah Alam</span></a></li>
            <li><a href="#">BANDAR BOTANIC<span>Klang</span></a></li>
            <li><a href="#">VALENCIA<span>Sungai Buloh</span></a></li>
            <li><a href="#">MADGE MANSIONS<span>Kuala Lumpur</span></a></li>
        </ul>
        <ul class="list-unstyled">
            <li><b>GOLF &amp; COUNTRY</b> CLUBS</li>
            <li><a href="http://www.kotapermai.com.my/" target="_blank">KOTA PERMAI<span>Golf &amp; Country Club</span></a></li>
            <li><a href="http://hhgcc.com.my/" target="_blank">HORIZON HILLS<span>Golf &amp; Country Club</span></a></li>
            <li><a href="http://www.botanicresortclub.com.my/" target="_blank">BOTANIC<span>Resort Club</span></a></li>
        </ul>
    </div>
</div>
<!-- MAIN -->
<div class="l-main">
	<div class="l-main-h i-cf">

		<main class="l-content" itemprop="mainContentOfPage">
			<?php do_action( 'us_before_page' ) ?>

			<?php
			while ( have_posts() ) {
				the_post();

				$the_content = apply_filters( 'the_content', get_the_content() );

				// The page may be paginated itself via <!--nextpage--> tags
				$pagination = us_wp_link_pages( array(
					'before' => '<div class="w-blog-pagination"><nav class="navigation pagination">',
					'after' => '</nav></div>',
					'next_or_number' => 'next_and_number',
					'nextpagelink' => '>',
					'previouspagelink' => '<',
					'link_before' => '<span>',
					'link_after' => '</span>',
					'echo' => 0
				) );

				// If content has no sections, we'll create them manually
				$has_own_sections = ( strpos( $the_content, ' class="l-section' ) !== FALSE );
				if ( ! $has_own_sections ) {
					$the_content = '<section class="l-section"><div class="l-section-h i-cf">' . $the_content . $pagination . '</div></section>';
				} elseif ( ! empty( $pagination ) ) {
					$the_content .= '<section class="l-section"><div class="l-section-h i-cf">' . $pagination . '</div></section>';
				}

				echo $the_content;

				// Post comments
				/*if ( comments_open() OR get_comments_number() != '0' ) {
					// Hotfix for events calendar plugin
					if ( ! is_post_type_archive( 'tribe_events' ) ) {
					?><section class="l-section for_comments">
						<div class="l-section-h i-cf"><?php
							wp_enqueue_script( 'comment-reply' );
							comments_template();
						?></div>
					</section><?php
					}
				}*/
			}
			?>

			<?php do_action( 'us_after_page' ) ?>

		</main>

		<?php /*if ( $us_layout->sidebar_pos == 'left' OR $us_layout->sidebar_pos == 'right' ): ?>
			<aside class="l-sidebar at_<?php echo $us_layout->sidebar_pos ?><?php echo generated_dynamic_sidebar_class() ?>" itemscope="itemscope" itemtype="https://schema.org/WPSideBar">
				<?php generated_dynamic_sidebar(); ?>
			</aside>
		<?php endif;*/ ?>

	</div>
</div>

<?php get_footer() ?>
