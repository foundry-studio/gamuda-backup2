<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

$us_layout = US_Layout::instance();

if ( $us_layout->header_show == 'never' ) {
	return;
}

global $us_header_settings;
us_load_header_settings_once();

$options = us_arr_path( $us_header_settings, 'default.options', array() );
$layout = us_arr_path( $us_header_settings, 'default.layout', array() );
$data = us_arr_path( $us_header_settings, 'data', array() );
$pathtemplate =get_stylesheet_directory_uri();

echo '<header class="l-header ' . $us_layout->header_classes();
if ( isset( $options['bg_img'] ) AND ! empty( $options['bg_img'] ) ) {
	echo ' with_bgimg';
}
echo '" itemscope="itemscope" itemtype="https://schema.org/WPHeader">';
foreach ( array( 'top', 'middle', 'bottom' ) as $valign ) {
	echo '<div class="l-subheader at_' . $valign;
	if ( isset( $options[ $valign . '_fullwidth' ] ) AND $options[ $valign . '_fullwidth' ] ) {
		echo ' width_full';
	}
	echo '"><div class="l-subheader-h">';
	foreach ( array( 'left', 'center', 'right' ) as $halign ) {
		echo '<div class="l-subheader-cell at_' . $halign . '">';
		if ( isset( $layout[ $valign . '_' . $halign ] ) ) {
			us_output_header_elms( $layout, $data, $valign . '_' . $halign );
		}
		echo '</div>';
	}
	    echo '
    
    
    <div class="collapse navbar-collapse js-navbar-collapse">
        <form role="search" method="post" id="searchform" action="'.esc_url(home_url( "/" )).'">
        
        <ul class="nav navbar-nav">
            <li class="dropdown dropdown-large">
                <a href="#" class="dropdown-toggle searchdropdown pull-left" data-toggle="dropdown">FIND A <b>PROPERTY</b> </a><a href="#" class="dropdown-toggle searchdropdowncaret pull-left" data-toggle="dropdown">
                &nbsp;</a>
                
                <ul class="dropdown-menu dropdown-menu-large row">
				
                    <li class="col-sm-12" style="margin-bottom: 30px;">
                        <div class="row">
                        <div class="col-sm-10">
                            <div class=" ">
                                <input type="text" class="form-control" placeholder="Search Location, property type or development" name="s" id="s"/>
                            </div>
                            
                        
                        </div>
                        <div class="col-sm-2">
                        <div class="row right-pad-15">
                            <input type="submit" name="reg_submit" title="" value="Search" id="submit-button" class="btn btn-red pull-right btn-lg">
                        </div>
                        </div>
                        </div>
                        </li>
                    <li class="col-sm-3 border-right" >
                        <ul>
                            <li class="dropdown-header">LOCATION</li>
                            
							<li><label class="checkboxfake"><input id="locationall"  type="checkbox" value="" onClick="check_all_location(this.checked);"><span class="fake"></span>All</label> </li>
                            <li><label class="checkboxfake"><input class="locationcheckbox" id="" name="location[]"  data-id="" onclick="" type="checkbox" value="Kuala Lumpur"><span class="fake"></span>Kuala Lumpur</label></li>
                            <li><label class="checkboxfake"><input class="locationcheckbox" id="" name="location[]"  data-id="" onclick="" type="checkbox" value="Selangor"><span class="fake"></span>Selangor</label></li>
                            <li><label class="checkboxfake"><input class="locationcheckbox" id="" name="location[]"  data-id="" onclick="" type="checkbox" value="Sabah"><span class="fake"></span>Sabah</label></li>
                            <li><label class="checkboxfake"><input class="locationcheckbox" id="" name="location[]"  data-id="" onclick="" type="checkbox" value="Johor"><span class="fake"></span>Johor</label></li>
							<li> &nbsp; </li>
                                   
                                        
                        </ul>
                    </li>
                    <li class="col-sm-3 border-right">
                        <ul>
                            <li class="dropdown-header">PROPERTY TYPE</li>
                           
							<li><label class="checkboxfake"><input id="propertytype"   type="checkbox" onClick="check_all_propertytype(this.checked);"><span class="fake" value=""></span>All</label> </li>
							
                            <li><label class="checkboxfake"><input class="propertytypecheckbox" id="" name="propertytype[]" data-id="" onclick="" type="checkbox" value="Terrace Homes"><span class="fake"></span>Terrace Homes</label></li>
							<li><label class="checkboxfake"><input class="propertytypecheckbox" id="" name="propertytype[]" data-id="" onclick="" type="checkbox" value="Semi-D Homes"><span class="fake"></span>Semi-D Homes</label></li>
							<li><label class="checkboxfake"><input class="propertytypecheckbox" id="" name="propertytype[]" data-id="" onclick="" type="checkbox" value="Bungalows"><span class="fake"></span>Bungalows</label></li>
							<li><label class="checkboxfake"><input class="propertytypecheckbox" id="" name="propertytype[]" data-id="" onclick="" type="checkbox" value="Condominium"><span class="fake"></span>Condominium</label></li>
							<li><label class="checkboxfake"><input class="propertytypecheckbox" id="" name="propertytype[]" data-id="" onclick="" type="checkbox" value="Serviced Apartments"><span class="fake"></span>Serviced Apartments</label></li>
                         
                            
                           
                            
                            
                        </ul>
                    </li>
                  
                
                    <li class="col-sm-3 border-right">
                        <ul>
                            <li class="dropdown-header">Development Phase</li>
                            
							<li><label class="checkboxfake"><input id="phase" onClick="check_all_phase(this.checked);" type="checkbox" value=""><span class="fake"></span>All</label> </li>
							<li><label class="checkboxfake"><input  class="phasecheckbox" id="" name="phase[]"  data-id="" onclick="" type="checkbox" value="On-going"><span class="fake"></span>New Launches</label></li>';
							echo '<li><label class="checkboxfake"><input  class="phasecheckbox" id="" name="phase[]"  data-id="" onclick="" type="checkbox" value="Completed"><span class="fake"></span>Completed</label></li>';		
                            /*<li><label class="checkboxfake"><input  class="phasecheckbox" id="" name="phase[]"  data-id="" onclick="" type="checkbox" value="On-Going"><span class="fake"></span>On-Going</label></li>';*/
                           /* <li><label class="checkboxfake"><input  class="phasecheckbox" id="" name="phase[]"  data-id="" onclick="" type="checkbox" value="Signature Projects"><span class="fake"></span>Signature Projects</label></li>*/
							echo '<li> &nbsp; </li>
							<li> &nbsp; </li>
							<li> &nbsp; </li>
							
						
							
                            
                        </ul>
                    </li>
                    <li class="col-sm-2">';
                    ?>
						<ul>
                            <li class="dropdown-header">Price Range</li>
                        </ul>
					<!--	<div style="padding-left: 22px;float:left; width: 40%">
							<input
								type="text"
								name="price_range"
								id="range1" 
								data-provide="slider"
							
								data-slider-min="400000"
								data-slider-max="1000000"
								data-slider-step="10"
								data-slider-value="[500000,680000]"
								data-slider-orientation="vertical"
							>
							
						</div>
						<div style="padding-left: 0;float:left; width: 60%">
							<ul class="list-unstyled ul_pricerange">
								<li>more than<br>1 000 000</li>
								<li>900 000</li>
								<li>800 000</li>
								<li>700 000</li>
								<li>600 000</li>
								<li>500 000</li>
								<li>less than<br>4 000 000</li>
								
							</ul>
						</div>
						-->
						<div class="pricebox" style="">
							<p>From</p>
							<input type="text" name="price_min" id="price_min" class="priceinput form-control" value="" placeholder="Min (MYR)">
							<br/>
							<p>To</p>
							<input type="text"  name="price_max" id="price_max" class="priceinput form-control" value="" placeholder="Max (MYR)">
						</div>

                    <?php
                    echo '</li>
					
                </ul>
                
            </li>
        </ul>';
		?>

		
		<?php
		echo '
        </form>
    </div>';

	echo '</div></div>';
}

// Outputting elements that are hidden in default state but are visible in tablets / mobiles state
$default_elms = us_get_header_shown_elements_list( us_get_header_layout() );
$tablets_elms = us_get_header_shown_elements_list( us_get_header_layout( 'tablets' ) );
$mobiles_elms = us_get_header_shown_elements_list( us_get_header_layout( 'mobiles' ) );
$layout['temporarily_hidden'] = array_diff( array_unique( array_merge( $tablets_elms, $mobiles_elms ) ), $default_elms );
echo '<div class="l-subheader for_hidden hidden">';
us_output_header_elms( $layout, $data, 'temporarily_hidden' );
echo '</div>';

echo '</header>';
