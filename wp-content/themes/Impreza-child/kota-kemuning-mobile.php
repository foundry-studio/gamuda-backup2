<?php
// Template Name:  Kota Kemuning Mobile Template 
get_header(); ?>

<div class="mobile-main-nav mobile-main-nav-all">
    <div class="mobile-main-nav-header">
        <a class="mobile-main-nav-logo" href="http://128.199.89.40/"><img src="http://128.199.89.40/wp-content/uploads/2016/07/logo-1.png" alt="Gamuda Land Logo"></a>
		<div class="mobile-main-nav-close"><img src="/wp-content/uploads/2016/09/nav-close-btn.png"></div>
    </div>
    <ul class="list-unstyled">
        <li><a href="http://128.199.89.40/about-us/">ABOUT US</a></li>
        <li class="mobile-sub-nav-open">
            <a href="http://128.199.89.40/our-developments/">OUR DEVELOPMENTS</a>
            <i class="fa fa-angle-right" aria-hidden="true"></i>
        </li>
        <li><a href="http://128.199.89.40/happenings/">HAPPENINGS</a></li>
        <li><a href="http://128.199.89.40/contact-us/">CONTACT US</a></li>
    </ul>
    <a class="mobile-main-nav-register" href="https://webreg.gamudaland.com.my/contact/_publicwebregistration/" target="_blank">REGISTER</a>
    <div class="mobile-main-nav-sns">
        <a href="https://www.facebook.com/GamudaLand/?fref=ts&ref=br_tf"><i class="fa fa-facebook" aria-hidden="true"></i></a>
        <a href="https://www.instagram.com/gamudaland/"><i class="fa fa-instagram" aria-hidden="true"></i></a>
    </div>
</div>

<div class="mobile-main-nav mobile-main-nav-projects">

    <div class="mobile-main-nav-header">
        <a class="mobile-main-nav-logo" href="http://128.199.89.40/"><img src="http://128.199.89.40/wp-content/uploads/2016/07/logo-1.png" alt="Gamuda Land Logo"></a>
		<div class="mobile-main-nav-close"><img src="/wp-content/uploads/2016/09/nav-close-btn.png"></div>
    </div>
    <div class="mobile-main-nav-projects-list">
        <ul class="list-unstyled">
            <li class="mobile-sub-nav-close">
                <b>FEATURED</b> DEVELOPMENTS
                <i class="fa fa-angle-left" aria-hidden="true"></i>
            </li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/highpark-suites/">HIGHPARK SUITES<span>Kelana Jaya</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/the-robertson/">THE ROBERTSON<span>Kuala Lumpur</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/jade-hills/">JADE HILLS<span>Kajang</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/horizon-hills/">HORIZON HILLS<span>Iskandar Puteri</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/bukit-bantayan/">BUKIT BANTAYAN<span>Kota Kinabalu</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/gamuda-walk/">GAMUDA WALK<span>Shah Alam</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/gem-residences/">GEM RESIDENCES<span>Toa Payoh</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/661-chapel-st/">661 CHAPEL ST.<span>Melbourne</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/gamuda-city/">GAMUDA CITY<span>Hanoi</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/featured-developments/celadon-city/">CELADON CITY<span>Ho Chi Minh</span></a></li>
        </ul>
        <ul class="list-unstyled">
            <li><b>UPCOMING</b> DEVELOPMENTS</li>
            <li><a href="http://gamudaland.com.my/kundangestates/">KUNDANG ESTATES<span>Kuang</span></a></li>
            <li><a href="#" class="navi-disable">GAMUDA GARDENS<span>Kuang</span></a></li>
            <li><a href="http://gamudaland.com.my/twentyfive7/">TWENTYFIVE7<span>Kota Kemuning</span></a></li>
        </ul>
        <ul class="list-unstyled">
            <li><b>FULLY SOLD</b> DEVELOPMENTS</li>
            <li><a href="http://128.199.89.40/our-developments/fully-sold-developments/kota-kemuning/">KOTA KEMUNING<span>Shah Alam</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/fully-sold-developments/bandar-botanic/">BANDAR BOTANIC<span>Klang</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/fully-sold-developments/valencia/">VALENCIA<span>Sungai Buloh</span></a></li>
            <li><a href="http://128.199.89.40/our-developments/fully-sold-developments/madge-mansions/">MADGE MANSIONS<span>Kuala Lumpur</span></a></li>
        </ul>
        <ul class="list-unstyled">
            <li><b>GOLF &amp; COUNTRY</b> CLUBS</li>
            <li><a href="http://www.kotapermai.com.my/" target="_blank">KOTA PERMAI<span>Golf &amp; Country Club</span></a></li>
            <li><a href="http://hhgcc.com.my/" target="_blank">HORIZON HILLS<span>Golf &amp; Country Club</span></a></li>
            <li><a href="http://www.botanicresortclub.com.my/" target="_blank">BOTANIC<span>Resort Club</span></a></li>
        </ul>
    </div>
</div>

<div id="mobile-pages" class="mobile-slide-page kota-kemuning mobile page">
    <div class="container-fluid document">
        <div class="row mobile-cover mobile-video-cover">
            <div class="mobile-cover-content">
                <img class="mobile-main-logo" src="/wp-content/themes/Impreza-child/img/kota-kemuning-logo.png" alt="Kota Kemuning Logo">
                <!--<img class="video-play-icon" src="/wp-content/themes/Impreza-child/img/play-icon.png" alt="Play Video Button">-->
            </div>
			<div class="mouse">
				<div class="mouse-icon">
					<span class="mouse-wheel"></span>
				</div>
			</div>
            <div class="mobile-cover-bg">
                <!--<img src="/wp-content/themes/Impreza-child/img/kota-kemuning-main-cover.jpg" alt="Mobile Cover Image">-->
            </div>
        </div>

        <div class="row mobile-cover">
            <div class="mobile-cover-content">
                <img class="mobile-overview-text1" src="/wp-content/themes/Impreza-child/img/kota-kemuning-overview1.png" alt="Overview - An award-winning 1,854-acre integrated township in shah alam">
            </div>
            <div class="mobile-cover-bg" style="background-image:url(/wp-content/themes/Impreza-child/img/kota-kemuning-overview-bg.jpg)">
                <!--<img src="/wp-content/themes/Impreza-child/img/kota-kemuning-overview-bg.jpg" alt="Kota Kemuning Overview Background Image">-->
            </div>
        </div>

        <div class="row sm-font mobile-text-group">
            <p>Kota Kemuning was first envisioned by DRB-HICOM Berhad and Gamuda Berhad some two decades ago. These affiliates foresaw the potential for something that had never been done before - the creation of an innovative masterplan that focused on the long-term, sustainable growth of the community.</p>
            <p>In 1995, these affiliates formed Hicom-Gamuda Development Sdn Bhd, a strategic partnership that delivered the best development solution in every aspect, creating a landmark of distinction for the community.</p>
        </div>

        <div class="row single-image">
            <img src="/wp-content/themes/Impreza-child/img/kota-kemuning-img1.jpg" alt="Kota Kemuning Image">
        </div>

        <div class="row single-image">
            <img src="/wp-content/themes/Impreza-child/img/kota-kemuning-img2.jpg" alt="Kota Kemuning Image">
        </div>

        <div class="row sm-font mobile-text-group">
            <p>Kota Kemuning is award-winning 1,854-acre integrated township in Shah Alam that epitomises quality living. This self-sustaining township contains a well-balanced mix of residential and commercial developments supported by essential amenities, as well as a network of expressways. All these elements together with others within its innovative masterplan, ensure residents get to enjoy a holistic lifestyle.</p>
            <p>While each residential precinct in this extraordinary township is crafted with its own distinct character, the township is still able to maintain a 'harmonious living environment'. With its emphasis on quality master planning and innovative solutions that deliver a quality lifestyle, Kota Kemuning is today one of the most sought after townships in the Klang Valley.</p>
        </div>

        <div class="row mobile-image-cover">
            <div class="mobile-image-cover-content">
                <img src="/wp-content/themes/Impreza-child/img/half-circle.png">
                <h3 class="md-font">CONCEPT & DESIGN</h3>
            </div>
            <img src="/wp-content/themes/Impreza-child/img/kota-kemuning-img3.jpg" alt="Kota Kemuning Image">
        </div>

        <div class="row mobile-text-group">
            <h2 class="lg-font">A DEFINING NEW BENCHMARK<br>FOR SUSTAINABLE COMMUNITY</h2>
            <p class="sm-font">Kota Kemuning is an award-winning intergrated township featuring unbridled green living accentuated by a harmonious blend of modern homes and
exceptional public amenities.</p>
            <hr class="paragraph-space">
            <h3 class="md-font green-gradient-font">CREATIVITY IN<br>ENVIRONMENTAL<br>ARCHITECTURE</h3>
            <p class="sm-font">Our effort to enhance nature's beauty is driven by our belief of creating eco-friendly environments where communities can learn to live with the delicate beauty of nature. We stand firm to the rule of enhancing the environment and this is a critical factor during the concept planning and design stages.</p>
        </div>

        <div class="row single-image">
            <img src="/wp-content/themes/Impreza-child/img/kota-kemuning-img4.jpg" alt="Kota Kemuning Image">
        </div>

        <div class="row single-image">
            <img src="/wp-content/uploads/2016/09/kota-kemuning-concept-bg-sm.jpg" alt="Kota Kemuning Image">
        </div>

        <!--<div class="row mobile-cover">
            <div class="mobile-cover-content">
            </div>
            <div class="mobile-cover-bg">
                <img src="/wp-content/themes/Impreza-child/img/kota-kemuning-concept-bg.jpg" alt="Kota Kemuning Concept & Design Background Image">
            </div>
        </div>-->

        <div class="row mobile-list-group" style="background-color:#fff;">
		
			<h2 class="lg-font" style="color: #095f1f;font-weight:bold">AWARDS &amp; CERTIFICATION</h2>
            <ul class="list-unstyled" style="margin-left:0;">
                <li style="text-align:center">
                    <img src="/wp-content/uploads/2016/08/kota.png" alt="The EDGE Property Development Excellence Award 2015 Gamuda Land">
                </li>
                <li style="text-align:center">
                    <img src="/wp-content/uploads/2016/08/honour.png" alt="MLAA Honour Award Gamuda Land">
                </li>
                <li style="text-align:center">
                    <img src="/wp-content/uploads/2016/08/conquas.png" alt="Conquas Rared Gamuda Homes Gamuda Land">
                </li>
            </ul>
        </div>

        <div class="row mobile-cover">
            <div class="mobile-cover-content">
                <img class="mobile-overview-text2" src="/wp-content/themes/Impreza-child/img/kota-kemuning-overview2.png" alt="Vision - An award-winning 1,854-acre integrated township in shah alam">
            </div>
            <div class="mobile-cover-bg" style="background-image:url(/wp-content/themes/Impreza-child/img/kota-kemuning-vision-bg.jpg); background-position: top 28% center;">
                <!--<img src="/wp-content/themes/Impreza-child/img/kota-kemuning-vision-bg.jpg" alt="Kota Kemuning Vision Background Image">-->
            </div>
        </div>

        <div class="row mobile-full-banner">
            <img src="/wp-content/themes/Impreza-child/img/kota-kemuning-banner1.jpg" alt="Kota Kemuning Banner">
			<ul class="kota-kemuning-map-box">
				<li><span class="md-font mobile-full-banner-title">RECREATIONAL<br>PARKS</span></li>
                <li><div class="trigger-modal-btn video_thumbnail" data-toggle="modal" data-target="#kkModalVideo2"><img src="/wp-content/uploads/2016/08/btn-video-play.png"></div></li>
                <li><div class="trigger-modal-btn " data-toggle="modal" data-target="#kk-recreational-more-modal"><img src="/wp-content/uploads/2016/08/btn-know-more.png"></div></li>
			</ul>
            <!--<h3 style="border-bottom-color: #148479;" class="md-font mobile-full-banner-title">RECREATIONAL<br>PARKS</h3>-->
        </div>
        <div class="row mobile-full-banner">
            <img src="/wp-content/themes/Impreza-child/img/kota-kemuning-banner4.jpg" alt="Kota Kemuning Banner">
			<ul class="kota-kemuning-map-box">
                <li><div class="trigger-modal-btn video_thumbnail" data-toggle="modal" data-target="#kkModalVideo1"><img src="/wp-content/uploads/2016/08/btn-video-play.png"></div></li>
                <li><div class="trigger-modal-btn" data-toggle="modal" data-target="#kk-golf-more-modal"><img src="/wp-content/uploads/2016/08/btn-know-more.png"></div></li>
				<li><span class="md-font mobile-full-banner-title">GOLF<br>COURSE</span></li>
			</ul>
            <!--<h3 style="border-bottom-color: #403125;" class="md-font mobile-full-banner-title">GOLF<br>COURSE</h3>-->
        </div>
        <div class="row mobile-full-banner">
            <img src="/wp-content/themes/Impreza-child/img/kota-kemuning-banner5.jpg" alt="Kota Kemuning Banner">
			<ul class="kota-kemuning-map-box">
				<li><span class="md-font mobile-full-banner-title">RESIDENTIAL<br>CLUSTER</span></li>
                <li><div class="trigger-modal-btn video_thumbnail" data-toggle="modal" data-target="#kkModalVideo3"><img src="/wp-content/uploads/2016/08/btn-video-play.png"></div></li>
                <li><div class="trigger-modal-btn " data-toggle="modal" data-target="#kk-residential-more-modal"><img src="/wp-content/uploads/2016/08/btn-know-more.png"></div></li>
			</ul>
            <!--<h3 style="border-bottom-color: #148479;" class="md-font mobile-full-banner-title">RESIDENTIAL<br>CLUSTER</h3>-->
        </div>
	
        <!----- Modals ----->
        <div class="modal fade modalVideo" id="kkModalVideo1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body text-center"> 
                        <ul class="video-modal-buttons">
                            <li><div class="trigger-modal-btn" data-toggle="modal" data-target="#kk-golf-more-modal" data-dismiss="modal" aria-label="Close"><img src="/wp-content/uploads/2016/08/btn-know-more.png" alt="Know More Button"></div></li>
                            <li><button type="button" class="close" data-dismiss="modal" aria-label="Close" style="text-transform: inherit;color:#fff;opacity:1"><img src="/wp-content/uploads/2016/08/btn-modal-close.png" alt="Close Button"></button>  </li>
                        </ul>

                        <video id="video1" controls = "controls" class="modal_video_element" width="100%" allowfullscreen>
                            <source src="http://103.8.25.232/uploads/golf.mp4" type="video/mp4" allowfullscreen />
                        </video>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade modalVideo" id="kkModalVideo2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body text-center"> 
                        <ul class="video-modal-buttons">
                            <li><div class="trigger-modal-btn" data-target="#kk-recreational-more-modal" data-toggle="modal" data-dismiss="modal" aria-label="Close"><img src="/wp-content/uploads/2016/08/btn-know-more.png" alt="Know More Button"></div></li>
                            <li><button type="button" class="close" data-dismiss="modal" aria-label="Close" style="text-transform: inherit;color:#fff;opacity:1"><img src="/wp-content/uploads/2016/08/btn-modal-close.png" alt="Close Button"></button>  </li>
                        </ul>

                        <video id="video1" controls = "controls" class="modal_video_element" width="100%" allowfullscreen>
                            <source src="http://103.8.25.232/uploads/commercial.mp4" type="video/mp4" allowfullscreen />
                        </video>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade modalVideo" id="kkModalVideo3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body text-center"> 
                        <ul class="video-modal-buttons">
                            <li><div class="trigger-modal-btn" data-toggle="modal" data-target="#kk-residential-more-modal" data-dismiss="modal" aria-label="Close"><img src="/wp-content/uploads/2016/08/btn-know-more.png" alt="Know More Button"></div></li>
                            <li><button type="button" class="close" data-dismiss="modal" aria-label="Close" style="text-transform: inherit;color:#fff;opacity:1"><img src="/wp-content/uploads/2016/08/btn-modal-close.png" alt="Close Button"></button>  </li>
                        </ul>

                        <video id="video1" controls = "controls" class="modal_video_element" width="100%" allowfullscreen>
                            <source src="http://103.8.25.232/uploads/residential.mp4" type="video/mp4" allowfullscreen />
                        </video>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade modal-info" id="kk-golf-more-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="container">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="text-transform: inherit;color:#333;opacity:1">Close  <i aria-hidden="true" class="fa fa-times-circle-o"></i></button>
                            <div class="header-one">
                                <p style="color:#9bba08;margin-bottom:50px;">GOLF<br>COURSE</p>
                            </div>
                            <div class="row">
                                <p class="col-lg-6 col-md-6 col-sm-7">The hallmark of the Kota Kemuning township, the Kota Permai Golf and Country Club is the perfect getaway for golf and recreational pleasures. Boasting an international standard 18-hole golf course designed by the renowned Ross Watson, this award-winning course offers arguably, the best golfing experience in Malaysia and ranks among Asia’s premier golfing destinations. Aside from its immaculately maintained greens, its premium clubhouse provides the community a full range of facilities and amenities.</p>
                            </div>
                            <div class="w-btn btn-popup-watchvideo col-lg-3 col-md-3 col-xs-8"  style="font-size: 15px ;margin-top:50px;" data-toggle="modal" data-target="#kkModalVideo1" data-dismiss="modal" aria-label="Close"><span class="w-btn-label">Watch Video</span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade modal-info" id="kk-recreational-more-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="container">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="text-transform: inherit;color:#333;opacity:1">Close  <i aria-hidden="true" class="fa fa-times-circle-o"></i></button>
                            <div class="header-one">
                                <p style="color:#066c68;margin-bottom:50px;">RECREATIONAL<br>PARKS</p>
                            </div>
                            <div class="row">
                                <p class="col-lg-10 col-md-10 col-sm-12">One of Kota Kemuning’s finest attributes is its vast tracks of greens, parks, lakes and landscaped gardens. More than 45% of its land is dedicated to greens, parks and lakes providing the community with a healthy and harmonious environment. The vast track of greens includes an eight-kilometre continuous walkway that meanders throughout the neighbourhood linking parks and precincts.<br><br>

                                <strong>CENTRAL LAKE</strong><br><br>
                                The signature 22-acre Central Lake and surround 25-acre park provide the perfect environment for the community to relax and unwind. For residents, there is nothing more soothing than the panoramic views of the lake and the surrounding greens. Residents can also take a stroll along Lakeside Drive that offers great vistas of the serene lake.<br><br>

                                <strong>WETLAND PARK</strong><br><br>
                                The 22-acre Wetland Park integrates a parkland environment with a touch of 'tropical wilderness' into this resort-styled township. Originally, swampland, Gamuda Land transformed this once unproductive area into a wetland park, making it the only wetlands to be built by a housing developer as a recreational park for the community. This award-winning park is today a habitat for a variety of birds species and local flora and fauna.<br><br>

                                <strong>HILL PARK</strong><br><br>
                                Set atop a hill, the lush three-acre Hill Park serves as a park for community recreation and relaxation activities. Built around existing greens and trees, the award-winning Hill Park encompasses attractions such as a reflexology path, timber lookout deck, gazebo for relaxation and walking trail that challenges fitness levels.</p>
                            </div>
                            <div class="w-btn btn-popup-watchvideo col-lg-3 col-md-3 col-xs-8"  style="font-size: 15px ;margin-top:50px;" data-toggle="modal" data-target="#kkModalVideo2" data-dismiss="modal" aria-label="Close"><span class="w-btn-label">Watch Video</span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade modal-info" id="kk-residential-more-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="container">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="text-transform: inherit;color:#333;opacity:1">Close  <i aria-hidden="true" class="fa fa-times-circle-o"></i></button>
                            <div class="header-one">
                                <p style="color:#409fb7;margin-bottom:50px;">RESIDENTIAL<br>CLUSTER</p>
                            </div>
                            <div class="row">
                                <p class="col-lg-6 col-md-6 col-sm-7">Within Kota Kemuning’s residential cluster is Kota Kemuning Hills, an exclusive gated residential precinct set atop the highest point of the township and offering magnificent views of the golf and country club. The homes and facilities here nestle amidst the undulating terrain and rolling greens, much of which has been preserved to ensure the integrity of the development’s hilltop charm.</p>
                            </div>
                            <div class="w-btn btn-popup-watchvideo col-lg-3 col-md-3 col-xs-8"  style="font-size: 15px ;margin-top:50px;" data-toggle="modal" data-target="#kkModalVideo3" data-dismiss="modal" aria-label="Close"><span class="w-btn-label">Watch Video</span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<div class="row">
			<div class="mobile-other-projects">
				<h2 class="md-font">OTHER DEVELOPMENTS</h2>
				<div class="col-xs-6 mobile-prev-project">
					<a href="http://128.199.89.40/our-developments/fully-sold-developments/madge-mansions/"><img src="/wp-content/uploads/2016/09/m-other-dev-banner-madgemansions.jpg" alt="Madge Mansions"></a>
				</div>
				<div class="col-xs-6 mobile-next-project">
					<a href="http://128.199.89.40/our-developments/fully-sold-developments/bandar-botanic/"><img src="/wp-content/uploads/2016/09/m-other-dev-banner-bandarbotanic.jpg" alt="Bandar Botanic"></a>
				</div>
			</div>
		</div>
    </div>
</div>
<?php get_footer();